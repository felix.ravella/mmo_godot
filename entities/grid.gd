extends Node2D

const cell_scene = preload('res://entities/cell/cell.tscn')

onready var cells_holder: Node2D = $CellsHolder

var cells: Array = []

var size: Vector2

# returns all cells in range from given origin
func get_area(origin: Vector2, shape: String = "circle", radius: int = 0, caster: Battler = null) -> Array:
  var valid_cells: Array = []
  match shape:
    "circle":
      valid_cells.append(cells[origin.x][origin.y])
      for distance in range(0, radius + 1):
        for b in range (distance * -1, distance):
          var coords_1 = Vector2(origin.x + b, origin.y - (distance - abs(b)))
          var coords_2 = Vector2(origin.x - b, origin.y + (distance - abs(b)))
          # ignore if coords are out of grid
          if not is_coords_out_of_bounds(coords_1):
            valid_cells.append(cells[coords_1.x][coords_1.y])
          if not is_coords_out_of_bounds(coords_2):
            valid_cells.append(cells[coords_2.x][coords_2.y])
          
#      for a in range(radius * -1, radius + 1):
#        # continue if coords are out of grid
#        if origin.x + a < 0 || origin.x + a >= size.x:
#          continue
#        # leeway in b is reduced by current a offset (absolute value)
#        for b in range( (radius - abs(a)) * -1, radius - abs(a) + 1 ):
#          # continue if coords are out of grid
#          if origin.y + b < 0 || origin.y + b >= size.y:
#            continue
#          valid_cells.append(cells[origin.x + a][origin.y + b])
    "square":
      for i in range(-radius, radius + 1):
        for j in range(-radius, radius + 1):
          if not is_coords_out_of_bounds(Vector2(origin.x + i, origin.y + j)):
            valid_cells.append(cells[origin.x + i][origin.y + j]) 
    "cleave":
      print("cleave")
      valid_cells.append(cells[origin.x][origin.y])
      var caster_pos: Vector2 = caster.current_cell.coords
      var delta_x = origin.x - caster_pos.x
      var delta_y = origin.y - caster_pos.y
      if delta_x * delta_y == 0:
        # one of the coordinate delta equals 0 => not a diagonal
        if not is_coords_out_of_bounds(Vector2(origin.x + delta_y, origin.y + delta_x)):
          valid_cells.append(cells[origin.x + delta_y][origin.y + delta_x])
        if not is_coords_out_of_bounds(Vector2(origin.x - delta_y, origin.y - delta_x)):
          valid_cells.append(cells[origin.x - delta_y][origin.y - delta_x])
      else:
        # all coordinate deltas not equals 0 => diagonal
        if not is_coords_out_of_bounds(Vector2(origin.x - delta_x, origin.y)):
          valid_cells.append(cells[origin.x - delta_x][origin.y])
        if not is_coords_out_of_bounds(Vector2(origin.x, origin.y - delta_y)):
          valid_cells.append(cells[origin.x][origin.y - delta_y])
      print("found valid cells:")
      for cell in valid_cells:
        print("[%d, %d]" % [cell.coords.x, cell.coords.y])
    _:
      print("ERROR! got unexpected area shape '%s" % shape)
  return valid_cells

func is_coords_out_of_bounds(coords: Vector2) -> bool:
  return coords.x > size .x-1 || coords.x < 0 || coords.y > size.y-1 || coords.y < 0

func initialize(_size: Vector2, parent):
  size = _size
  for row in size.x:
    cells.append([]) # add new row to cells handle
    for col in size.y:
      var new_cell = cell_scene.instance()
      cells_holder.add_child(new_cell)
      var is_edge = true if col == size.y - 1 || row == size.x - 1 else false
      new_cell.initialize(Vector2(row, col), is_edge, 'green')
      new_cell.connect('cell_entered', parent,'_on_cell_hover')
      new_cell.connect('cell_clicked', parent,'_on_cell_clicked')
      cells[row].append(new_cell)
  # another loop once every cells are instanciated, to give each cells their neighbors
  for cell in get_tree().get_nodes_in_group("Cell"):
    cell.neighbors = get_neighbors(cell.coords)

# trouve le path de Cell pour aller d'une Cell origin à un Cell goal
func find_path(battler, goal) -> Array:
  var origin = battler.current_cell
  if origin == goal:
    reset_pathfinding_values()
    return []
  var last_found_cells = [origin]  # array of cells found during previous loop
  origin.checked = true
  # obsolete usage of i for countaing already checked cells. remove when certain it is not needed
#  var i = 1 # use so that we dont check more cell than max (in case not path is found)
#  while i < size.x * size.y:  
  while last_found_cells.size() > 0:
    var found_cells = [] # array of cells found during current loop
    for cur_cell in last_found_cells:
      for neighbor in cur_cell.neighbors:
        if neighbor == goal:
          var path: Array = cur_cell.path_to_origin.duplicate()
          path += [cur_cell, goal]
          path.remove(0) # we remove origin
          reset_pathfinding_values()
          return path.duplicate()
        # ignore neighbor at hand if already checked for this pathfinding
        if neighbor.checked:
          continue
#        i += 1
        neighbor.checked = true
        if not neighbor.is_crossable_by(battler):
          continue
#        # if we're here, we assume neighbor at hand is viable
#        # we add it to found cells array and we make it mark it's path to origin
        found_cells.append(neighbor)
        var t_path = cur_cell.path_to_origin.duplicate()
        t_path.append(cur_cell)
        neighbor.path_to_origin = t_path.duplicate()
    last_found_cells = found_cells.duplicate()
  print("could't find path")
  reset_pathfinding_values()
  return []
#
func get_neighbors(coords: Vector2):
  # coords of all potential neighbors
  var neighbors_coords = [
    Vector2(coords.x - 1, coords.y),
    Vector2(coords.x + 1, coords.y),
    Vector2(coords.x, coords.y - 1),
    Vector2(coords.x, coords.y + 1),
  ]
  var neighbor_cells = []
  # check if potential neighbor coord doesn't overflow grid size;
  # aggregates and returns 
  for neighbor in neighbors_coords:
    if not is_coords_out_of_bounds(neighbor):
      neighbor_cells.append(cells[neighbor.x][neighbor.y])
  return neighbor_cells

func reset_pathfinding_values():
  for cell in get_tree().get_nodes_in_group("Cell"):
    cell.is_pathing = false
    cell.path_to_origin = []
    cell.checked = false
    
func reset_targeting_values():
  for cell in get_tree().get_nodes_in_group("Cell"):
    cell.is_target = false

