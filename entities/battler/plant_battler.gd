extends Battler
class_name PlantBattler

var plant_type: String
var caster: Battler
var power: int
var growth: int = 0  setget _set_growth # value increased on start turn or when being boosted by caster
var growth_stage_index: int = 0 # current index of 
var next_stage = null setget ,_get_next_stage

var linked_ground_effect: GroundEffect = null
  
func _get_next_stage():
  if !plant_type:
    return null
  var stages = Data.plants_stages[plant_type].growth_stages
  # check if has next stage
  if stages.size() > growth_stage_index + 1:
    return stages[growth_stage_index + 1]
  else:
    return null
  
func _set_growth(value: int) -> void:
  growth = value
  var stages = Data.plants_stages[plant_type].growth_stages
  print(stages)
  print(growth)
  print(growth_stage_index)
  if self.next_stage != null:
    if growth > stages[growth_stage_index+1].threshold:
      growth_stage_index += 1
      handle_new_stage()
  
func create(_plant_type: String, _caster: Battler, _power: int, _uuid, cell: Cell, _team = 0, _facing: String = "down_left") -> void:
  # load battler_data in Global Data
  plant_type = _plant_type
  caster = _caster
  power = _power
  var battler_data = Data.plant_battlers_datas[plant_type]
  .initialize(battler_data, {}, _uuid, cell, _team, _facing)
  handle_new_stage()
  # add status (rooted)
  
  
func die():
  if linked_ground_effect != null:
    get_node("/root/Battle").remove_ground_effect(linked_ground_effect.uuid)
  .die()
  
func start_turn() -> void:
  .start_turn()
  if !!self.next_stage:
    self.growth += 1

func handle_new_stage():
  var new_stage = Data.plants_stages[plant_type].growth_stages[growth_stage_index]
  var hp_delta = 0
  var new_data = Data.plant_battlers_datas[new_stage.name]
  if growth_stage_index > 0:
    var previous_data = data
    hp_delta = new_data.max_hp - data.max_hp
  # battler_data
  data = new_data
  # update current hp
  self.hp += hp_delta
  # update sprite
  var sprite_frames_path = "res://tres/battler_frames/%s.tres" % new_stage.name
  if not ResourceLoader.exists(sprite_frames_path):
    sprite_frames_path = "res://tres/battler_frames/default.tres"
  animated_sprite.frames = load(sprite_frames_path)
  animated_sprite.scale = new_stage.scale
  position = current_cell.position + new_stage.offset
  # type-specific actions
  var battle = get_node("/root/Battle")
  match plant_type:
    'thorny_grove':
      if (new_stage.name == 'thorny_sprout'):
        add_status("thorns", self, power * 2, 99)
      if (new_stage.name == 'thorny_bush'):
        var spawn_cell = current_cell
        var base_radius = 1
        linked_ground_effect = battle.add_ground_effect(spawn_cell, self, "thorns", base_radius, 99, power)
      if (new_stage.name == 'thorny_grove'):
        battle.remove_ground_effect(linked_ground_effect.uuid)
        var spawn_cell = current_cell
        var base_radius = 2
        linked_ground_effect = battle.add_ground_effect(spawn_cell, self, "thorns", base_radius, 99, power) 
      pass
    'toxic_mycelium':
      if (new_stage.name == 'growing_mycelium'):
        var spawn_cell = current_cell
        var base_radius = 1
        linked_ground_effect = battle.add_ground_effect(spawn_cell, self, "mushrooms", base_radius, 99, power)
      if (new_stage.name == 'noxious_mycelium'):
        battle.remove_ground_effect(linked_ground_effect.uuid)
        var spawn_cell = current_cell
        var base_radius = 2
        linked_ground_effect = battle.add_ground_effect(spawn_cell, self, "mushrooms", base_radius, 99, power)
      if (new_stage.name == 'toxic_mycelium'):
        battle.remove_ground_effect(linked_ground_effect.uuid)
        var spawn_cell = current_cell
        var base_radius = 3
        linked_ground_effect = battle.add_ground_effect(spawn_cell, self, "mushrooms", base_radius, 99, power)
    _:
      pass
