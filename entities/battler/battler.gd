extends Node2D
class_name Battler

const OFFSET_TO_CELL = Vector2(0, -14)

const hero_data_scene: PackedScene = preload("res://classes/hero_data.tscn")
const numeric_display_scene: PackedScene = preload("res://entities/numeric_display/numeric_display.tscn")

onready var animated_sprite: AnimatedSprite = $AnimatedSprite
onready var effect_animated_sprite: AnimatedSprite = $EffectAnimatedSprite
onready var tween: Tween = $Tween
onready var name_label = $Decoration/NameLabel
onready var ready_sprite = $Decoration/ReadySprite
onready var is_current_sprite = $Decoration/IsCurrentSprite
onready var animation_player = $AnimationPlayer
onready var decide_timer = $DecideTimer


var data = {
  "id": "0",
  "name": "default_name",
  "sprite": "default",
  "max_ap": 6,
  "max_mp": 3,
  "max_hp": 1,
  "skills": [],
 }
var hero_data: HeroData

var uuid: String = ""
var team: int = 0
var is_ready = false setget _set_is_ready
var hp: int = 10 setget _set_hp
var ap: int = 3 setget _set_ap
var mp: int = 3 setget _set_mp
var armor: int = 0 setget _set_armor
var armor_cap: int = 0 setget , _get_armor_cap
var statuses: Array = []
var entered_ground_effects_this_turn: Array = []
var left_ground_effects_this_turn: Array = []
var cooldowns: Dictionary = {}

var current_cell: Cell = null setget _set_current_cell
var facing: String setget _set_facing
var is_on_occupied_cell = false
var is_current_battler: bool = false setget _set_is_current_battler

signal step_finished
signal travel_finished
signal battler_clicked(battler)
signal monster_decide_move(author, destination)
signal monster_decide_action(author, target, action)
signal monster_decide_pass(author)
signal values_changed(battler)
signal battler_damaged(battler, value)
signal battler_healed(battler, value)
signal battler_died(battler)

func _get_armor_cap() -> int:
  var max_hp_ratio = get_total_stat("max_hp") * 0.2
  var strength_bonus = get_total_stat("strength") / 100.0
  var willpower_bonus = (get_total_stat("willpower") / 100.0) / 2.0
  return int(max_hp_ratio * (1.0 + strength_bonus + willpower_bonus))

func _set_hp(value: int):
  hp = clamp(value, 0, data.max_hp)
  emit_signal("values_changed", self)
  if hp <= 0:
    die()
  
func die() -> void:
  # clean statuses 
  for i in range(statuses.size() -1, -1, -1):
    # 2nd argument as false so that doesn't trigger on_remove hook
    remove_status(i, false)
  emit_signal("battler_died", self)
  current_cell.battler = null
  current_cell = null
  animation_player.play("die")
  yield(animation_player, "animation_finished")
  hide()

func _set_ap(value: int):
  ap = clamp(value, 0, data.max_ap)
  emit_signal("values_changed", self)
  
func _set_mp(value: int):
  mp = clamp(value, 0, data.max_mp)
  emit_signal("values_changed", self)
  
func _set_armor(value: int):
  armor = clamp(value, 0, self.armor_cap)
  emit_signal("values_changed", self)

func _set_current_cell(new_cell) -> void:
  if current_cell == null:
    current_cell = new_cell
    new_cell.battler = self
    return
  if not is_on_occupied_cell:
    current_cell.battler = null
  # so that we don't erase cell of allied crossed 
  # so wont be needed if cannot cross allied's cell
  if new_cell.battler == null:
    new_cell.battler = self
    is_on_occupied_cell = false
  else:
    is_on_occupied_cell = true
  current_cell = new_cell

func _set_facing(value: String) -> void:
  facing = value
  animated_sprite.animation = 'move_up' if facing.begins_with('up') else 'move_down'
  animated_sprite.flip_h = facing.ends_with('right')

func _set_is_ready(value: bool) -> void:
  is_ready = value
  ready_sprite.visible = value
  
func _set_is_current_battler(value: bool) -> void:
  is_current_battler = value
  is_current_sprite.visible = value

#func is_charmed_by_target(target: Battler) -> bool:
#  for status in statuses:
#    if status.type == "charmed" && status.author == target:
#      return true
#  return false

func initialize(_battler_data: Dictionary, _hero_data: Dictionary, _uuid, cell: Cell, _team = 0, _facing: String = "down_left") -> void:
  uuid = _uuid
  team = _team
  data = _battler_data
  var sprite_frames_path
  # if is a player
  if not _hero_data.empty():
    hero_data = hero_data_scene.instance()
    hero_data.initialize(_hero_data)
    data.max_hp = hero_data.get_total_stat("max_hp")
    self.armor = hero_data.get_starting_armor()
    self.hp = hero_data.hp
    sprite_frames_path = "res://tres/battler_frames/%s.tres" % hero_data.job
  # if is monster
  else:
    self.hp = data.max_hp
    sprite_frames_path = "res://tres/battler_frames/%s.tres" % data.sprite
  if not ResourceLoader.exists(sprite_frames_path):
      sprite_frames_path = "res://tres/battler_frames/default.tres"
  animated_sprite.frames = load(sprite_frames_path)
  name_label.text = data.name
  self.ap = data.max_ap
  self.mp = data.max_mp
  self.current_cell = cell
  # places visual battler on cell
  position = current_cell.position + OFFSET_TO_CELL
  # TODO change sprite frames depending on data 
  self.facing = _facing
 
func get_total_stat(stat_name: String) -> int:
  var base_stat = 0
  if !!hero_data:
    base_stat += hero_data.get_total_stat(stat_name)
  elif data.keys().has(stat_name):
    base_stat += data[stat_name]
  var statuses_stat = 0
  for status in statuses:
    for affix in status.affixes:
      if affix.attribute == stat_name:
        statuses_stat += affix.value
  return base_stat + statuses_stat
  
func get_status_boost_ratio() -> float:
  var influence_ratio = get_total_stat("influence") / 100.0
  var intelligence_ratio = (get_total_stat("intelligence") / 100.0) / 2.0
  return 1.0 + influence_ratio + intelligence_ratio

func get_dot_boost_ratio() -> float:
  var influence_ratio = get_total_stat("intelligence") / 100.0
  var intelligence_ratio = (get_total_stat("agility") / 100.0) / 2.0
  return 1.0 + influence_ratio + intelligence_ratio

func start_turn() -> void:
  entered_ground_effects_this_turn = []
  left_ground_effects_this_turn = []
  for status in statuses:
    status._on_turn_started()
    status.duration -= 1
  # takes all indices from max to 0
  for i in range(statuses.size() -1, -1, -1):
    if statuses[i].duration <= 0:
      remove_status(i)
  for key in cooldowns.keys():
    cooldowns[key] -= 1
    if cooldowns[key] <= 0:
      cooldowns.erase(key)
  emit_signal("values_changed", self)

func end_turn() -> void:
  self.ap = data.max_ap
  self.mp = data.max_mp
  for status in statuses:
    status._on_turn_ended()

func step_to(next_cell: Cell) -> void:
  self.facing = get_direction_to(next_cell)
  var next_position: Vector2 = next_cell.position + OFFSET_TO_CELL
  tween.interpolate_property(self, 'position', position, next_position, 0.3)
  tween.start()
  # wait until tween to next cell has finished
  yield(tween, 'tween_completed')
  self.mp -= 1
  #var leaving_ground_effects = []
  # leaving
  var battle = get_node("/root/Battle")
  for effect_uuid in current_cell.ground_effects:
    if not next_cell.ground_effects.keys().has(effect_uuid):
      var ground_effect_instance = battle.ground_effects[effect_uuid]
      # ignore if ground_effect is not mutlitriggerable
      if Data.ground_effects[ground_effect_instance.type].multitrigger == false \
        and left_ground_effects_this_turn.has(effect_uuid):
        continue
      #leaving_ground_effects.push_back(current_cell.ground_effects[effect_uuid])
      var effect_blueprint = GroundEffectsHolder.get(ground_effect_instance.type)
      if effect_blueprint.has_method("_on_battler_left"):
        effect_blueprint._on_battler_left(self, ground_effect_instance)
        left_ground_effects_this_turn.append(effect_uuid)
  
  var entering_ground_effects = []
  # entering
  for effect_uuid in next_cell.ground_effects:
    if not current_cell.ground_effects.keys().has(effect_uuid):
      var ground_effect_instance = battle.ground_effects[effect_uuid]
      # ignore if ground_effect is not mutlitriggerable and already triggered
      if Data.ground_effects[ground_effect_instance.type].multitrigger == false \
        and entered_ground_effects_this_turn.has(effect_uuid):
        continue
      #leaving_ground_effects.push_back(current_cell.ground_effects[effect_uuid])
      var effect_blueprint = GroundEffectsHolder.get(ground_effect_instance.type)
      if effect_blueprint.has_method("_on_battler_entered"):
        effect_blueprint._on_battler_entered(self, ground_effect_instance)
        entered_ground_effects_this_turn.append(effect_uuid)
    
  # TODO handle entering in ground_effect
  # for groudn effect uuid in next_cell
  #   trigger step to
  #   if uuid not in current_cell.ground_effects && not in entered_ground_effects
  #     trigger entered
  #     entered_ground_effects.push_front(uuid)
  self.current_cell = next_cell
  emit_signal('step_finished')
  
func travel_to(cells: Array) -> void:
  animated_sprite.playing = true
  for cell in cells:
    # trigger step_to function, then wait for signal that step animation is finished
    step_to(cell)
    yield(self, 'step_finished')
  emit_signal('travel_finished')
  # resets animation
  reset_animation()
  
func teleport_to(cell: Cell) -> void:
  self.current_cell = cell
  position = current_cell.position + OFFSET_TO_CELL  
  
func get_direction_to(cell: Cell) -> String:
  if cell.position == current_cell.position:
    return facing
  var vertical = "up" if cell.position.y < current_cell.position.y else "down"
  var horizontal = "left" if cell.position.x < current_cell.position.x else "right"
  return "%s_%s" % [vertical, horizontal] 

func undergo_attack(caster: Battler, damages: int) -> void:
  var charm_ratio = caster.get_charm_ratio(self)
  var lethal_ratio = caster.get_lethal_ratio()
  var vulnerable_ratio = get_vulnerable_ratio()
  var taken_damages = int(damages * charm_ratio * lethal_ratio * vulnerable_ratio)
  
  for status in statuses:
    status._on_attack_taken(caster, taken_damages)
  take_damage(taken_damages)
  
func play_effect_animation(animation_name: String) -> void:
  if effect_animated_sprite.frames.get_animation_names().has(animation_name):
    effect_animated_sprite.frame = 0
    effect_animated_sprite.play(animation_name)
    
func _on_effect_animation_finished():
  effect_animated_sprite.frame = 0
  effect_animated_sprite.playing = false
  effect_animated_sprite.animation = "default"
  
func reset_animation() -> void:
  var direction = "up" if facing.begins_with("up") else "down"  
  animated_sprite.animation = "move_%s" % direction
  animated_sprite.playing = false
  animated_sprite.frame = 0
  
func cast(skill_id: String, target_cell: Cell) -> void:
  self.facing = get_direction_to(target_cell)
  var skill_data = Data.skills[skill_id]
  lose_ap(skill_data.ap_cost)
  play_effect_animation(skill_data.cast_animation)
  var direction = "up" if facing.begins_with("up") else "down"
  animated_sprite.play("%s_%s" % [skill_data.cast_animation, direction])
  yield(get_tree().create_timer(.5), "timeout")
  reset_animation()
  
  # TODO play animation

func take_damage(value) -> void:
  emit_signal("battler_damaged", self, value)
  var new_display = numeric_display_scene.instance()
  current_cell.add_child(new_display)
  new_display.initialize("-%s" % value, current_cell.global_position)
  if armor >= value:
    self.armor -= value
  else:
    var hp_loss = value - armor
    self.armor = 0
    self.hp -= hp_loss
    var direction = "up" if facing.begins_with("up") else "down"    
    animated_sprite.play("hurt_%s" % direction)
    yield(get_tree().create_timer(.5), "timeout")
    reset_animation()
  
func heal(hp_gain) -> void:
  emit_signal("battler_healed", self, hp_gain)  
  var new_display = numeric_display_scene.instance()
  current_cell.add_child(new_display)
  new_display.initialize("+%s" % hp_gain, current_cell.global_position, "heal")
  self.hp += hp_gain

#func pay_skill_cost(skill_id: String) -> void:
#  var skillData = Data.skills[skill_id]
#  lose_ap(skillData.ap_cost)
  
func lose_ap(value: int) -> void:
  var new_display = numeric_display_scene.instance()
  current_cell.add_child(new_display)
  new_display.initialize("-%s" % value, current_cell.global_position, "ap")
  self.ap -= value
  

func add_status(type: String, author: Battler, power: int, duration: int, affixes = []) -> void:
  var status_class = load("res://classes/status/%s_status.gd" % type)
  var new_status = status_class.new(author, self, power, duration, affixes)
  if has_status(type) && not new_status.is_stackable:
    new_status.queue_free()
    # TODO replace old status
    return
  else:
    statuses.push_back(new_status)
    new_status._on_added()

func remove_status(index, triggers_remove: bool = true) -> void:
  # status is either numeric index, or type string
  if typeof(index) == TYPE_STRING:
    var found_index = -1
    for i in range(statuses.size()):
      if statuses[i].type == index:
        found_index = i
    index = found_index
  if index == -1:
    return
  # triggers_remove indicates if removal of status should triggers its _on_remove effects, or be silent
  # for example, on death
  if triggers_remove:
    statuses[index]._on_removed()
  statuses[index].queue_free()
  statuses.remove(index)
  
func has_status(type: String) -> bool:
  for status in statuses:
    if status.type == type:
      return true
  return false
  
func get_harmful_status_indexes() -> Array:
  var index_list = []
  for i in range(statuses.size()):
    if statuses[i].type == "modified_attribute":
      var has_only_negatives = true
      for affix in statuses[i].affixes:
        if affix.value > 0:
          has_only_negatives = false
      if !!has_only_negatives:
        index_list.append(i)
    else:
      if statuses[i].is_harmful:
        index_list.append(i) 
  return index_list
  
func get_harmless_status_indexes() -> Array:
  var index_list = []
  for i in range(statuses.size()):
    if statuses[i].type == "modified_attribute":
      var has_only_positives = true
      for affix in statuses[i].affixes:
        if affix.value < 0:
          has_only_positives = false
      if !!has_only_positives:
        index_list.append(i)
    else:
      if statuses[i].is_harmful == false:
        index_list.append(i) 
  return index_list
  
# -10% damage inflicted at power 100
func get_charm_ratio(target: Battler) -> float:
  for status in statuses:
    if status.type == "charmed" && status.author == target:
      return 1.0 - 0.1 * (status.power / 100.0)
  return 1.0
  
# +30% damage inflicted at power 100
func get_lethal_ratio() -> float:  
  for status in statuses:
    if status.type == "lethal":
      return 1.0 + 0.3 * (status.power / 100.0)
  return 1.0

# +10% damage received at power 100 
func get_vulnerable_ratio() -> float:
  for status in statuses:
    if status.type == "vulnerable":
      return 1.0 + 0.1 * (status.power / 100.0)
  return 1.0
  
func _on_mouse_entered():
  var info_text = data.name
  info_text += "\nHP: %d / %d" % [hp, data.max_hp]
  info_text += " ARM: %d" % armor if armor > 0 else ""
  for status in statuses:
    info_text += "\n%s (%d)" % [status.label, status.duration] 
#  Tooltip.show_battler(info_text, self.global_position)
  Tooltip.show_battler(self)


func _on_mouse_exited():
  Tooltip.hide()


# TODO monster AI
# this function should examine possibilities and take into account remaining HP, MP, AP,
# and proximity to nearest foe / friend, to take an action or decide to end turn
func ia_decide():
  decide_timer.start(0.2)
  yield(decide_timer, "timeout")
  var castable_skills = []
  for skill_id in data.skills:
    if can_launch(skill_id):
      castable_skills.append(skill_id)
  for skill_id in castable_skills:
    var valid_targets = valid_targets(skill_id)
    if valid_targets.size() == 0:
      continue
    else:
      var target = valid_targets[GameManager.RNG.randi_range(0, valid_targets.size() -1)] 
      print("%s decide action %s" % [data.name, skill_id])
      emit_signal("monster_decide_action", self, skill_id, target ) 
      return
  if mp >= 1:
    print("%s ponders moving closer..." % data.name)
#    var destination: Vector2 = current_cell.coords + Vector2(+1,+1)
#    print("%s moving to cell %d %d" % [data.name, destination.x, destination.y])  
#    emit_signal("monster_decide_move", self, destination)
    var path = ia_path_to_get_in_range(castable_skills[0]) if castable_skills.size() > 0 else ia_path_to_get_in_range('default')
    if path.size() > 0:
      # get last cell of path
      var last_cell = path.pop_back()
      if last_cell != null:
        emit_signal("monster_decide_move", self, Vector2(last_cell.coords.x, last_cell.coords.y))
        return
  print("%s is passing" % data.name)
  emit_signal("monster_decide_pass", self)
  return
  
func ia_path_to_get_in_range(skill_id: String):
  var skill: SkillData = Data.skills[skill_id]
  var max_range = skill.target_range + skill.area_radius
  var closest_data = get_closest_foe()
  if (DataMonsters.monsters[data["name"]].behavior == "flee" || closest_data["path"].size() <= max_range):
    return []
  var path: Array = closest_data["path"].slice(0, mp)
  # remove last item with pop_back because it's the actual cell of closest foe: wa can a most go to
  # a neighboring cell
  path.pop_back()
  return path

func get_closest_foe():
  var battle = get_node("/root/Battle")
  var grid = battle.grid
  var foe_uuid = ""
  var shortest_path = []
  for battler_uuid in battle.battlers:
    if battler_uuid == uuid:
      continue
    var battler = battle.battlers[battler_uuid]
    if battler.hp <= 0:
      continue
    if battler.team == team:
      continue
    var path_to = grid.find_path(self, battler.current_cell) 
    if path_to.size() < shortest_path.size() || foe_uuid == "":
      shortest_path = path_to
      foe_uuid = battler.uuid
  return {"foe_uuid": foe_uuid, "path": shortest_path}
  
func can_launch(skill_id: String) -> bool:
  var skill = Data.skills[skill_id]
  if ap < skill.ap_cost:
    return false
  if skill_id in cooldowns:
    return false
  return true

func valid_targets(skill_id: String) -> Array:
  var skill = Data.skills[skill_id]  
  var battle = get_node("/root/Battle")
  var grid = battle.grid
  var targetable_area: Array = grid.get_area(current_cell.coords, "circle", skill.target_range)
  var targets = []
  for cell in targetable_area:
    var is_valid = false
    if skill.target_free_cell == true && cell.battler == null:
      is_valid = true
    elif skill.target_free_cell == false && cell.battler != null:
      if cell.battler.team != team:
        is_valid = true
    if is_valid:
      targets.append(cell)
  return targets
