extends Sprite
class_name Cell

const CELL_WIDTH: int = 68 
const CELL_HEIGHT: int = 42
const COLORS: Dictionary = {
  'pathing_green': "00780b",
  'green': "7cb342",
  'light_green': "d4e157",
  'brown': "827717",
  'red': 'c30000',
  'blue': '0000c3',
  'targetable_blue': '4444c3',
  'targetable_blue_hover': '0000c3',
  'area_blue': '0000ff',
 }

onready var placement_sprite = $Placement
onready var hover_sprite: Sprite = $Hover

var team_placement = null setget _set_team_placement

var battler = null
var is_obstacle: bool = false
var coords: Vector2 = Vector2(0, 0)

var checked: bool = false # bool for chekcing if already examined during pathfinding
var path_to_origin: Array = [] # array of Cells for pathfinding
var neighbors: Array = [] # array of the adjacent Cells
var is_pathing: bool = false setget _set_is_pathing # is part of current pathfinding
var is_target: bool = false setget _set_is_target # is a valid targettable cell for current armed skill
var is_free: bool = true setget ,_get_is_free
var is_aoe: bool = false setget _set_is_aoe

# Dictionary of ground effects (key: uuid) whose aura this cell is inside
var ground_effects = {}

signal cell_entered(cell)
signal cell_clicked(cell)

func _get_is_free() -> bool:
  if is_obstacle || battler != null:
    return false
  else:
    return true

func is_crossable_by(_battler):
  if is_obstacle:
    return false
  if battler!= null: # && _battler.team != battler.team:
    return false
  return true

# set which team players can chose this cell as starting point (can be null
func _set_team_placement(value):
  team_placement = value
  if value == null:
    placement_sprite.visible = false
  if value == 0:
    placement_sprite.visible = true
    placement_sprite.self_modulate = Color(COLORS.blue)
  if value == 1:
    placement_sprite.visible = true
    placement_sprite.self_modulate = Color(COLORS.red)
  

# set if cell is being used in path calculation, change appearance of hover sprite
func _set_is_pathing(value):
  is_pathing = value
  if is_pathing:
    hover_sprite.self_modulate = Color(COLORS.pathing_green)
  hover_sprite.visible = value

# set if cell is being targeted, change appearance of hover sprite
func _set_is_target(value):
  is_target = value
  hover_sprite.self_modulate = Color(COLORS.targetable_blue)
  hover_sprite.visible = value
  
func _set_is_aoe(value: bool):
  is_aoe = value
  if is_aoe:
    hover_sprite.self_modulate = Color(COLORS.area_blue)
    hover_sprite.visible = true
  else:
    if is_target:
      hover_sprite.self_modulate = Color(COLORS.targetable_blue)
      hover_sprite.visible = true
    else:
      hover_sprite.visible = false


func initialize(_coords: Vector2, is_column_end: bool, color_name: String) -> void:
#  # coordonées et calcul de position de la cell
  coords = _coords
#  # TODO retirer après tests
  $Label.text = str(coords.x) + ',' + str(coords.y)
  var col = coords.x
  var row = coords.y
  # sets pixel position on grid depending on coordinates and 
  position = Vector2( (col - row) * CELL_WIDTH / 2, (col + row) * CELL_HEIGHT / 2)
  $Bottom.visible = is_column_end
  if COLORS.keys().has(color_name):
    self_modulate = Color(COLORS[color_name])
  else:
    print ('Warning! color_name %s is not in COLORS' % color_name  )

func _on_mouse_entered():
  emit_signal('cell_entered', self)

func _on_mouse_exited():
  if is_target:
    hover_sprite.self_modulate = Color(COLORS.targetable_blue)

func _on_input_event(_viewport, event, _shape_idx):
  if event.is_pressed() && event.button_index == BUTTON_LEFT:
    emit_signal('cell_clicked', self)

