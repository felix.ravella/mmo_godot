extends PanelContainer

onready var greyscale_shader = preload("res://tres/shaders/greyscale.tres")

onready var icon: TextureRect = $VBoxContainer/Icon
onready var progress_bar = $VBoxContainer/ProgressBar
onready var level_label = $VBoxContainer/ProgressBar/LevelPanel/Label



var skill_data: SkillData
var skill_level = 1
var floor_xp_value = 0
var current_xp = 0
var ceil_xp_value = 0

func initialize(tree_index: int = 0, skill_index: int = 0):
  var tree_name: String = Data.jobs[GlobalStore.hero["job"]].skill_trees[tree_index]
  # DEV: if skills isn't yet added to list, default
  if Data.skill_trees[tree_name].skills.size() <= skill_index:
    return
  var skill_id: String = Data.skill_trees[tree_name].skills[skill_index]
  
  if not Data.skills.has(skill_id):
    print("ERROR! Skill icon received unknown skill_id '%s'" % skill_id)
    return
  skill_data = Data.skills[skill_id]
  var skill_texture = load("res://assets/icons/skills/%s.png" % skill_data.id)
  icon.texture = skill_texture
  # compute min value
  current_xp = GlobalStore.hero["skill_%d%d_xp" % [tree_index, skill_index]]
#  current_xp = 61
  skill_level = GlobalStore.hero.get_skill_lvl(current_xp)
  floor_xp_value = GlobalStore.hero.get_total_skill_xp_to_next(skill_level-1)

  ceil_xp_value = GlobalStore.hero.get_total_skill_xp_to_next(skill_level)
  progress_bar.min_value = floor_xp_value
  progress_bar.max_value = ceil_xp_value
  progress_bar.value = current_xp
  level_label.text = str(skill_level)
  if GlobalStore.hero.get_unlocked_skills().has(skill_id):
    icon.material = null
  else:
    icon.material = ShaderMaterial.new()
    icon.material.shader = greyscale_shader

func _on_icon_mouse_entered():
  # TODO DEV verification. remove in PROD
  if !!skill_data:
    Tooltip.show_skill(skill_data.id, rect_global_position)

func _on_icon_mouse_exited():
  Tooltip.hide()
  
func _on_progress_bar_mouse_entered():
  Tooltip.show_raw_content("%d / %d" % [current_xp, ceil_xp_value], rect_global_position)
  
func _on_progress_bar_mouse_exited():
  Tooltip.hide()
  
#func _ready():
#  initialize(0,0)
