extends Node2D
class_name GroundEffect

const CELL_SIZE = Vector2(68, 42)
const TOP_RIGHT = Vector2(34, 21)
const BOTTOM_RIGHT = Vector2(-34, 21) 
const BOTTOM_LEFT = Vector2(-34, -21) 
const TOP_LEFT = Vector2(34, -21)

const default_visual_scene = preload("res://entities/ground_effect/visuals/default.tscn")

onready var type_label: Label = $TypeLabel
onready var duration_label: Label = $DurationLabel

var uuid: String
var caster: Battler
var duration: int
var type: String
var power: float

var affected_cells: Array = []

# object-scope variables used for path drawing
#var draw_head = Vector2.ZERO
#var path = []

func _initialize(_uuid, _caster, _type: String, _affected_cells: Array, _duration: int, _power: float) -> void:
  uuid = _uuid
  caster = _caster
  type = _type
  affected_cells = _affected_cells
  power = _power
  duration = _duration
  type_label.text = "type: %s" % type
  duration_label.text = "time: %d" % duration
#  get_boundaries_path(radius)
  # load visual scene depending on type
  var visual_scene = null
  if ResourceLoader.exists("res://entities/ground_effect/visuals/%s.tscn" % type):
    visual_scene = load("res://entities/ground_effect/visuals/%s.tscn" % type)
  else :
    visual_scene = default_visual_scene
  # instanciate visual
  for cell in affected_cells:
    var new_visual = visual_scene.instance()
    add_child(new_visual)
    new_visual.global_position = cell.global_position
    
#func get_boundaries_path(radius) -> void:
#  draw_head = Vector2(CELL_SIZE.x * radius/2, (CELL_SIZE.y * radius/-2) + 21)
#  path = [draw_head]
#  if radius == 0:
#    add_corner("left")
#    add_corner("right")
#  else:
#    for i in range(1, radius + 1):
#      add_corner("right")
#    add_edge(BOTTOM_LEFT)
#    for i in range(1, radius + 1):
#      add_corner("bottom")
#    add_edge(TOP_LEFT)
#    for i in range(1, radius + 1):
#      add_corner("left")
#    add_edge(TOP_RIGHT)
#    for i in range(1, radius + 1):
#      add_corner("top")
#    add_edge(BOTTOM_RIGHT)
#
#func add_edge(direction: Vector2) -> void:
#  draw_head += direction
#  path.append(draw_head)
#
#func add_corner(angle) -> void:
#  match angle:
#    "top":
#      add_edge(TOP_LEFT)
#      add_edge(TOP_RIGHT)
#    "right":
#      add_edge(TOP_RIGHT)
#      add_edge(BOTTOM_RIGHT)
#    "bottom":
#      add_edge(BOTTOM_RIGHT)
#      add_edge(BOTTOM_LEFT)
#    "left":
#      add_edge(BOTTOM_LEFT)
#      add_edge(TOP_LEFT)


#func _ready():
#  global_position = Vector2(400,400)
#  _initialize("laehfa", null, "darkness", 1, 1, 100)

func _on_created():
  pass

func _on_battler_entered(battler: Battler) -> void:
  pass

func _on_battler_left(battler: Battler) -> void:
  pass

func _on_battler_start_turn(battler: Battler) -> void:
  pass
  
func _on_battler_end_turn(battler: Battler) -> void:
  pass

func _on_removed():
  pass
