extends Label

const group_button_scene: PackedScene = preload("res://entities/group_button/group_button.tscn")

onready var container = $VBoxContainer
onready var groups_container = $Label/GroupList
onready var http_request = $HTTPRequest

signal other_players_list_received(player_ids)
signal room_data_received(data)


# players
func display_list(ids):
  for child in container.get_children():
    child.queue_free()
  for id in ids:
    var new_label = Label.new()
    new_label.text = "id: %s" % id
    container.add_child(new_label)
    
  # groups
func display_groups(groups):
  for child in groups_container.get_children():
    child.queue_free()
  for item in groups:
    var new_group_button = group_button_scene.instance()
    groups_container.add_child(new_group_button)
    new_group_button.initialize(item.id, item.data)
    
func _ready():
  pass
  
func request_list():
  var headers = ["Content-Type: application/json"]
  # get room users
#  var url = "http://localhost:3000/rooms/%s/users" % GlobalData.current_map
  # get room groups
#  var url = "http://localhost:3000/rooms/%s/groups" % GlobalData.current_map
  var url = "http://localhost:3000/rooms/%s" % GlobalStore.current_map
  http_request.connect("request_completed", self, "_on_request_completed")
  var resp = http_request.request(url, headers, false, HTTPClient.METHOD_GET)
  
func _on_request_completed(result, response_code, headers, body):
  if response_code != 200:
    print("error!")
  else: 
    var response = parse_json(body.get_string_from_utf8())
    response.playerIds.erase(GlobalData.user.id)
    emit_signal("room_data_received", response)
    display_list(response.playerIds)
    display_groups(response.groups)
    
#    response.erase(GlobalData.user.id)
#    display_list(response)
#    emit_signal("other_players_list_received", response)
