extends TextureButton

onready var illustration: TextureRect = $Illutration
onready var name_label: Label = $NameLabel
onready var info_label: Label = $InfoLabel


var character_data = null

signal character_button_pressed(data)

func initialize(data):
  character_data = data
  name_label.text = data.name
  var texture = load("res://assets/illustrations/character_%s.png" % [data.job])
  illustration.texture = texture
  var job_label = Data.jobs[data.job].label
  var level = get_player_level(data.xp)
  info_label.text = "%s Level  %d" % [job_label, level]
  
func _on_pressed():
  emit_signal("character_button_pressed", character_data)
  

func get_player_level(total_xp):
  var lvl = 1
  var aggregated_xp = 0
  while total_xp > get_player_xp_to_next(lvl):
    lvl += 1
  return lvl
  
func get_player_xp_to_next(level: int):
  if level <= 1:
    return 100
  return 100*pow(1.5, level-1) + 75*(level-1)
  
