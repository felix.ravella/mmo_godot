extends TextureButton
class_name InventoryItem

const quality_star_scene: PackedScene = preload("res://entities/inventory_item/quality_star.tscn")

onready var icon_texture: TextureRect = $IconTexture
onready var quantity_panel: Panel = $IconTexture/QuantityPanel
onready var quantity_label: Label = $IconTexture/QuantityPanel/QuantityLabel
onready var quality_stars_container = $IconTexture/QualityStarsContainer

var index
var instance_data
var item_type
# set to true after dropped
var dropped_on_target: bool = false

signal clicked(item_index)

func _initialize(_index: int, _item_type: String, _instance_data: Dictionary) -> void:
  index = _index
  instance_data = _instance_data
  item_type = _item_type
  var texture_path
  match item_type:
    "equipable":
      texture_path = "res://assets/icons/equipables/%s.png" % instance_data.blueprint
    "resource":
      quantity_panel.show()
      quantity_label.text = "%d" % instance_data.quantity
      for i in range(instance_data.quality):
        var new_star = quality_star_scene.instance()
        quality_stars_container.add_child(new_star)
      texture_path = "res://assets/icons/resources/%s.png" % instance_data.blueprint
  if ResourceLoader.exists(texture_path):
    icon_texture.texture = load(texture_path)

func get_drag_data(position):
  if not dropped_on_target:
    set_drag_preview(_get_preview_control())
  return self
  
func _get_preview_control() -> Control:
  """
    Must not be in the scene tree. Must not be freed, it will be automatically after drag.
    Do not keep a reference beyond the drag.
  """
  var preview: TextureRect = TextureRect.new()
  preview.texture = icon_texture.texture
  preview.expand = true
  preview.stretch_mode = TextureRect.STRETCH_SCALE_ON_EXPAND
  preview.rect_size = Vector2(80,80)
  preview.modulate.a = 0.5
  return preview

func _on_pressed():
  emit_signal("clicked", index)

  
func _on_mouse_exited():
  ItemTooltip.hide()
#  print("should hide tooltip: %s" % instance_data.blueprint)


func _on_gui_input(event: InputEvent):
  if event is InputEventMouseMotion:
    if not ItemTooltip.visible:
      if item_type == "resource":
        ItemTooltip.show_resource_tooltip(instance_data, rect_global_position)
      if item_type == "equipable":
        ItemTooltip.show_equipment_tooltip(instance_data, rect_global_position)
