extends TextureButton

onready var icon_texture: TextureRect = $IconTexture

export(Enums.Slot) var slot
var instance_data = null

signal unequip_requested(location, equipment_instance)
signal draggable_dropped(location, item_instance)

func can_drop_data(position, node):
  if node is InventoryItem:
    if node.item_type == "equipable":
      if DataItems.equipables[node.instance_data.blueprint].slot == slot:
        return true
  return false
  
func drop_data(position, node: InventoryItem):
  emit_signal('draggable_dropped', slot, node.instance_data)

func _on_mouse_exited():
  ItemTooltip.hide()

func _on_gui_input(event):
  if event is InputEventMouseButton:    
    if event.button_index == BUTTON_RIGHT && !event.is_pressed():
      emit_signal("unequip_requested", slot)
      ItemTooltip.hide()
  if event is InputEventMouseMotion:
    if ItemTooltip.visible == false && instance_data != null:
      ItemTooltip.show_equipment_tooltip(instance_data, rect_global_position)
