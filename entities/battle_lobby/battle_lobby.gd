extends Area2D
class_name BattleLobby

onready var names_container = $NamesContainer
onready var locked_label = $LockedLabel


var uuid: String
var horde_data

# ensure contact isn't triggered before init and placing done
var is_active = false
var status: String = "open"

signal lobby_clicked(lobby)

func initialize(_uuid, _battle_data, _position):
  uuid = _uuid
  horde_data = _battle_data
  for item in horde_data.horde:
    var monster = DataMonsters.monsters[item.id]
    var new_label = Label.new()
    new_label.text = monster.battler_data.name
    names_container.add_child(new_label)
  position = _position
  is_active = true

func _set_status(value: String) -> void:
  # TODO
  status = value
  $LockedLabel.visible = (status == "locked")

func _on_clicked():
  emit_signal('lobby_clicked', self)
