extends Control

onready var progress_bar: ProgressBar = $ProgressBar

var skill_id: String
var current_xp: float = 0.0
var xp_gain: int = 0
var displayed_xp = 0 setget _set_displayed_xp

func _set_displayed_xp(value: float):
  progress_bar.value = value

func initialize(_skill_id: String, gain: int) -> void:
  skill_id = _skill_id
  var skill: SkillData = Data.skills[skill_id]
  xp_gain = gain
  $Icon.texture = load("res://assets/icons/skills/%s.png" % skill_id)
  $SkillNameLabel.text = skill.label
  
  var hero = GlobalStore.hero
  current_xp = hero.get("skill_%s_xp" % skill.skill_code)
  var current_level: int = hero.get_skill_lvl(current_xp)
  var floor_xp: int = hero.get_total_skill_xp_to_next(current_level-1)
  var to_next_xp: int = hero.get_total_skill_xp_to_next(current_level)
  progress_bar.min_value = floor_xp
  progress_bar.max_value = to_next_xp
  self.displayed_xp = current_xp
  $Gainlabel.text = "+ %d" % xp_gain
  
func display_gain_tween():
  $Tween.interpolate_property(self, "displayed_xp",
    current_xp, current_xp + xp_gain, 0.5,
    Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
  $Tween.start()
  
  
  
  
