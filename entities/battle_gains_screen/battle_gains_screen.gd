extends CanvasLayer

const skill_gain_display_scene: PackedScene = preload("skill_gain_display/skill_gain_display.tscn")
const loot_item_scene: PackedScene = preload("res://entities/inventory_item/loot_item.tscn")

onready var loot_grid_container: GridContainer = $Panel/LootGridContainer

# called from map script of GlobalStore.battle_gains is not empty
func initialize() -> void:
  var gains = GlobalStore.battle_gains
  var update_payload = {}
  # display gains data stored in GlobalStore at the end of battle
  $Panel/ResultLabel.text = "VICTORY" if gains.result == "win" else "DEFEAT"
  $Panel/XpLabel.text = "Xp: %d" % gains.xp_gain
  update_payload["xp"] = GlobalStore.hero.xp + gains.xp_gain
  for skill_id in gains.skill_xp:
    var skill_code = Data.skills[skill_id].skill_code
    var total_skill_xp = GlobalStore.hero.get("skill_%s_xp" % skill_code) + gains.skill_xp[skill_id]
    update_payload["skill_%s_xp" % skill_code] = total_skill_xp
    var new_skill_gain = skill_gain_display_scene.instance()
    $Panel/SkillGainsHolder.add_child(new_skill_gain)
    new_skill_gain.initialize(skill_id, gains.skill_xp[skill_id])
  
  for item in gains.loot[GlobalStore.hero.uuid]:
    print("looted: ", item)
    var new_loot_item = loot_item_scene.instance()
    loot_grid_container.add_child(new_loot_item)
    # giving -1 for index since it won't be used in battle gains display
    new_loot_item._initialize(-1, "resource", item)
    
  show()
  
  $HTTPRequest.connect("request_completed", self, "_on_update_request_completed")
  var headers = ["Content-Type: application/json"]
  var url = "%s/players/%s" % [Data.server_url, GlobalStore.hero.uuid]
  var payload = JSON.print(update_payload)
  var _resp = $HTTPRequest.request(url, headers, false, HTTPClient.METHOD_PATCH, payload)
  
  yield(get_tree().create_timer(.5), "timeout")  
  for child in $Panel/SkillGainsHolder.get_children():
    yield(get_tree().create_timer(.6), "timeout")
    child.display_gain_tween()
  
func _on_update_request_completed(_result, _response_code, _headers, body):
  var _response = parse_json(body.get_string_from_utf8())
  GlobalStore.load_hero(_response)

func _on_close_button_pressed():
  hide()
