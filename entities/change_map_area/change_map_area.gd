extends Area2D
class_name ChangeMapArea

export var destination_map: String
export var destination_position: Vector2

func _ready():
  $Label.text = destination_map
