extends HBoxContainer

const STAT_LABELS = {
  "max_hp": "Health",
  "strength": "Strength",
  "agility": "Agility",
  "intelligence": "Intelligence",
  "willpower": "Willpower",
  "influence": "Influence"
 }

onready var icon_texture: TextureRect = $IconTexture
onready var stat_name_label: Label = $StatNameLabel 
onready var value_label: Label = $ValueLabel 

var stat = ""

func _initialize(stat_name: String):
  stat = stat_name
  var stat_texture = load("res://assets/icons/misc/%s.png" % stat)
  if !!stat_texture:
    icon_texture.texture = stat_texture
  stat_name_label.text = "%s" % STAT_LABELS[stat]
  value_label.text = "%d" % GlobalStore.hero.get_total_stat(stat)
  
  
