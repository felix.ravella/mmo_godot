extends TextureButton
class_name SkillButton

onready var cost_panel = $CostPanel
onready var cost_label = $CostPanel/Label
onready var cooldown_panel = $CooldownPanel
onready var cooldown_label = $CooldownPanel/Label

var skill: SkillData
# store battler in case it someday possible to control other battler than self player
var battler: Battler 

signal skill_button_clicked(button)

# store battler in case it someday possible to control other battler than self player
func initialize(skill_id, _battler):
  battler = _battler
  skill = Data.skills[skill_id]
  var texture = load("res://assets/icons/skills/%s.png" % skill_id)
  texture_normal = texture
  cost_label.text = "%s AP" % skill["ap_cost"]
  refresh_display()
 
func refresh_display():
  var skill_id = skill.id
  var cooldown = 0
  if skill_id in battler.cooldowns:
    cooldown = battler.cooldowns[skill_id]
  cooldown_panel.visible = cooldown > 0
  cooldown_label.text = str(cooldown)
  self_modulate = Color.white if battler.can_launch(skill_id) else Color.gray
  disabled = !battler.can_launch(skill_id)
 
func _on_mouse_entered():
  Tooltip.show_skill(skill.id, rect_global_position, Vector2(90, -130))
  
func _on_mouse_exited():
  Tooltip.hide()

func _on_pressed():
  emit_signal("skill_button_clicked", skill.id)
