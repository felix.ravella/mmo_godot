extends KinematicBody2D
class_name Player

const MAX_SPEED = 250
const MOVE_SPEED = 0.1

#onready var move_tween: Tween = $MoveTween
onready var visual_sprite: AnimatedSprite = $Visual
onready var uuid_label = $Visual/UUIDLabel
onready var name_label = $Visual/NameLabel
onready var navigation_agent = $NavigationAgent2D

var uuid = ""
var data = ""

var did_arrive = false
var velocity = Vector2.ZERO

signal change_map_area_touched(destination)
signal horde_touched(uuid)
signal lobby_touched(uuid)

signal path_changed(path)
signal target_reached


func initialize(_uuid: String, _data):
  uuid = _uuid
  data = _data
  # uuid_label.text = uuid
  var frames_path = "res://tres/battler_frames/%s.tres" % data.job
  if ResourceLoader.exists(frames_path):
    visual_sprite.frames = load(frames_path)
  visual_sprite.animation = "move_down"
  name_label.text = data.name
  var new_pos = Vector2(data.position_x, data.position_y)
  position = new_pos
  set_target_location(new_pos)
  # data_label.text = "data: %s" % data

func set_target_location(target:Vector2) -> void:
  did_arrive = false
  navigation_agent.set_target_location(target)

func _physics_process(_delta):
  if not visible:
    return
  
  var move_direction = position.direction_to(navigation_agent.get_next_location())
  velocity = move_direction * MAX_SPEED
  #look_at_direction(move_direction)
  #_play_animation(state)
  navigation_agent.set_velocity(velocity)


#func _input(event):
#  if event is InputEventMouseButton and event.pressed:
#    var target_location = get_global_mouse_position()
#    set_target_location(target_location)

func _on_NavigationAgent2D_velocity_computed(safe_velocity):
  if not navigation_agent.is_navigation_finished():
    visual_sprite.playing = true    
    velocity = move_and_slide(safe_velocity)
    visual_sprite.animation = "move_down" if velocity.normalized().y > 0.0 else "move_up"
    visual_sprite.flip_h = true if velocity.normalized().x > 0.0 else false
  elif not did_arrive:
    did_arrive = true
    visual_sprite.playing = false
    visual_sprite.frame = 0
    emit_signal("path_changed", [])
    emit_signal("target_reached")

func _on_NavigationAgent2D_path_changed():
  emit_signal("path_changed", navigation_agent.get_nav_path())

func _on_area_entered(area):
  if area is ChangeMapArea:
    emit_signal("change_map_area_touched", area.destination_map, area.destination_position)
  if area is Horde:
    if not area.is_active: return
    emit_signal("horde_touched", area.uuid)
  if area is BattleLobby:
    if not area.is_active: return
    if area.status == "locked": return
    emit_signal("lobby_touched", area.uuid)
