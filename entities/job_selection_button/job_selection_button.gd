extends TextureButton

var job_id: String

signal job_button_pressed(job_name)

func initialize(jobData: Job):
  job_id = jobData.id
  $TextureRect.texture = load("res://assets/illustrations/character_%s.png" % job_id)
  $JobNameLabel.text = jobData.label


func _on_pressed():
  emit_signal("job_button_pressed", job_id)
