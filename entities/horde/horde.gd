extends Area2D
class_name Horde

const MOVE_SPEED = 1.0

onready var move_tween: Tween = $MoveTween
onready var visual_sprite = $Visual
onready var names_container = $NamesContainer

var uuid = ""
var data = ""

# ensure contact isn't triggered before init and placing done
var is_active = false

signal change_map_area_touched(destination)
signal horde_clicked(horde)

func initialize(_uuid, _data, _position):
  uuid = _uuid
  data = _data
  for item in data.horde:
    var monster = DataMonsters.monsters[item.id]
    var new_label = Label.new()
    new_label.text = monster.battler_data.name
    names_container.add_child(new_label)
  position = _position
  is_active = true

func move_to(destination):
  move_tween.stop(self)
  move_tween.interpolate_property(self, "position",
        position, destination, 1,
        Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
  move_tween.start()

func _on_clicked():
  emit_signal('horde_clicked', self)
