extends CanvasLayer

var colors: Dictionary = {
  "damage": Color.red,
  "heal": Color.green,
  "ap": Color.blue,
  "mp": Color.green,
 }

func initialize(value_string: String, position: Vector2, type: String = "damage") -> void:
  $Holder.global_position = position
  $Holder/Label.text = value_string
  $Holder/Label.add_color_override("font_color", colors[type])
  $AnimationPlayer.connect("animation_finished", self, "destroy")
  $AnimationPlayer.play("display_value")
  
func destroy(_animation_name):
  queue_free()
