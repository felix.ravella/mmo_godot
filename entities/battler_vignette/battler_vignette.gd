extends Control

const greyscale_shader = preload("res://tres/shaders/greyscale.tres")

onready var hp_bar: ProgressBar = $TextureButton/HPBar
onready var battler_texture: TextureRect = $TextureButton/BattlerTexture

var uuid: String = ""

func initialize(_uuid: String, battler: Battler) -> void:
  uuid = _uuid
  var texture_path = "res://assets/battler_vignettes/%s.png" % battler.data.sprite
  if !ResourceLoader.exists(texture_path):
    texture_path = "res://assets/battler_vignettes/default.png"
  var texture = load(texture_path)
  battler_texture.texture = texture
  hp_bar.max_value = battler.data.max_hp
  hp_bar.value = battler.hp
  battler.connect("values_changed", self, "_on_values_changed")

func _on_values_changed(battler: Battler) -> void:
  hp_bar.max_value = battler.data.max_hp
  hp_bar.value = battler.hp
  if battler.hp <= 0:
    battler_texture.material = ShaderMaterial.new()
    battler_texture.material.shader = greyscale_shader
