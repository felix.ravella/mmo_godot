extends RichTextLabel

func initialize(head: String, body: String) -> void:
  bbcode_text = "[b]%s[/b]: %s" % [head, body]
  

func _ready() -> void:
  initialize("hero", "this is some body for a message")
