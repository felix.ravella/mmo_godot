extends Node

# Our WebSocketClient instance
var _client = WebSocketClient.new()


signal ws_connection_completed
signal chat_message_received(author_uuid, message_data)
signal player_joined(uuid)
signal player_left(uuid)
signal player_moved(uuid, destination)
signal horde_added(uuid, data, position)
signal horde_removed(uuid)
signal battle_added(uuid, data, position)
signal battle_removed(uuid)
signal player_joined_lobby(uuid, battler_data, position, team)
signal player_changed_start_position(uuid, target)
signal player_changed_ready_status(uuid, value)
signal battle_started
signal battler_moved(battler_uuid, destination)
signal battler_launched_skill(battler_uuid, skill_id, target, random_seed)
signal turn_ended(battler_uuid)
signal battle_ended(result)

func connect_ws():
  #var err = _client.connect_to_url("ws://192.168.1.44:3000/ws/%s" % GlobalStore.user.player_uuid)
  var err = _client.connect_to_url("%s/ws/%s" % [Data.server_url, GlobalStore.user.player_uuid])
  
  if err != OK:
    print("Unable to connect")
    set_process(false)

func _ready():
  _client.connect("connection_closed", self, "_closed")
  _client.connect("connection_error", self, "_error")
  _client.connect("connection_established", self, "_connected")
  _client.connect("data_received", self, "_on_data")
  

func _process(_delta):
    # Call this in _process or _physics_process. Data transfer, and signals
    # emission will only happen when calling this function.
    _client.poll()
    
# ["lws-mirror-protocol"]
#func _send():
#  _client.get_peer(1).put_packet(JSON.print({"message": "Test packet"}).to_utf8())
  
# build payload from type and content, and sends it
func _send_packet(type: String, content): 
  var payload = {
    "author": {
      "uuid": GlobalStore.user.player_uuid,
      "current_map": GlobalStore.current_map
    },
    "type": type,
    "content": content
   }
  _client.get_peer(1).put_packet(JSON.print(payload).to_utf8())


func _error():
  Heartbeat.error_numbers += 1
  print("connection error; %d" % Heartbeat.error_numbers)

func _closed(was_clean = false):
  # was_clean will tell you if the disconnection was correctly notified
  # by the remote peer before closing the socket.
  print("Closed, clean: ", was_clean)
  Heartbeat.stop_heartbeat()
  set_process(false)

func _connected(proto = ""):
    # This is called on connection, "proto" will be the selected WebSocket
    # sub-protocol (which is optional)
    print("Connected with protocol: ", proto)
    Heartbeat.start_heartbeat()
#    var data = JSON.print(message)
#    _client.get_peer(1).set_write_mode(WebSocketPeer.WRITE_MODE_TEXT)
#    _client.get_peer(1).put_var(data)
    # You MUST always use get_peer(1).put_packet to send data to server,
    # and not put_packet directly when not using the MultiplayerAPI.
#    print("Test packet".to_utf8())
#    print(message)
#    _client.get_peer(1).put_packet("Test packet".to_utf8())

func _on_data():
    # Print the received packet, you MUST always use get_peer(1).get_packet
    # to receive data from server, and not get_packet directly when not
    # using the MultiplayerAPI.
    var payload = JSON.parse(_client.get_peer(1).get_packet().get_string_from_utf8()).result
    
    #print("Got data from server: ", payload)
    if not payload.has("type"):
      print("Warning! no type in received ws message")
      return
      
    match payload.type:
      'connection_granted':
        print("ws connection completed")
        emit_signal("ws_connection_completed")
      'game_message':
        print("GAME MESSAGE: %s" % payload.content)
      'server_message':
        print("SERVER MESSAGE: %s" % payload.content)
      'chat_message':
        emit_signal("chat_message_received", payload.content.uuid, payload.content.message_data)
      'map_joined':
        GlobalStore.current_map = payload.content
        #GlobalStore.current_position = 
        get_tree().change_scene("res://scenes/maps/%s/%s.tscn" % [payload.content, payload.content])
      'player_joined':
        emit_signal("player_joined", payload.content.uuid, payload.content.data)
      'player_left':
        emit_signal("player_left", payload.content)
      'player_moved':
        emit_signal("player_moved", payload.content.uuid, payload.content.coords)
      'horde_added':
        emit_signal("horde_added", payload.content.uuid, payload.content.data, Vector2(0,0))
      'horde_removed':
        emit_signal("horde_removed", payload.content)
      'battle_added':
        emit_signal("battle_added", payload.content.uuid, payload.content.battleData.horde, Vector2(0,0))        
      'battle_removed':
        emit_signal("battle_removed", payload.content)
      'battle_granted':
        GlobalStore.battle_uuid = payload.content.uuid
        GlobalStore.battle_data = payload.content.battleData
        GlobalStore.battle_team = payload.content.team
        GlobalStore.is_battle_owner = true
        get_tree().change_scene("res://scenes/battle/battle.tscn")
      'lobby_joined':
        GlobalStore.battle_uuid = payload.content.uuid        
        GlobalStore.battle_data = payload.content.battleData
        GlobalStore.battle_team = payload.content.team
        GlobalStore.is_battle_owner = false
        get_tree().change_scene("res://scenes/battle/battle.tscn")
      'player_joined_lobby':
        emit_signal("player_joined_lobby", payload.content.uuid, payload.content.battler_data, payload.content.hero_data, payload.content.position, payload.content.team)        
      'player_change_start_position':
        emit_signal("player_changed_start_position", payload.content.uuid, payload.content.target)
      'battle_started':
        # every instances set RNG to same seed generated by server
        GameManager.RNG.seed = payload.content.seed
        emit_signal('battle_started')
      'player_changed_ready':
        emit_signal('player_changed_ready_status', payload.content.uuid, payload.content.value)
      'battler_moved':
        emit_signal('battler_moved', payload.content.battler_uuid, payload.content.destination)
      'battler_launched_skill':
        emit_signal('battler_launched_skill', payload.content.battler_uuid, payload.content.skill_id, payload.content.target, payload.content.random_seed)
      'turn_ended':
        emit_signal('turn_ended', payload.content.battler_uuid)
      'battle_ended':
        emit_signal("battle_ended", payload.content)
      'heartbeat':
        #print("heartbeat received from server")
        Heartbeat.is_live = true
        pass
      _:
        print("WARNING! Unknown websocket message receive: %s" % payload.type)
        # TODO
        
        

