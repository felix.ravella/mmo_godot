extends SkillEffect
class_name Slash

func launch(caster: Battler, target_cells: Array, random_seed):
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_00_xp"])
  var mastery = get_mastery(caster, 0)

  var damages = int(4 * (1.00 + mastery / 100.0))
  for cell in target_cells:
    if cell.battler != null:
      cell.battler.undergo_attack(caster, damages)
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var skill: SkillData = Data.skills["slash"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_00_xp"])
  var mastery = 0
  for skill_index in range(0, 3):
    mastery += GameManager.get_skill_level(GlobalStore.hero["skill_00_xp"])
  var damages = int(4 * (1.00 + mastery / 100.0))
  
  return "%d PA, Range %d.\nDeals %d damages to a single target" % [skill.ap_cost, skill.target_range, damages]
