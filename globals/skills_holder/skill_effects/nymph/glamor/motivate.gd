extends SkillEffect
class_name Motivate

func launch(caster: Battler, target_cells: Array, random_seed):
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_12_xp"])
  var mastery = get_mastery(caster, 1)
  
#  var base_damage = int(5 + level / 10.0)
  var mastery_ratio = 1.00 + mastery / 100.0
#  var damages = int(base_damage * mastery_ratio)
  
  var base_duration = 3
  
  var base_power = 100 + level 
  var total_power = int(base_power * caster.get_status_boost_ratio())
  var buff_value = int(10 * (total_power / 100.0))
  var buff_affixes = [
    { "attribute": "strength", "value": buff_value, "origin": "Motivate (%s)" % caster.data.name },
    { "attribute": "agility", "value": buff_value, "origin": "Motivate (%s)" % caster.data.name },
    { "attribute": "intelligence", "value": buff_value, "origin": "Motivate (%s)" % caster.data.name },
    { "attribute": "influence", "value": buff_value, "origin": "Motivate (%s)" % caster.data.name },
    { "attribute": "willpower", "value": buff_value, "origin": "Motivate (%s)" % caster.data.name },
  ]
  
  for cell in target_cells:
    # check if battler exist before each effect (disapears on die or battle end)
    if cell.battler != null:
      if cell.battler.team == caster.team:
        cell.battler.add_status("modified_attribute", caster, total_power, base_duration, buff_affixes)
  # TODO add status
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var skill: SkillData = Data.skills["motivate"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_12_xp"])

#  var base_damage = int(8 + level / 10.0)
  var base_power = 100 + level
  var buff_value = int(10 * 1 + (base_power / 100.0))
  var damage_reduction_percentage = int(base_power / 10.0)
  
  var base_duration = 3
  
  var text = skill.get_description_header()
  text += "\nGrants +%d to all attribute for all allies in a 2 cell radius (%d turns)." % [buff_value, base_duration]
  
  return text
