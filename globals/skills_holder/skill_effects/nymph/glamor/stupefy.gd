extends SkillEffect
class_name Stupefy

var min_roll = 2
var max_roll = 5

func launch(caster: Battler, target_cells: Array, random_seed):
  GameManager.RNG.seed = random_seed
  var random_damage = GameManager.RNG.randi_range(min_roll, max_roll)
  
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_11_xp"])
  var mastery = get_mastery(caster, 1)
  
  var base_damage = int(random_damage + level / 10.0)
  var mastery_ratio = 1.00 + mastery / 100.0
  var stat_ratio = 1.0 + caster.get_total_stat("influence") / 100.0
  var damages = int(base_damage * mastery_ratio)
  
  var base_duration = 2
  
  var base_power = 150
  var bonus_power = level / 3.0
  var total_power = int((base_power + bonus_power) * caster.get_status_boost_ratio())
  var debuff_affixes = [{
    "attribute": "intelligence",
    "value": int(-5 *  total_power / 100.0),
    "origin": "Stupefy (%s)" % caster.data.name,
  },
  {
    "attribute": "willpower",
    "value": int(-5 *  total_power / 100.0),
    "origin": "Stupefy (%s)" % caster.data.name,
   }]
  
  for cell in target_cells:
    # check if battler exist before each effect (disapears on die or battle end)
    if cell.battler != null:
      cell.battler.undergo_attack(caster, damages)
    if cell.battler != null:    
      cell.battler.add_status("modified_attribute", caster, total_power, base_duration, debuff_affixes)
      cell.battler.add_status("charmed", caster, total_power, base_duration)
  
  # TODO add status
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var skill: SkillData = Data.skills["stupefy"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_10_xp"])

  var level_damage = int(level / 10.0)
  var base_power = int(150 + level / 3.0)
  var debuff_value = int(5 *  base_power / 100.0)
  var damage_reduction_percentage = int(base_power / 10.0)
  
  var base_duration = 2
  
  var text = skill.get_description_header()
  text += "\nInflicts %d-%d damages, reduces Intelligence and Willpower by %d." % [min_roll + level_damage, max_roll + level_damage, debuff_value]
  text += "\nThe target can become charmed for %d turns, reducing the damages they deal to the caster by %d%%." % [base_duration, damage_reduction_percentage]
  
  return text
