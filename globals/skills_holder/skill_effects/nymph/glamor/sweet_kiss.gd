extends SkillEffect
class_name SweetKiss

var min_roll = 6
var max_roll = 10

func launch(caster: Battler, target_cells: Array, random_seed):
  GameManager.RNG.seed = random_seed
  var random_value = GameManager.RNG.randi_range(min_roll, max_roll)
  
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_13_xp"])
  var mastery = get_mastery(caster, 1)
  
  var base_heal = int(random_value + level / 10.0)
  var mastery_ratio = 1.00 + mastery / 100.0
  var stat_ratio = 1.0 + caster.get_total_stat("influence") / 100.0
  var heal_value = int(base_heal * mastery_ratio * stat_ratio)
  var base_power = int(100 + level *2)
  var total_power = int(base_power * caster.get_status_boost_ratio())
  var stat_loss = int(1 * total_power/100.0)
  
  for cell in target_cells:
    var battler = cell.battler
    if battler != null:
      if battler.team == caster.team:
        battler.heal(heal_value)
      else:
        battler.mp -= stat_loss
        battler.ap -= stat_loss

  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var skill: SkillData = Data.skills["sweet_kiss"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_13_xp"])
  
  var level_heal = int(level / 10.0)
  var power = int(100 + level *2)
  var stat_loss = int(1 * power/100.0)
  
  var text = skill.get_description_header()
  text += "\nIf target is an ally, heals for %d-%d health points." % [min_roll + level_heal, max_roll + level_heal]
  text += "\nIf it is a foe, removes %d move point and %d action point" % [stat_loss, stat_loss]
  return text
