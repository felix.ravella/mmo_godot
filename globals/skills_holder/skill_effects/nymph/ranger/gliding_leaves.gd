extends SkillEffect
class_name GlidingLeaves

func launch(caster: Battler, target_cells: Array, random_seed):
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_22_xp"])
  var mastery = get_mastery(caster, 2)
  
#  var base_damage = int(3 + level / 10.0)
#  var mastery_ratio = 1.00 + mastery / 100.0
#  var damages = int(base_damage * mastery_ratio)
  
  var battle = get_node("/root/Battle")
  var spawn_cell = caster.current_cell
  var power = 100 + level
  var buff_power = int(power * caster.get_status_boost_ratio())
  var buff_duration = 2
  var buff_affixes = [
    { "attribute": "agility", "value": int(10 *  buff_power / 100.0) },
    { "attribute": "willpower", "value": int(10 *  buff_power / 100.0) },
  ]
  
  caster.teleport_to(target_cells[0])
  caster.add_status("modified_attribute", caster, buff_power, buff_duration, buff_affixes)
  
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var skill: SkillData = Data.skills["gliding_leaves"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_22_xp"])
  
  var power = int(100 + level)
  var buff_value = int(10 * power / 100.0)
  var buff_duration = 2
  
  var text = skill.get_description_header()
  text += "\nTeleports to target cell. Gain +%d Agility and Willpower for %d turns." % [buff_value, buff_duration]
  
  return text
