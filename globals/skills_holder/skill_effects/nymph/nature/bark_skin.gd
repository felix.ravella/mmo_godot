extends SkillEffect
class_name BarkSkin

func launch(caster: Battler, target_cells: Array, random_seed):
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_03_xp"])
  var mastery = get_mastery(caster, 0)

  var mastery_ratio = 1.00 + (mastery * 1) / 100.0
  var base_armor = int(5 + level / 4.0)
  var total_armor = int(base_armor * mastery_ratio)

  # var bonus_duration = int(level / 30.0)
  for cell in target_cells:
    if cell.battler != null:
      cell.battler.armor += base_armor
  
  # TODO add status
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var skill: SkillData = Data.skills["bark_skin"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_02_xp"])
    
  var base_armor = int(5 + level / 4.0)

  var text = skill.get_description_header()
  text += "\nGrants %d armor to a single target." % base_armor
  
  return text
