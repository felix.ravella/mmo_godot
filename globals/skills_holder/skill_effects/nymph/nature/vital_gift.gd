extends SkillEffect
class_name VitalGift

var min_roll = 3
var max_roll = 5

func launch(caster: Battler, target_cells: Array, random_seed):
  GameManager.RNG.seed = random_seed
  var random_value = GameManager.RNG.randi_range(min_roll, max_roll)
  
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_00_xp"])
  var mastery = get_mastery(caster, 0)
  
  var base_heal = int(random_value + level / 10.0)
  var mastery_ratio = 1.00 + mastery / 100.0
  var stat_ratio = 1.0 + caster.get_total_stat("willpower") / 100.0
  var heal_value = int(base_heal * mastery_ratio * stat_ratio)
  
  for cell in target_cells:
    var battler = cell.battler
    if battler != null:
      battler.heal(heal_value)
      if battler is PlantBattler:
        battler.growth += 1
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var skill: SkillData = Data.skills["vital_gift"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_00_xp"])
  
  var level_heal = int(level / 10.0)
  
#  var text = "%d PA, Range %d." % [skill.ap_cost, skill.target_range]
  var text = skill.get_description_header()
  text += "\nHeals for %d-%d health points." % [min_roll + level_heal, max_roll + level_heal]
  text += "\nIf target is a plant, it accelerates its growth." 
  
  return text
