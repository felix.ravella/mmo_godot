extends SkillEffect
class_name ThornyGrove

func launch(caster: Battler, target_cells: Array, random_seed):
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_01_xp"])
  var mastery = get_mastery(caster, 0)
  
  var base_power = 100 + level *2 + mastery
  var invoc_power = int(base_power * caster.get_status_boost_ratio())

  var battle = get_node("/root/Battle")
  var spawn_position = Vector2(target_cells[0].coords.x, target_cells[0].coords.y)

  battle.add_plant_battler("thorny_grove", caster, spawn_position, invoc_power)
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var skill: SkillData = Data.skills["thorny_grove"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_01_xp"])

  var text = skill.get_description_header()
  text += "\nPlants a spiked sprout."
  text += "\nStage 1: hurts anyone attacking it."
  text += "\nStage 2: spits thorns (range 2) and surround itself with thorny ground." 
  text += "\nStage 3: spits thorns (range 3), larger thorny ground." 
  
  return text
