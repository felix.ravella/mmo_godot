extends SkillEffect
class_name AlphasHowl

func launch(caster: Battler, target_cells: Array, random_seed):
  # compute level and mastery
#  var base_damage = int(3)
  
  var base_armor = 8
  var base_power = 100
  var base_duration = 2
  
  for cell in target_cells:
    # check if battler exist before each effect (disapears on die or battle end)
    if cell.battler != null:
      if cell.battler.team == caster.team:
        cell.battler.armor += base_armor
        cell.battler.add_status("lethal", caster, base_power, base_duration)
  
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  return "This is a monster-only skill; I you see this, something went wrong"
