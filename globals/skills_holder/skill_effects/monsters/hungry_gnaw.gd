extends SkillEffect
class_name HungryGnaw

var min_roll = 1
var max_roll = 4

func launch(caster: Battler, target_cells: Array, random_seed):
  GameManager.RNG.seed = random_seed
  var base_damage = GameManager.RNG.randi_range(min_roll, max_roll)
  
  # TODO remove when monster stats implemented
  if caster.data.name == "wolf":
    base_damage += 1
  if caster.data.name == "white_wolf":
    base_damage += 4
  
  for cell in target_cells:
    # check if battler exist before each effect (disapears on die or battle end)
    if cell.battler != null:
      cell.battler.undergo_attack(caster, base_damage)
  
  # TODO add status
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  return "This is a monster-only skill; I you see this, something went wrong"
