extends SkillEffect
class_name Kindling

var min_roll = 5
var max_roll = 8

func launch(caster: Battler, target_cells: Array, random_seed):
  GameManager.RNG.seed = random_seed
  var random_value = GameManager.RNG.randi_range(min_roll, max_roll)
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_12_xp"])
  var mastery = get_mastery(caster, 0)
  
  var base_damage = int(random_value + level / 10.0)
  var mastery_ratio = 1.00 + mastery / 100.0
  var stat_ratio = 1.0 + caster.get_total_stat("intelligence") / 100.0
  var damages = int(base_damage * mastery_ratio * stat_ratio)
  
  var base_duration = 3
  #var bonus_duration = int(level / 30.0)
  var base_power = 200
  var bonus_power = level / 2.0
  var total_power = int((base_power + bonus_power) * caster.get_dot_boost_ratio())
  
  for cell in target_cells:
    # check if battler exist before each effect (disapears on die or battle end)
    if cell.battler != null:
      if cell.battler.has_status("burning"):
        cell.battler.undergo_attack(caster, damages)
      else:    
        cell.battler.add_status("burning", caster, total_power, base_duration)

func get_description() -> String:
  var skill: SkillData = Data.skills["kindling"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_12_xp"])
    
  var level_damage = int(level / 10.0)
  
  var text = skill.get_description_header()
  text += "\nIf target is burning, deals %d-%d damages." % [min_roll + level_damage, max_roll + level_damage]
  text += "If not, inflicts burning status instead."
  return text
