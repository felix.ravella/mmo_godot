extends SkillEffect
class_name ScorchedEarth

func launch(caster: Battler, target_cells: Array, random_seed):
  GameManager.RNG.seed = random_seed
#  var random_value = GameManager.RNG.randi_range(min_roll, max_roll)
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_11_xp"])
  var mastery = get_mastery(caster, 2)
  
  var mastery_ratio = 1.00 + mastery / 100.0
  
  var battle = get_node("/root/Battle")
  var spawn_cell = target_cells[0]
  var base_radius = 2
  var base_duration = 3
  var base_power = 100 + level * 2
  var total_power = int(base_power * caster.get_status_boost_ratio())
  
  battle.add_ground_effect(spawn_cell, caster, "scorched_earth", base_radius, base_duration, total_power)
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var skill: SkillData = Data.skills["scorched_earth"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_11_xp"])
  
  var level_damage = int(level / 10.0)
  
  var text = skill.get_description_header()
  text += "\nCreate burning ground on a 2-cell radius zone, inflicting burning status to anyone entering inside."
    
  return text
