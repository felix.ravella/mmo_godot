extends SkillEffect
class_name Purification

var min_heal_roll = 4
var max_heal_roll = 6
var min_roll = 6
var max_roll = 10

func launch(caster: Battler, target_cells: Array, random_seed):
  GameManager.RNG.seed = random_seed
  var random_heal_value = GameManager.RNG.randi_range(min_heal_roll, max_heal_roll)
  var random_damage_value = GameManager.RNG.randi_range(min_roll, max_roll)
  
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_23_xp"])
  var mastery = get_mastery(caster, 1)
  
  var base_heal = int(random_heal_value + level / 10.0)
  var base_damage = int(random_damage_value + level / 10.0)
  var mastery_ratio = 1.00 + mastery / 100.0
  var heal_stat_ratio = 1.0 + caster.get_total_stat("influence") / 100.0  
  var damage_stat_ratio = 1.0 + caster.get_total_stat("willpower") / 100.0
  var heal_value = int(base_heal * mastery_ratio * heal_stat_ratio)
  var damage_value = int(base_damage * mastery_ratio * heal_stat_ratio)
  var base_power = int(100 + level *2)
  var total_power = int(base_power * caster.get_status_boost_ratio())
  var remove_number = int(1 + level / 33)
  
  
  for cell in target_cells:
    var battler = cell.battler
    if battler != null:
      if battler.team == caster.team:
        for i in range(remove_number):
          var indexes: Array = battler.get_harmful_status_indexes()
          if indexes.size() > 0:
            battler.remove_status(indexes[0])
        battler.heal(heal_value)
      else:
        for i in range(remove_number):
          var indexes: Array = battler.get_harmless_status_indexes()
          if indexes.size() > 0:
            battler.remove_status(indexes[0])
        cell.battler.undergo_attack(caster, damage_value)
                

  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var skill: SkillData = Data.skills["purification"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_23_xp"])
  
  var level_boost = int(level / 10.0)
  var power = int(100 + level *2)
  var remove_number = int(1 + level / 33)
  
  var text = skill.get_description_header()
  text += "\nIf target is an ally, removes up to %d harmful status, and heals for %d-%d health points." % [remove_number, min_heal_roll + level_boost, max_heal_roll + level_boost]
  text += "\nIf it is a foe, removes beneficial status and inflicts  %d-%d damages instead" % [min_roll + level_boost, max_roll + level_boost]
  return text
