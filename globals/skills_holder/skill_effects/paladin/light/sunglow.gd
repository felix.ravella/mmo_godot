extends SkillEffect
class_name Sunglow

var min_roll = 5
var max_roll = 8

func launch(caster: Battler, target_cells: Array, random_seed):
  GameManager.RNG.seed = random_seed
  var random_value = GameManager.RNG.randi_range(min_roll, max_roll)
  
  var battle: Battle = get_node("/root/Battle")
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_21_xp"])
  var mastery = get_mastery(caster, 1)
  
  var base_damage = int(random_value + level / 10.0)
  var mastery_ratio = 1.00 + mastery / 100.0
  var stat_ratio = 1.0 + caster.get_total_stat("intelligence") / 100.0
  var damages = int(base_damage * mastery_ratio * stat_ratio)
  
  var boost_cells = battle.grid.get_area(caster.current_cell.coords, "circle", 1)
  var base_duration = 3
  var base_power = 100 + level 
  var total_power = int(base_power * caster.get_status_boost_ratio())
  var buff_value = int(5 * (total_power / 100.0))
  var buff_affixes = [
    { "attribute": "willpower", "value": buff_value, "origin": "Sunglow (%s)" % caster.data.name },
  ]
  
  for cell in target_cells:
    # check if battler exist before each effect (disapears on die or battle end)
    if cell.battler != null:
      cell.battler.undergo_attack(caster, damages)
  for cell in boost_cells:
    if cell.battler != null:
      if cell.battler.team == caster.team:
        cell.battler.add_status("modified_attribute", caster, total_power, base_duration, buff_affixes)
  
  # TODO add status
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")


func get_description() -> String:
  var skill: SkillData = Data.skills["sunglow"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_21_xp"])

  var level_damage = int(level / 10.0)
  var base_power = 100 + level
  var buff_value = int(5 * 1 + (base_power / 100.0))
  
  var base_duration = 3
  
  var text = skill.get_description_header()
  text += "\nInflicts %d-%d damages to a single target." % [min_roll + level_damage, max_roll + level_damage]
  text += "\nGrants +%d willpower (%d turns) to caster and adjacent allies." % [buff_value, base_duration]
  
  return text
