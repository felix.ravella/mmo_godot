extends SkillEffect
class_name ForgeBoon

var min_roll = 2
var max_roll = 3

func launch(caster: Battler, target_cells: Array, random_seed):
  GameManager.RNG.seed = random_seed
  var random_value = GameManager.RNG.randi_range(min_roll, max_roll)
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_02_xp"])
  var mastery = get_mastery(caster, 0)

  var base_armor = int(5 + level / 10.0)
    
  var mastery_ratio = 1.00 + mastery / 100.0
  
  var base_duration = 2
  var base_power = 100 + level 
  var total_power = int(base_power * caster.get_status_boost_ratio())
  var buff_value = int(10 * (total_power / 100.0))
  var buff_affixes = [
    { "attribute": "strength", "value": buff_value, "origin": "Forge Boon (%s)" % caster.data.name },
  ]


  for cell in target_cells:
    # check if battler exist before each effect (disapears on die or battle end)
    if cell.battler != null:
      if cell.battler.team == caster.team:
        cell.battler.add_status("modified_attribute", caster, total_power, base_duration, buff_affixes)
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")
  

func get_description() -> String:
  var skill: SkillData = Data.skills["forge_boon"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_02_xp"])
    
  var base_armor = int(5 + level / 10.0)
  var base_power = 100 + level
  var buff_value = int(10 * 1 + (base_power / 100.0))
  var duration = 2
  
  var text = "%d PA, Range %d, CD %d" % [skill.ap_cost, skill.target_range, skill.cooldown]
  text += "\nBoost allies in a 2-cell radius area, giving %d armor and +%d for %d turns" % [base_armor, buff_value, duration]
  
  return text
