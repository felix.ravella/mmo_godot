extends SkillEffect
class_name MetalShards

var min_roll = 2
var max_roll = 3

func launch(caster: Battler, target_cells: Array, random_seed):
  GameManager.RNG.seed = random_seed
  var random_value = GameManager.RNG.randi_range(min_roll, max_roll)
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_00_xp"])
  var mastery = get_mastery(caster, 0)

  var base_damage = int(random_value + level / 10.0)
  var base_armor = int(2 + level / 10.0)
  var mastery_ratio = 1.00 + mastery / 100.0
  var stat_ratio = 1.0 + caster.get_total_stat("strength") / 100.0
  var damages = int(base_damage * mastery_ratio * stat_ratio)

  for cell in target_cells:
    if cell.battler != null:
      cell.battler.undergo_attack(caster, damages)
      # add armor only if target exists, and if caster is still alive
      if caster != null:
        caster.armor += base_armor
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")
  

func get_description() -> String:
  var skill: SkillData = Data.skills["metal_shards"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_00_xp"])
    
  var level_damage = int(level / 10.0)
  var base_armor = int(2 + level / 10.0)  
  
  var text = "%d PA, Range %d." % [skill.ap_cost, skill.target_range]
  text += "\nDeals %d-%d damages to a single target" % [min_roll + level_damage, max_roll + level_damage]
  text += "\nGain %d armor" % [base_armor]
  
  return text
