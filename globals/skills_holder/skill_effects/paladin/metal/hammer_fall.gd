extends SkillEffect
class_name HammerFall

var min_roll = 4
var max_roll = 8

func launch(caster: Battler, target_cells: Array, random_seed):
  GameManager.RNG.seed = random_seed
  var random_damage = GameManager.RNG.randi_range(min_roll, max_roll)
  
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_03_xp"])
  var mastery = get_mastery(caster, 1)
  
  var base_damage = random_damage + level / 10.0
  var bonus_damage = caster.armor / 5.0
  var mastery_ratio = 1.00 + mastery / 100.0
  var stat_ratio = 1.0 + caster.get_total_stat("strength") / 100.0
  var damages = int((base_damage + bonus_damage) * mastery_ratio)

  var base_duration = 2
  
  var base_power = 100
  var bonus_power = level / 3.0
  var total_power = int((base_power + bonus_power) * caster.get_status_boost_ratio())
  var debuff_affixes = [{
    "attribute": "strength",
    "value": int(-10 *  total_power / 100.0),
    "origin": "Hammer Fall (%s)" % caster.data.name,
  }]

  for cell in target_cells:
    # check if battler exist before each effect (disapears on die or battle end)
    if cell.battler != null:
      cell.battler.add_status("modified_attribute", caster, total_power, base_duration, debuff_affixes)
      cell.battler.undergo_attack(caster, damages)

func get_description() -> String:
  var skill: SkillData = Data.skills["stupefy"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_10_xp"])

  var level_damage = int(level / 10.0)
  var base_power = int(100 + level / 3.0)
  var debuff_value = int(10 *  base_power / 100.0)
  
  var base_duration = 2
  
  var text = skill.get_description_header()
  text += "\nInflicts %d-%d damages in a 1-cell radius zone (+1 damage for each 5 points of armor the caster has)" % [min_roll + level_damage, max_roll + level_damage]
  text += "reduces Strength by %d (%d turns)." % [debuff_value, base_duration]
  
  return text
