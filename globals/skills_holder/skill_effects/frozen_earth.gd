extends SkillEffect
class_name FrozenEarth

func launch(caster: Battler, target_cells: Array, random_seed):
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_10_xp"])
  var mastery = get_mastery(caster, 1)
  

  var battle = get_node("/root/Battle")
  var spawn_cell = target_cells[0]
  var base_radius = Data.skills["frozen_earth"].area_radius
  var radius_bonus = int(mastery / 40)
  var power = 100 + int(mastery/2)
  battle.add_ground_effect(spawn_cell, caster, "frost", base_radius + radius_bonus, 2, power)
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var skill: SkillData = Data.skills["frozen_earth"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_10_xp"])
  var mastery = 0
  for skill_index in range(0, 3):
    mastery += GameManager.get_skill_level(GlobalStore.hero["skill_10_xp"])
  #var damages = int(4 * (1.00 + mastery / 100.0))
  
  return "%d PA, Range self.\nCreate frozen ground aroud the hero, reducing foes AP." % [skill.ap_cost]
