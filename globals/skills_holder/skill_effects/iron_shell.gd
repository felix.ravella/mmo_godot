extends SkillEffect
class_name IronShell


func launch(caster: Battler, target_cells: Array, random_seed):
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_10_xp"])
  var mastery = get_mastery(caster, 1)

  var armor_gain = int(2)
  caster.armor += armor_gain
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_10_xp"])
  var mastery = 0
  for skill_index in range(0, 3):
    mastery += GameManager.get_skill_level(GlobalStore.hero["skill_10_xp"])
    
  var armor = int(2)
  return "Grants %d armor to the hero" % [armor]
