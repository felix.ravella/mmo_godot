extends SkillEffect
class_name BloodRage

func launch(caster: Battler, target_cells: Array, random_seed):
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_20_xp"])
  var mastery = get_mastery(caster, 2)
  
  var bonus_duration = int(level / 30.0)
  var bonus_power = int(level / 2.0) 
  for cell in target_cells:
    if cell.battler != null:
      cell.battler.add_status("blood_rage", caster, 100 + bonus_power, 3 + bonus_duration)
  
  # TODO add status
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_20_xp"])
  var mastery = 0
  for skill_index in range(0, 3):
    mastery += GameManager.get_skill_level(GlobalStore.hero["skill_20_xp"])
    
  var turns = int(1)
  return "Enrages the hero for %d, boosting their strength and reducing their defenses" % [turns]
