extends SkillEffect
class_name Chase

func launch(caster: Battler, target_cells: Array, random_seed):
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_02_xp"])
  var mastery = get_mastery(caster, 0)
  
  var base_duration = 2
  # var bonus_duration = int(level / 30.0)
  var mastery_ratio = 1.00 + (mastery * 1) / 100.0
  var base_power = 100
  var bonus_power = level / 2.0
  var total_power = int((base_power + bonus_power) * caster.get_status_boost_ratio())
  
  for cell in target_cells:
    if cell.battler != null:
      cell.battler.add_status("lethal", caster, total_power, base_duration)
  
  # TODO add status
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var skill: SkillData = Data.skills["chase"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_02_xp"])
    
  var total_effect = int(10 * (1.0 + level / 100))
  var turns = int(2)
  
  var text = skill.get_description_header()
  text += "\nGain 1 PM."
  text += "\nGrants status Lethal giving +%s%% crit chances (%s turns)" % [total_effect, turns]
  
  return text
