extends SkillEffect
class_name MercuryFumes

func launch(caster: Battler, target_cells: Array, random_seed):
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_12_xp"])
  var mastery = get_mastery(caster, 1)

  var base_armor = int(5 + level / 4.0)

  var base_duration = 2
  var mastery_ratio = 1.00 + mastery / 100.0
  var base_power = int(100 + level / 2.0)
  var total_power = int(base_power * caster.get_status_boost_ratio())
  
  var buff_affixes = [{
    "attribute": "strength", 
    "value": int(5 *  total_power / 100.0), 
    "origin": "Mercury fumes (%s)" % caster.data.name,  
  }]
  
  # var bonus_duration = int(level / 30.0)
  for cell in target_cells:
    if cell.battler != null:
      cell.battler.armor += base_armor
    if cell.battler != null:
      cell.battler.add_status("mercury_fumes", caster, total_power, base_duration, buff_affixes)
  
  # TODO add status
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var skill: SkillData = Data.skills["mercury_fumes"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_12_xp"])
    
  var base_armor = int(5 + level / 4.0)
  var base_power = 100 + int(level / 2.0)
  var stat_gain = int(5 * base_power / 100.0)
  
  var text = skill.get_description_header()
  text += "\nGain %d armor" % base_armor
  text += "\nGain +%d Agility and Intelligence (2 turns)." % stat_gain
  return text
