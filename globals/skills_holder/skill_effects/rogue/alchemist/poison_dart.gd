extends SkillEffect
class_name PoisonDart

var min_roll = 1
var max_roll = 3

func launch(caster: Battler, target_cells: Array, random_seed):
  GameManager.RNG.seed = random_seed
  var random_value = GameManager.RNG.randi_range(min_roll, max_roll)
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_10_xp"])
  var mastery = get_mastery(caster, 1)
  
  var base_damage = int(random_value + level / 20.0)
  var mastery_ratio = 1.0 + mastery / 100.0
  var stat_ratio = 1.0 + caster.get_total_stat("intelligence") / 100.0
  var damages = int(base_damage * mastery_ratio * stat_ratio)
  
  var base_duration = 2
  #var bonus_duration = int(level / 30.0)
  var base_power = 100
  var bonus_power = level / 2.0
  var total_power = int((base_power + bonus_power) * caster.get_dot_boost_ratio())
  
  for cell in target_cells:
    # check if battler exist before each effect (disapears on die or battle end)
    if cell.battler != null:
      cell.battler.undergo_attack(caster, damages)
    if cell.battler != null:    
      cell.battler.add_status("poison_dart", caster, total_power, base_duration)
  
  # TODO add status
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var skill: SkillData = Data.skills["poison_dart"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_10_xp"])
    
  var level_damage = int(level / 20.0)
  var duration = 2
  var power = 100 + level * 2
  var dot_damage = int(2 * power / 100.0)
  
  var text = skill.get_description_header()
  text += "\nDeals %d-%d damages to a single target." % [min_roll + level_damage, max_roll + level_damage] 
  text += "\nInflicts Poison Dart status for %s turns (%d Damage per turn)" % [duration, dot_damage]
  
  return text
