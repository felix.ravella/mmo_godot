extends SkillEffect
class_name SlowPoison

func launch(caster: Battler, target_cells: Array, random_seed):
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_13_xp"])
  var mastery = get_mastery(caster, 1)
  
  var mastery_ratio = 1.0 + mastery / 100.0
  
  var dot_duration = 5
  var debuff_duration = 2
  #var bonus_duration = int(level / 30.0)
  var base_power = 100 + level * 2
  var dot_power = int(base_power * caster.get_dot_boost_ratio())
  var debuff_power = int(base_power * caster.get_status_boost_ratio())
  var debuff_affixes = [
    { "attribute": "strength", "value": int(-5 *  debuff_power / 100.0) },
  ]
  
  for cell in target_cells:
    if cell.battler != null:    
      cell.battler.add_status("slow_poison", caster, dot_power, dot_duration)
      cell.battler.add_status("modified_attribute", caster, debuff_power, debuff_duration, debuff_affixes)
  
  # TODO add status
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var skill: SkillData = Data.skills["slow_poison"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_13_xp"])

  var power = 100 + level * 2
  var dot_damage = int(1 * power / 100.0)
  var debuff_value = int(5 * power / 100.0)  
  var dot_duration = 5
  var debuff_duration = 2
  
  var text = skill.get_description_header()
  text += "\nInflicts Slow poison status for %d turns (%d damages per turn)." % [dot_duration, dot_damage]
  text += "\nApplies -%d strength for %d turns" % [debuff_value, debuff_duration ]
  
  return text
