extends SkillEffect
class_name ObscurePuddle

var min_roll = 2
var max_roll = 6

func launch(caster: Battler, target_cells: Array, random_seed):
  GameManager.RNG.seed = random_seed
  var random_value = GameManager.RNG.randi_range(min_roll, max_roll)
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_21_xp"])
  var mastery = get_mastery(caster, 2)
  
  var base_damage = int(random_value + level / 10.0)
  var mastery_ratio = 1.00 + mastery / 100.0
  var stat_ratio = 1.0 + caster.get_total_stat("willpower") / 100.0
  var damages = int(base_damage * mastery_ratio)
  
  var battle = get_node("/root/Battle")
  var spawn_cell = target_cells[0]
  var base_radius = 0
  var base_duration = 2
  var base_power = 100 + level
  var total_power = int(base_power * caster.get_status_boost_ratio())
  
  for cell in target_cells:
    if cell.battler != null:
      cell.battler.undergo_attack(caster, damages)
  battle.add_ground_effect(spawn_cell, caster, "darkness", base_radius, base_duration, total_power)
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var skill: SkillData = Data.skills["obscure_puddle"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_21_xp"])
  
  var level_damage = int(level / 10.0)
  
  var text = skill.get_description_header()
  text += "\nDeals %d-%d damages to a single target" % [min_roll + level_damage, max_roll + level_damage]
  text += "\nCreate on their cell a Dark Zone obscuring vision"
    
  return text
