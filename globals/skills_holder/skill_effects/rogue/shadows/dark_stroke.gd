extends SkillEffect
class_name DarkStroke

var min_roll = 5
var max_roll = 8

func launch(caster: Battler, target_cells: Array, random_seed):
  GameManager.RNG.seed = random_seed
  var random_value = GameManager.RNG.randi_range(min_roll, max_roll)
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_20_xp"])
  var mastery = get_mastery(caster, 2)
  
  var base_damage = int(random_value + level / 10.0)
  var mastery_ratio = 1.00 + mastery / 100.0
  var stat_ratio = 1.0 + caster.get_total_stat("willpower") / 100.0
  var damages = int(base_damage * mastery_ratio)
  
  for cell in target_cells:
    if cell.battler != null:
      cell.battler.undergo_attack(caster, damages)
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var skill: SkillData = Data.skills["dark_stroke"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_20_xp"])
  
  var level_damage = int(level / 10.0)
  
  var text = skill.get_description_header()
  text += "\nDeals %d-%d damages to a single target" % [min_roll + level_damage, max_roll + level_damage]
    
  return text
