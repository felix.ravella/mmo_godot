extends SkillEffect
class_name ShadowMantle

func launch(caster: Battler, target_cells: Array, random_seed):
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_23_xp"])
  var mastery = get_mastery(caster, 2)
  
  
  var duration = int(3)
  var mastery_ratio = 1.00 + (mastery * 1) / 100.0
  var base_armor =  (4 + level / 4.0)
  var total_armor = int(base_armor * mastery_ratio)
  var base_power = 100 + level * 2
  var total_power = int(base_power * caster.get_status_boost_ratio())
  var buff_affixes = [
    { "attribute": "mobility", "value": int(20 *  total_power / 100.0) },
  ]
  # var bonus_power = int(level / 2.0) 
  for cell in target_cells:
    if cell.battler != null:
      cell.battler.armor += total_armor      
      cell.battler.add_status("shadow_mantle", caster, total_power, duration, buff_affixes)
  
  # TODO add status
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var skill: SkillData = Data.skills["shadow_mantle"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_23_xp"])
  
  var base_armor =  (4 + level / 4.0)  
  var base_power = 100 + level * 2
  var total_effect = int(20 * (base_power / 100))
  var duration = int(3)
  
  var text = skill.get_description_header()
  text += "\nGrants %d armor." % [base_armor]
  text += "\nGrants +%d mobility for %d turns." % [total_effect, duration]
  return text
   
   
