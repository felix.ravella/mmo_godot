extends SkillEffect
class_name ShadowStep

func launch(caster: Battler, target_cells: Array, random_seed):
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_22_xp"])
  var mastery = get_mastery(caster, 2)
  
#  var base_damage = int(3 + level / 10.0)
#  var mastery_ratio = 1.00 + mastery / 100.0
#  var damages = int(base_damage * mastery_ratio)
  
  var battle = get_node("/root/Battle")
  var spawn_cell = caster.current_cell
  var base_radius = 1
  var base_duration = 2
  var base_power = 100 + level
  var total_power = int(base_power * caster.get_status_boost_ratio())
  
  caster.teleport_to(target_cells[0])
  battle.add_ground_effect(spawn_cell, caster, "darkness", base_radius, base_duration, base_power)
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var skill: SkillData = Data.skills["shadow_step"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_22_xp"])
  
  var text = skill.get_description_header()
  text += "\nTeleports to a cell nearby, creating Dark Zone on the starting position and the cells around it." 
  
  return text
