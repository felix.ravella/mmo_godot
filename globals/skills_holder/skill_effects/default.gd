extends SkillEffect
class_name Default

var min_roll = 3
var max_roll = 4

func launch(caster: Battler, target_cells: Array, random_seed):
  GameManager.RNG.seed = random_seed
  var random_value = GameManager.RNG.randi_range(min_roll, max_roll) 
  var damages = random_value
  for cell in target_cells:
    if cell.battler != null:
      cell.battler.undergo_attack(caster, damages)
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")
  
