extends SkillEffect
class_name SummonImp

func launch(caster: Battler, target_cells: Array, random_seed):
  # compute level and mastery
  var level = GameManager.get_skill_level(caster.hero_data["skill_20_xp"])
  var mastery = get_mastery(caster, 0)
  

  var battler_data = { 
    "name": "imp", 
    "sprite": "imp",
    "max_ap": 3,
    "max_mp": 2,
    "max_hp": 5,
    "skills": ["default"]
  }
  var spawn_position = Vector2(target_cells[0].coords.x, target_cells[0].coords.y)
  var battle = get_node("/root/Battle")
  battle.summon(battler_data, spawn_position, caster)
  yield(get_tree().create_timer(0.2), "timeout")
  .emit_signal("skill_effect_finished")

func get_description() -> String:
  var skill: SkillData = Data.skills["summon_imp"]
  var level = GameManager.get_skill_level(GlobalStore.hero["skill_20_xp"])
  var mastery = 0
  for skill_index in range(0, 3):
    mastery += GameManager.get_skill_level(GlobalStore.hero["skill_20_xp"])
  #var damages = int(4 * (1.00 + mastery / 100.0))
  
  return "%d PA, Range %d.\nSummon an imp to fight for the hero." % [skill.ap_cost, skill.target_range]
