extends Node
class_name SkillEffect

signal skill_effect_finished

func get_mastery(caster, tree_index):
  var mastery = 0
  for skill_index in range(0, 4):
    var field_name = "skill_%d%d_xp" % [tree_index, skill_index]
    mastery += caster.hero_data.get_skill_lvl(caster.hero_data[field_name])
  return mastery

func get_description() -> String:
  return 'TODO'
