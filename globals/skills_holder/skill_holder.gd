# this node exist as holder for all skill classes
# it also grant access to scene tree to those classes, for exemple for using timers and yields
extends Node

onready var default = $Default
# tree 'swordman'
onready var slash = $Slash
# tree 'armors'
onready var iron_shell = $IronShell
# tree 'berserker'
onready var blood_rage = $BloodRage
# tree 'frost'
onready var frozen_earth = $FrozenEarth
# tree 'summon'
onready var summon_imp = $SummonImp

# rogue
# tree 'assasisn'
onready var throw_dagger = $Rogue/Assassin/ThrowDagger
onready var opening_strike = $Rogue/Assassin/OpeningStrike
onready var chase = $Rogue/Assassin/Chase
onready var exploit_weakness = $Rogue/Assassin/ExploitWeakness
# tree 'alchemist'
onready var poison_dart = $Rogue/Alchemist/PoisonDart
onready var explosive_flask = $Rogue/Alchemist/ExplosiveFlask
onready var mercury_fumes = $Rogue/Alchemist/MercuryFumes
onready var slow_poison = $Rogue/Alchemist/SlowPoison
# tree 'shadows'
onready var dark_stroke = $Rogue/Shadows/DarkStroke
onready var obscure_puddle = $Rogue/Shadows/ObscurePuddle
onready var shadow_step = $Rogue/Shadows/ShadowStep
onready var shadow_mantle = $Rogue/Shadows/ShadowMantle

# rogue
# tree 'nature'
onready var vital_gift = $Nymph/Nature/VitalGift
onready var thorny_grove = $Nymph/Nature/ThornyGrove
onready var bark_skin = $Nymph/Nature/BarkSkin
onready var toxic_mycelium = $Nymph/Nature/ToxicMycelium
# tree 'glamor'
onready var mental_strike = $Nymph/Glamor/MentalStrike
onready var stupefy = $Nymph/Glamor/Stupefy
onready var motivate = $Nymph/Glamor/Motivate
onready var sweet_kiss = $Nymph/Glamor/SweetKiss
# tree 'ranger'
onready var wild_arrow = $Nymph/Ranger/WildArrow
onready var tracking_arrow = $Nymph/Ranger/TrackingArrow
onready var gliding_leaves = $Nymph/Ranger/GlidingLeaves
onready var rain_of_arrows = $Nymph/Ranger/RainOfArrows

# paladin
# tree 'metal'
onready var metal_shards = $Paladin/Metal/MetalShards
onready var sweeping_blade = $Paladin/Metal/SweepingBlade
onready var forge_boon = $Paladin/Metal/ForgeBoon
onready var hammer_fall = $Paladin/Metal/HammerFall
# tree 'fire'
onready var fire_ray = $Paladin/Fire/FireRay
onready var scorched_earth = $Paladin/Fire/ScorchedEarth
onready var kindling = $Paladin/Fire/Kindling
onready var flaming_circle = $Paladin/Fire/FlamingCircle
# tree 'light'
onready var blinding_light = $Paladin/Light/BlindingLight
onready var sunglow = $Paladin/Light/Sunglow
onready var holy_candle = $Paladin/Light/HolyCandle
onready var purification = $Paladin/Light/Purification



# monsters skills
onready var shredding_fang = $Monsters/ShreddingFang
onready var toxic_stinger = $Monsters/ToxicStinger
onready var alphas_howl = $Monsters/AlphasHowl
onready var hungry_gnaw = $Monsters/HungryGnaw
