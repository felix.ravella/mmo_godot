extends Node
class_name DarknessEffect


func _on_battler_entered(battler: Battler, effect_instance: GroundEffect):
  #  var ap_loss = int(1.0 * (effect_instance.power / 100.0))
  #  battler.lose_ap(ap_loss)
  if not battler.has_status('blinded'):
    var duration = 1
    var power = effect_instance.power
    battler.add_status("blinded", effect_instance.caster, power, duration)
