extends Node
class_name MushroomsGroundEffect


func _on_battler_entered(battler: Battler, effect_instance: GroundEffect):
  
  var duration = 3
  #var bonus_duration = int(level / 30.0)
  var power = effect_instance.power
  var debuff_affixes = [
    { "attribute": "strength", "value": int(-5 *  power / 100.0) },
    { "attribute": "agility", "value": int(-5 *  power / 100.0) },
    { "attribute": "intelligence", "value": int(-5 *  power / 100.0) },
    { "attribute": "charm", "value": int(-5 *  power / 100.0) },
  ]
  battler.add_status("toxic_mycelium", effect_instance.caster, power, duration, debuff_affixes)
