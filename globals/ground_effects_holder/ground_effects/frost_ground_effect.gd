extends Node
class_name FrostGroundEffect


func _on_battler_entered(battler: Battler, effect_instance: GroundEffect):
  var ap_loss = int(1.0 * (effect_instance.power / 100.0))
  battler.lose_ap(ap_loss)
