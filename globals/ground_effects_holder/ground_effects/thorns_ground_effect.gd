extends Node
class_name ThornsGroundEffect


func _on_battler_entered(battler: Battler, effect_instance: GroundEffect):
  print(battler)
  print(effect_instance.caster)
  if battler != effect_instance.caster:
    var damages = int(3 * effect_instance.power / 100.0)
    battler.take_damage(damages)
