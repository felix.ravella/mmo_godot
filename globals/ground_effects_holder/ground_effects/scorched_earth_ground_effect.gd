extends Node
class_name ScorchedEarthGroundEffect

func _on_battler_entered(battler: Battler, effect_instance: GroundEffect):
  var duration = 2
  var power = effect_instance.power
  battler.add_status("burning", effect_instance.caster, power, duration)
