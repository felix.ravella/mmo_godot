extends HTTPRequest

signal seed_received(value)

func get_seed():
  var url = "%s/randomSeed" % Data.server_url
  var headers = ["Content-Type: text/plain"]
  request(url, headers, false, HTTPClient.METHOD_GET)

func _on_request_completed(result, response_code, headers, body):
  if response_code != 200:
    print("error!")
  else: 
    var response = parse_json(body.get_string_from_utf8())
    emit_signal("seed_received", int(response))
