extends Node

var server_url = "http://localhost:3000"
#var server_url = "https://mmo-b75a3ec16f3f.herokuapp.com"

var plants_stages = {
  "thorny_grove": {
    "growth_stages": [
      {"threshold": 0, "name": "thorny_sprout", "offset": Vector2(-5, -39), "scale": Vector2(0.256, 0.256)},
      {"threshold": 2, "name": "thorny_bush", "offset": Vector2(-3, -32), "scale": Vector2(0.2, 0.2)},
      {"threshold": 4, "name": "thorny_grove", "offset": Vector2(-5, -77), "scale": Vector2(0.22, 0.22)},
     ],
   },
  "toxic_mycelium": {
    "growth_stages": [
      {"threshold": 0, "name": "growing_mycelium", "offset": Vector2(0, -15), "scale": Vector2(0.3, 0.3)},
      {"threshold": 2, "name": "noxious_mycelium", "offset": Vector2(0, -20), "scale": Vector2(0.235, 0.235)},
      {"threshold": 3, "name": "toxic_mycelium", "offset": Vector2(0, -33), "scale": Vector2(0.23, 0.23)},
     ],
   },
 }

var plant_battlers_datas = {
  "thorny_sprout": { 
    "name": "Thorny sprout", 
    "sprite": "thorny_sprout",
    "max_ap": 3,
    "max_mp": 0,
    "max_hp": 15,
    "skills": []
  },
  "thorny_bush": { 
    "name": "Thorny bush", 
    "sprite": "thorny_bush",
    "max_ap": 3,
    "max_mp": 0,
    "max_hp": 20,
    "skills": []
  },
  "thorny_grove": { 
    "name": "Thorny grove", 
    "sprite": "thorny_grove",
    "max_ap": 4,
    "max_mp": 0,
    "max_hp": 30,
    "skills": []
  },
  "growing_mycelium": { 
    "name": "Growing mycelium", 
    "sprite": "growing_mycelium",
    "max_ap": 4,
    "max_mp": 0,
    "max_hp": 15,
    "skills": []
  },
  "noxious_mycelium": { 
    "name": "Noxious mycelium", 
    "sprite": "noxious_mycelium",
    "max_ap": 4,
    "max_mp": 0,
    "max_hp": 25,
    "skills": []
  },
  "toxic_mycelium": { 
    "name": "Toxic mycelium", 
    "sprite": "toxic_mycelium",
    "max_ap": 4,
    "max_mp": 0,
    "max_hp": 35,
    "skills": []
  },
}

var ground_effects: Dictionary = {
  "frost": GroundEffectData.new({"id": "frost", "label": "Frost", "multitrigger": false}),
  "darkness": GroundEffectData.new({"id": "darkness", "label": "Darkness", "multitrigger": true}),
  "thorns": GroundEffectData.new({"id": "thorns", "label": "Thorns", "multitrigger": true}),
  "mushrooms": GroundEffectData.new({"id": "mushrooms", "label": "Mushrooms", "multitrigger": false}),
  "scorched_earth": GroundEffectData.new({"id": "scorched_earth", "label": "Scorched Earth", "multitrigger": true}),
 }

var jobs: Dictionary = {
#  "test_melee": Job.new({"id": "test_melee", "label": "[test]Warrior", "skill_trees": ["swords", "armors", "berserk"]}),
#  "test_mage": Job.new({"id": "test_mage", "label": "[test]Mage", "skill_trees": ["fire", "frost", "summon"]}),
  "rogue": Job.new({"id": "rogue", "label": "Rogue", "skill_trees": ["assassin", "alchemist", "shadows"]}),
  "nymph": Job.new({"id": "nymph", "label": "Nymph", "skill_trees": ["nature", "glamor", "ranger"]}),
  "paladin": Job.new({"id": "paladin", "label": "Paladin", "skill_trees": ["metal", "fire", "light"]}),
 }

var skill_trees: Dictionary = {
  "default": SkillTree.new({"id": "default", "label": "Default", "skills": ["default"]}),
  "swords": SkillTree.new({"id": "swords", "label": "Swordman", "skills": ["slash"]}),
  "armors": SkillTree.new({"id": "armors", "label": "Armors", "skills": ["iron_shell"]}),
  "berserk": SkillTree.new({"id": "berserk", "label": "Berserker", "skills": ["blood_rage"]}),
#  "fire": SkillTree.new({"id": "fire", "label": "Fire", "skills": ["fire_ray", "default"]}),
  "frost": SkillTree.new({"id": "frost", "label": "Frost", "skills": ["frozen_earth", "default"]}),
  "summon": SkillTree.new({"id": "summon", "label": "Summon", 
    "skills": ["summon_imp"]}),
  # end test list
  # rogue
  "assassin": SkillTree.new({"id": "assassin", "label": "Assassin", 
    "skills": ["throw_dagger", "opening_strike", "chase", "exploit_weakness"],
  }),
  "alchemist": SkillTree.new({"id": "alchemist", "label": "Alchemist", 
    "skills": ["poison_dart", "explosive_flask", "mercury_fumes", "slow_poison"]}),
  "shadows": SkillTree.new({"id": "shadows", "label": "Shadow Weaver", 
    "skills": ["dark_stroke", "obscure_puddle", "shadow_step", "shadow_mantle"]}),
  # nymph
  "nature": SkillTree.new({"id": "nature", "label": "Nature", 
    "skills": ["vital_gift", "thorny_grove", "bark_skin", "toxic_mycelium"]}),
  "glamor": SkillTree.new({"id": "glamor", "label": "Glamor", 
    "skills": ["mental_strike", "stupefy", "motivate", "sweet_kiss"]}),
  "ranger": SkillTree.new({"id": "ranger", "label": "Ranger", 
    "skills": ["wild_arrow", "tracking_arrow", "gliding_leaves", "rain_of_arrows"]}),
  # paladin
  "metal": SkillTree.new({"id": "metal", "label": "Metal", 
    "skills": ["metal_shards", "sweeping_blade", "forge_boon", "hammer_fall"]}),
  "fire": SkillTree.new({"id": "fire", "label": "Fire", 
    "skills": ["fire_ray", "scorched_earth", "kindling", "flaming_circle"]}),
  "light": SkillTree.new({"id": "light", "label": "Light", 
    "skills": ["blinding_light", "sunglow", "holy_candle", "purification"]}),
 }

var skills = {
  "default": SkillData.new({
    "id": "default",
    "label": "Default",
    "ap_cost": 2,
    "cooldown": 0,
    "xp_ratio": 1.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 2,
    "target_free_cell": false,
    "modifiable_range": true,
    "straight": false,
    "need_sight": false,
    "cast_animation": "strike",
    "target_animation": "hurt",
    "skill_tree_id": "default",
    "skill_code": "00",    
   }),
  "slash": SkillData.new({
    "id": "slash",
    "label": "Slash",
    "ap_cost": 3,
    "cooldown": 0,
    "xp_ratio": 1.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 1,
    "target_free_cell": false,    
    "modifiable_range": false,
    "straight": false,
    "need_sight": false,
    "cast_animation": "strike",
    "target_animation": "hurt",
    "skill_tree_id": "swords",
    "skill_code": "00",
   }),
  "iron_shell": SkillData.new({
    "id": "iron_shell",
    "label": "Iron Shell",
    "ap_cost": 3,
    "cooldown": 2,
    "xp_ratio": 2.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 0,
    "target_free_cell": false,    
    "modifiable_range": false,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "armors",
    "skill_code": "10",
   }),
  "blood_rage": SkillData.new({
    "id": "blood_rage",
    "label": "Blood Rage",
    "ap_cost": 2,
    "cooldown": 4,
    "xp_ratio": 4.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 0,
    "target_free_cell": false,    
    "modifiable_range": false,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "berserk",
    "skill_code": "20",
   }),
  "frozen_earth": SkillData.new({
    "id": "frozen_earth",
    "label": "Frozen Earth",
    "ap_cost": 2,
    "cooldown": 0,
    "xp_ratio": 1.0,
    "area_shape": "circle",
    "area_radius": 2,
    "target_range": 3,
    "modifiable_range": false,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "frost",
    "skill_code": "10",
   }),
  "summon_imp": SkillData.new({
    "id": "summon_imp",
    "label": "Summon Imp",
    "ap_cost": 4,
    "cooldown": 4,
    "xp_ratio": 1.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 2,
    "target_free_cell": true,    
    "modifiable_range": false,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "summon",
    "skill_code": "20",
   }),
  # Rogue -> assassin
  "throw_dagger": SkillData.new({
    "id": "throw_dagger",
    "label": "Throw Dagger",
    "ap_cost": 2,
    "cooldown": 0,
    "xp_ratio": 1.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 3,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": true,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "assassin",
    "skill_code": "00",
   }),
  "opening_strike": SkillData.new({
    "id": "opening_strike",
    "label": "Opening Strike",
    "ap_cost": 4,
    "cooldown": 0,
    "xp_ratio": 1.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 2,
    "target_free_cell": false,    
    "modifiable_range": false,
    "straight": false,
    "need_sight": true,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "assassin",
    "skill_code": "01",
   }),
  "chase": SkillData.new({
    "id": "chase",
    "label": "Chase",
    "ap_cost": 1,
    "cooldown": 3,
    "xp_ratio": 5.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 0,
    "target_free_cell": false,    
    "modifiable_range": false,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "assassin",
    "skill_code": "02",
   }),
  "exploit_weakness": SkillData.new({
    "id": "exploit_weakness",
    "label": "Exploit weakness",
    "ap_cost": 5,
    "cooldown": 0,
    "xp_ratio": 1.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 1,
    "target_free_cell": false,    
    "modifiable_range": false,
    "straight": false,
    "need_sight": true,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "assassin",
    "skill_code": "03",
   }),
  # Rogue -> alchemist
  "poison_dart": SkillData.new({
    "id": "poison_dart",
    "label": "Poison dart",
    "ap_cost": 3,
    "cooldown": 0,
    "xp_ratio": 1.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 4,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": true,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "alchemist",
    "skill_code": "10",
   }),
  "explosive_flask": SkillData.new({
    "id": "explosive_flask",
    "label": "Explosive flask",
    "ap_cost": 5,
    "cooldown": 0,
    "xp_ratio": 1.0,
    "area_shape": "circle",
    "area_radius": 1,
    "target_range": 3,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "alchemist",
    "skill_code": "11",
   }),
  "mercury_fumes": SkillData.new({
    "id": "mercury_fumes",
    "label": "Mercury fumes",
    "ap_cost": 2,
    "cooldown": 3,
    "xp_ratio": 4.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 1,
    "target_free_cell": false,    
    "modifiable_range": false,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "alchemist",
    "skill_code": "12",
   }),
  "slow_poison": SkillData.new({
    "id": "slow_poison",
    "label": "Slow poison",
    "ap_cost": 1,
    "cooldown": 1,
    "xp_ratio": 1.5,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 5,
    "target_free_cell": false,    
    "modifiable_range": false,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "alchemist",
    "skill_code": "13",
   }),
  # Rogue -> shadows
  "dark_stroke": SkillData.new({
    "id": "dark_stroke",
    "label": "Dark stroke",
    "ap_cost": 4,
    "cooldown": 0,
    "xp_ratio": 1.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 4,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "shadows",
    "skill_code": "20",
   }),
  "obscure_puddle": SkillData.new({
    "id": "obscure_puddle",
    "label": "Obscure puddle",
    "ap_cost": 3,
    "cooldown": 0,
    "xp_ratio": 1.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 3,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": true,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "shadows",
    "skill_code": "21",
   }),
  "shadow_step": SkillData.new({
    "id": "shadow_step",
    "label": "Shadow step",
    "ap_cost": 3,
    "cooldown": 2,
    "xp_ratio": 2.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 3,
    "target_free_cell": true,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "shadows",
    "skill_code": "22",
   }),
  "shadow_mantle": SkillData.new({
    "id": "shadow_mantle",
    "label": "Shadow mantle",
    "ap_cost": 2,
    "cooldown": 4,
    "xp_ratio": 4.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 0,
    "target_free_cell": false,    
    "modifiable_range": false,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "shadows",
    "skill_code": "23",
   }),
  # Nymph -> nature
  "vital_gift": SkillData.new({
    "id": "vital_gift",
    "label": "Vital gift",
    "ap_cost": 2,
    "cooldown": 1,
    "xp_ratio": 3.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 4,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": true,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "nature",
    "skill_code": "00",
   }),
  "thorny_grove": SkillData.new({
    "id": "thorny_grove",
    "label": "Thorny grove",
    "ap_cost": 3,
    "cooldown": 3,
    "xp_ratio": 3.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 4,
    "target_free_cell": true,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "nature",
    "skill_code": "01",
   }),
  "bark_skin": SkillData.new({
    "id": "bark_skin",
    "label": "Bark skin",
    "ap_cost": 2,
    "cooldown": 2,
    "xp_ratio": 2.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 2,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": true,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "nature",
    "skill_code": "02",
   }),
  "toxic_mycelium": SkillData.new({
    "id": "toxic_mycelium",
    "label": "Toxic mycelium",
    "ap_cost": 4,
    "cooldown": 3,
    "xp_ratio": 3.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 4,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": true,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "nature",
    "skill_code": "03",
   }),
  # Nymph -> glamor
  "mental_strike": SkillData.new({
    "id": "mental_strike",
    "label": "Mental strike",
    "ap_cost": 3,
    "cooldown": 0,
    "xp_ratio": 1.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 2,
    "target_free_cell": false,  
    "modifiable_range": true,
    "straight": false,
    "need_sight": true,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "glamor",
    "skill_code": "10",
   }),
  "stupefy": SkillData.new({
    "id": "stupefy",
    "label": "Stupefy",
    "ap_cost": 2,
    "cooldown": 1,
    "xp_ratio": 1.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 3,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": true,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "glamor",
    "skill_code": "11",
   }),
  "motivate": SkillData.new({
    "id": "motivate",
    "label": "Motivate",
    "ap_cost": 1,
    "cooldown": 5,
    "xp_ratio": 10.0,
    "area_shape": "circle",
    "area_radius": 2,
    "target_range": 0,
    "target_free_cell": false,    
    "modifiable_range": false,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "glamor",
    "skill_code": "12",
   }),
  "sweet_kiss": SkillData.new({
    "id": "sweet_kiss",
    "label": "Sweet kiss",
    "ap_cost": 3,
    "cooldown": 1,
    "xp_ratio": 2.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 3,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": true,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "glamor",
    "skill_code": "13",
   }),
  # Nymph -> ranger
  "wild_arrow": SkillData.new({
    "id": "wild_arrow",
    "label": "Wild arrow",
    "ap_cost": 3,
    "cooldown": 0,
    "xp_ratio": 1.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 3,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": true,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "ranger",
    "skill_code": "20",
   }),
  "tracking_arrow": SkillData.new({
    "id": "tracking_arrow",
    "label": "Tracking arrow",
    "ap_cost": 2,
    "cooldown": 2,
    "xp_ratio": 2.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 5,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": true,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "ranger",
    "skill_code": "21",
    }),
  "gliding_leaves": SkillData.new({
    "id": "gliding_leaves",
    "label": "Gliding leaves",
    "ap_cost": 2,
    "cooldown": 4,
    "xp_ratio": 4.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 3,
    "target_free_cell": true,    
    "modifiable_range": false,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "ranger",
    "skill_code": "22",
   }),
  "rain_of_arrows": SkillData.new({
    "id": "rain_of_arrows",
    "label": "Rain of arrows",
    "ap_cost": 4,
    "cooldown": 0,
    "xp_ratio": 1.0,
    "area_shape": "circle",
    "area_radius": 1,
    "target_range": 4,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "ranger",
    "skill_code": "23",
   }),
  # Paladin -> metal
  "metal_shards": SkillData.new({
    "id": "metal_shards",
    "label": "Metal Shards",
    "ap_cost": 2,
    "cooldown": 0,
    "xp_ratio": 1.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 2,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": true,
    "cast_animation": "attack",
    "target_animation": "none",
    "skill_tree_id": "metal",
    "skill_code": "00",
   }),
  "sweeping_blade": SkillData.new({
    "id": "sweeping_blade",
    "label": "Sweeping blade",
    "ap_cost": 4,
    "cooldown": 0,
    "xp_ratio": 1.0,
    "area_shape": "cleave",
    "area_radius": 0,
    "target_range": 1,
    "target_shape": "square",
    "target_free_cell": false,    
    "modifiable_range": false,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "metal",
    "skill_code": "01",
   }),
  "forge_boon": SkillData.new({
    "id": "forge_boon",
    "label": "Forge boon",
    "ap_cost": 3,
    "cooldown": 3,
    "xp_ratio": 3.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 2,
    "target_free_cell": false,    
    "modifiable_range": false,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "metal",
    "skill_code": "02",
   }),
  "hammer_fall": SkillData.new({
    "id": "hammer_fall",
    "label": "Hammer Fall",
    "ap_cost": 6,
    "cooldown": 0,
    "xp_ratio": 1.0,
    "area_shape": "circle",
    "area_radius": 1,
    "target_range": 3,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "none",
    "skill_tree_id": "metal",
    "skill_code": "03",
   }),
  # Paladin -> fire
  "fire_ray": SkillData.new({
    "id": "fire_ray",
    "label": "Fire Ray",
    "ap_cost": 3,
    "cooldown": 0,
    "xp_ratio": 1.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range":3,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "hurt",
    "skill_tree_id": "fire",
    "skill_code": "10",
  }),
  "scorched_earth": SkillData.new({
    "id": "scorched_earth",
    "label": "Scorched Earth",
    "ap_cost": 5,
    "cooldown": 1,
    "xp_ratio": 2.0,
    "area_shape": "circle",
    "area_radius": 2,
    "target_range":3,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "hurt",
    "skill_tree_id": "fire",
    "skill_code": "11",
  }),
  "kindling": SkillData.new({
    "id": "kindling",
    "label": "Kindling",
    "ap_cost": 2,
    "cooldown": 3,
    "xp_ratio": 5.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 2,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "hurt",
    "skill_tree_id": "fire",
    "skill_code": "12",
   }),
  "flaming_circle": SkillData.new({
    "id": "flaming_circle",
    "label": "Flaming Circle",
    "ap_cost": 4,
    "cooldown": 0,
    "xp_ratio": 1.0,
    "area_shape": "square",
    "area_radius": 1,
    "target_range": 0,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "hurt",
    "skill_tree_id": "fire",
    "skill_code": "13",
   }),
  # Paladin -> light
  "blinding_light": SkillData.new({
    "id": "blinding_light",
    "label": "Blinding Light",
    "ap_cost": 4,
    "cooldown": 0,
    "xp_ratio": 1.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 4,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "hurt",
    "skill_tree_id": "light",
    "skill_code": "20",
   }),
  "sunglow": SkillData.new({
    "id": "sunglow",
    "label": "Sunglow",
    "ap_cost": 6,
    "cooldown": 0,
    "xp_ratio": 1.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 3,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "hurt",
    "skill_tree_id": "light",
    "skill_code": "21",
   }),
  "holy_candle": SkillData.new({
    "id": "holy_candle",
    "label": "Holy Candle",
    "ap_cost": 2,
    "cooldown": 4,
    "xp_ratio": 6.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 2,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "hurt",
    "skill_tree_id": "light",
    "skill_code": "22",
   }),
  "purification": SkillData.new({
    "id": "purification",
    "label": "Purification",
    "ap_cost": 3,
    "cooldown": 2,
    "xp_ratio": 2.0,
    "area_shape": "circle",
    "area_radius": 0,
    "target_range": 3,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": false,
    "cast_animation": "cast",
    "target_animation": "hurt",
    "skill_tree_id": "light",
    "skill_code": "23",
   }),
  # plants
  "barbed_thorn": SkillData.new({
    "id": "barbed_thorn",
    "label": "Barbed thorn",
    "ap_cost": 3,
    "cooldown": 0,
    "area_radius": 0,
    "target_range": 2,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": true,
    "cast_animation": "cast",
    "target_animation": "none",
   }),
  "barbed_dart": SkillData.new({
    "id": "barbed_dart",
    "label": "Barbed dart",
    "ap_cost": 3,
    "cooldown": 0,
    "area_radius": 0,
    "target_range": 3,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": true,
    "cast_animation": "cast",
    "target_animation": "none",
   }),
  # monsters
  "toxic_stinger": SkillData.new({
    "id": "toxic_stinger",
    "label": "Toxic stinger",
    "ap_cost": 3,
    "cooldown": 0,
    "area_radius": 0,
    "target_range": 2,
    "target_free_cell": false,    
    "modifiable_range": true,
    "straight": false,
    "need_sight": true,
    "cast_animation": "cast",
    "target_animation": "none",
   }),
  "hungry_gnaw": SkillData.new({
    "id": "hungry_gnaw",
    "label": "Hungry Gnaw",
    "ap_cost": 3,
    "cooldown": 0,
    "area_radius": 0,
    "target_range": 1,
    "target_free_cell": false,    
    "modifiable_range": false,
    "straight": false,
    "need_sight": true,
    "cast_animation": "cast",
    "target_animation": "none",
   }),
  "shredding_fang": SkillData.new({
    "id": "shredding_fang",
    "label": "Shredding Fang",
    "ap_cost": 3,
    "cooldown": 2,
    "area_radius": 0,
    "target_range": 1,
    "target_free_cell": false,    
    "modifiable_range": false,
    "straight": false,
    "need_sight": true,
    "cast_animation": "cast",
    "target_animation": "none",
   }),
  "alphas_howl": SkillData.new({
    "id": "alphas_howl",
    "label": "Alpha's Howl",
    "ap_cost": 2,
    "cooldown": 3,
    "area_radius": 2,
    "target_range": 0,
    "target_free_cell": false,    
    "modifiable_range": false,
    "straight": false,
    "need_sight": true,
    "cast_animation": "cast",
    "target_animation": "none",
   }),
 }
