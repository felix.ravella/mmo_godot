extends Panel

onready var mastery_name_label = $MasteryPanel/MasteryNameLabel
onready var mastery_level_label = $MasteryPanel/MasteryLevelLabel
onready var skill_icons_holder = $MasteryPanel/SkillIconsHolder
onready var mastery_bar = $MasteryPanel/MasteryBar

var selected_mastery_index: int = -1 setget _set_selected_mastery_index

var player_job: Job

func initialize():
  player_job = Data.jobs[GlobalStore.hero.job]
  self.selected_mastery_index = 0  
  for i in range (0, player_job.skill_trees.size()):
    var button = get_node("MasteryButtonsContainer/Mastery%dTabButton" % i)
    button.text = Data.skill_trees[player_job.skill_trees[i]].label

func _set_selected_mastery_index(value: int) -> void:  
  selected_mastery_index = value
  if selected_mastery_index == -1:
    return
  refresh_display()
  
func refresh_display() -> void:
  var skill_tree_id = player_job.skill_trees[selected_mastery_index]
  var tree_data: SkillTree = Data.skill_trees[skill_tree_id]
  var mastery_level = GlobalStore.hero.get_mastery(tree_data.id)
  mastery_name_label.text = tree_data.label
  mastery_level_label.text = "Mastery: %d" % mastery_level
  # mastery progressBar
  set_mastery_bar(mastery_level)
  
  # TODO refresh display of all children of skill_icons_holder
  var skill_icons = skill_icons_holder.get_children()
  for i in range (0, skill_icons.size()):
    skill_icons[i].initialize(selected_mastery_index, i)

func set_mastery_bar(mastery_level) -> void:
  var bars = mastery_bar.get_children()
  for i in range(bars.size()):
    var progress_bar: ProgressBar = bars[i]
    if mastery_level < progress_bar.min_value:
      progress_bar.value = 0
    elif mastery_level > progress_bar.max_value:
      progress_bar.value = progress_bar.max_value
    else:
      progress_bar.value = mastery_level
    
func _on_mastery_tab_button_pressed(index: int) -> void:
  self.selected_mastery_index = index  

func _draw():
  if selected_mastery_index != -1:
    refresh_display()
