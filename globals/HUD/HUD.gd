extends CanvasLayer

onready var background = $BackGround
onready var character_menu_panel = $CharacterMenuPanel
onready var skills_menu_panel = $SkillsMenuPanel
onready var inventory_node = $Inventory
onready var bag_panel = $Inventory/BagPanel
onready var equipment_panel = $Inventory/EquipmentPanel
onready var hp_progress: TextureProgress = $HPProgressBar
onready var hp_progress_label: Label = $HPProgressBar/Label

onready var menus = {
  "character": character_menu_panel,
  "skills": skills_menu_panel,
  "inventory": inventory_node,
 }

var displayed_menu: String = '' setget _set_displayed_menu

func _set_displayed_menu(value: String):
  background.hide()
  character_menu_panel.hide()
  skills_menu_panel.hide()
  inventory_node.hide()
  Tooltip.hide()
  ItemTooltip.hide()
  displayed_menu = value
  if value != '':
    background.show()
    menus[value].show()

# called when player is selected and loaded.
# load all specific assets (skills, battler...)
func initialize():
  skills_menu_panel.initialize()
  displayed_menu = ''
  refresh_hp_display()
  
# test
func set_index(index):
  skills_menu_panel.selected_mastery_index = index

func refresh_hp_display():
  var max_hp = GlobalStore.hero.get_total_stat("max_hp")
  var current_hp = GlobalStore.hero.hp
  hp_progress.max_value = max_hp
  hp_progress.value = current_hp
  hp_progress_label.text = "%d / %d" % [current_hp, max_hp]

func _on_equipment_list_changed() -> void:
  refresh_hp_display()

func _on_menu_button_pressed(type: String) -> void:
  if (displayed_menu == type):
    self.displayed_menu = ''
  else:
    self.displayed_menu = type

func _unhandled_key_input(event: InputEventKey):
  # get real key depending on keyboard language
  var key = OS.get_scancode_string(event.scancode)
  match key:
    "K":
      if event.pressed && !event.is_echo():
        self.displayed_menu = "skills" if displayed_menu != "skills" else ""
    "I":
      if event.pressed && !event.is_echo():
        self.displayed_menu = "inventory" if displayed_menu != "inventory" else ""
    "M":
      if event.pressed && !event.is_echo():
        if $MerchantWindow.visible == false:
          $MerchantWindow.show()
        else:
          $MerchantWindow.hide()
    "Escape":
      self.displayed_menu = ''

func _ready():
  hide()
  

