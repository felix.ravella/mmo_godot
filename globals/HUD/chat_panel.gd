extends Panel

const chat_message_scene: PackedScene = preload("res://entities/chat_message/chat_message.tscn")

onready var chat_scroll_container: ScrollContainer = $ScrollContainer
onready var scrollbar = chat_scroll_container.get_v_scrollbar()
onready var messages_holder: VBoxContainer = $ScrollContainer/MessagesHolder
onready var line_edit: LineEdit = $LineEdit

var message_text: String = "" setget _set_message_text

func _set_message_text(value) -> void:
  message_text = value
  line_edit.text = value

func add_message(head, body) -> void:
  var new_message = chat_message_scene.instance()
  messages_holder.add_child(new_message)
  new_message.initialize(head, body)
  chat_scroll_container.scroll_vertical = scrollbar.max_value

func _on_line_edit_text_changed(new_text):
  message_text = new_text

func _on_chat_message_received(_author_uuid, message_data):
  add_message(message_data.head, message_data.body)

func _ready():
  WsClient.connect("chat_message_received", self, "_on_chat_message_received")
  
func _on_text_entered(_new_text = ""):
  if message_text == "":
    return
  var hero_name = GlobalStore.hero.name
  add_message(hero_name, message_text)
  WsClient._send_packet("chat_message", {"head": hero_name, "body": message_text })
  self.message_text = ""
