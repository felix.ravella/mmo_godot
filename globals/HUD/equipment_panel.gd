extends Panel

const stat_block_scene: PackedScene = preload("res://entities/stat_block/stat_block.tscn")

onready var slots_holder = $SlotsHolder
onready var body_slot = $SlotsHolder/BodySlot
onready var foot_slot = $SlotsHolder/FootSlot
onready var update_hero_http_request = $UpdateHeroHTTPRequest
onready var stat_blocks_container = $StatBlocksContainer

var headers = ["Content-Type: application/json"]

signal equipment_list_changed

func refresh_display():
  # slots
  for node in slots_holder.get_children():
    var equiped_item = GlobalStore.hero.equiped[Enums.Slot.keys()[node.slot]]
    if !!equiped_item:
      var texture = load("res://assets/icons/equipables/%s.png" % equiped_item.blueprint)
      node.icon_texture.texture = texture
      node.instance_data = equiped_item
    else:
      node.instance_data = null
      node.icon_texture.texture = load("res://assets/icons/equipables/empty.png")
  # stat_blocks
  var stat_list = ["max_hp", "strength", "agility", "intelligence","willpower", "influence"]
  for child in stat_blocks_container.get_children():
    child._initialize(stat_list.pop_front())
        
func _on_draggable_dropped(location, item_instance):
  GlobalStore.hero.equiped[Enums.Slot.keys()[location]] = item_instance
  HUD.bag_panel.refresh_display()
  refresh_display()
  # build payload, order send patch request to server
  var field_name = "equiped_%s_uuid" % Enums.Slot.keys()[location]
  send_update_request({field_name: item_instance.uuid})
  emit_signal("equipment_list_changed")

      
func _on_unequip_requested(location):
  GlobalStore.hero.equiped[Enums.Slot.keys()[location]] = null
  HUD.bag_panel.refresh_display()
  refresh_display()
  # build payload, order send patch request to server
  var field_name = "equiped_%s_uuid" % Enums.Slot.keys()[location]
  send_update_request({field_name: null})
  emit_signal("equipment_list_changed")
  
  
func send_update_request(payload: Dictionary):
  var url = "%s/players/%s" % [Data.server_url, GlobalStore.hero.uuid]
  var json_payload = JSON.print(payload)
  var _resp = update_hero_http_request.request(url, headers, false, HTTPClient.METHOD_PATCH, json_payload)
  
func _on_update_request_completed(_result, _response_code, _headers, body):
  var _response = parse_json(body.get_string_from_utf8())
  if _response_code != 200:
    print("error while updating hero")
    return

func _draw():
  refresh_display()
  
func _ready():
  update_hero_http_request.connect("request_completed", self, "_on_update_request_completed")

