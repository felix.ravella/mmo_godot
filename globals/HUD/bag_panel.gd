extends Panel

const inventory_item_scene: PackedScene = preload("res://entities/inventory_item/inventory_item.tscn")

onready var items_grid = $ItemsGridContainer
onready var money_label = $MoneyIcon/MoneyLabel

var selected_item_type: String = "resources" setget _set_selected_item_type
var armed_item_index = null setget _set_armed_item_index

func _set_selected_item_type(value: String) -> void:
  var old_value = selected_item_type
  if old_value != value:
    selected_item_type = value
    refresh_display()
  
func _set_armed_item_index(value) -> void:
  armed_item_index = value

func refresh_display():
  # handle gold
  money_label.text = str(GlobalStore.hero.money)
  # handle items
  for child in items_grid.get_children():
    child.queue_free()
  match selected_item_type:
    "equipables":
      for index in range(GlobalStore.hero.equipables.size()):
        var equipable = GlobalStore.hero.equipables[index]
        var location = Enums.Slot.keys()[DataItems.equipables[equipable.blueprint].slot]
        # check if item is currently equiped. if yes, don't display it
        if !!GlobalStore.hero.equiped[location]:
          if GlobalStore.hero.equiped[location].uuid == equipable.uuid:
            continue
        var new_inventory_item = inventory_item_scene.instance()
        items_grid.add_child(new_inventory_item)
        new_inventory_item._initialize(index, "equipable", equipable)
        new_inventory_item.connect("clicked", self, "_on_bag_item_clicked")
    "resources":
      for index in range(GlobalStore.hero.resources.size()):
        var resource = GlobalStore.hero.resources[index]
        var new_inventory_item = inventory_item_scene.instance()
        items_grid.add_child(new_inventory_item)        
        new_inventory_item._initialize(index, "resource", resource)
        new_inventory_item.connect("clicked", self, "_on_bag_item_clicked")        

func _on_item_type_button_pressed(item_type: String):
  self.selected_item_type = item_type
  self.armed_item_index = null

func _on_bag_item_clicked(index):
  if armed_item_index == index:
    self.armed_item_index = null
  else:
    self.armed_item_index = index
  print(armed_item_index)


func _draw():
  refresh_display()
