extends Button

onready var http_request: HTTPRequest = $HTTPRequest

  
func _ready():
  http_request.connect("request_completed", self, "_on_update_response_received")
  
func _on_pressed():
  var url = "%s/players/%s" % [Data.server_url, GlobalStore.hero.uuid]
  var headers = ["Content-Type: application/json"]
  var payload = JSON.print({"hp": GlobalStore.hero.get_total_stat("max_hp")})
  var _resp = $HTTPRequest.request(url, headers, false, HTTPClient.METHOD_PATCH, payload)

func _on_update_response_received(_result, _response_code, _headers, body):
  var _response = parse_json(body.get_string_from_utf8())
  if _response_code != 200:
    return
  GlobalStore.hero.hp = _response.hp
  HUD.refresh_hp_display()
