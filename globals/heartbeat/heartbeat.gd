extends Node

onready var color_rect: ColorRect = $ColorRect

var is_live = false setget _set_is_live
var error_number = 0 setget _set_error_number
var colors = {
  "green": Color(0, 1, 0, 1),
  "red": Color(1, 0, 0, 1),
}

func _set_is_live(value: bool) -> void:
  is_live = value
  color_rect.color = colors["green"] if value == true else colors["red"]

func _set_error_number(value: int) -> void:
  error_number = value
  # TODO display alert if too much errors

func start_heartbeat():
  color_rect.show()  
  error_number = 0
  WsClient._send_packet("heartbeat", null)
  $Timer.start()

func _on_heartbeat_timer_timeout():
  WsClient._send_packet("heartbeat", null)

func stop_heartbeat():
  $Timer.stop()
  self.is_live = false
  color_rect.hide()


