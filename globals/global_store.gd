extends Node

var user = {
  "uuid": null,
  "player_uuid": null,
  "login": null,
  "battler_data": { 
    "name": "defaultname",
    "sprite": "default",
    "max_ap": 6,
    "max_mp": 3,
    "max_hp": 40,
    "skills": [],
  }
}

var hero: HeroData

var current_map = null
var current_position = null

var battle_team = null
var battle_uuid = null
var battle_data = null
var is_battle_owner = null

# 
var battle_gains: Dictionary = {}

func load_hero(data) -> void:
  user.player_uuid = data.uuid
  var hero_scene: PackedScene = load("res://classes/hero_data.tscn")
  hero = hero_scene.instance()
  hero.initialize(data)
