extends Node

var RNG: RandomNumberGenerator = RandomNumberGenerator.new()


func _ready():
#  RNG.seed = 156489
#  print(RNG.randi_range(0,10))
#  print(RNG.randi_range(0,10))
#  print(RNG.randi_range(0,10))
#  print(RNG.randi_range(0,10))
#  print(RNG.randi_range(0,10))
#  print(RNG.randi_range(0,10))
#  print(RNG.randi_range(0,10))
#  print(RNG.randi_range(0,10))

#  var current = get_player_level(400)
#  var total_next = 0
#  for i in range(0, current):
#    total_next += get_player_xp_to_next(i)
#  print("%d / %d" % [400, total_next])
  pass
  
func get_mastery(tree_name: String):
  var mastery = 0
  var job: Job = Data.jobs[GlobalStore.hero.job]
  var tree_index = job.skill_trees.find(tree_name)
  if tree_index == -1:
    print("ERROR! couldn't find tree with name '%s' in job '%s'" % [tree_name, job.id])
    return 1
  else:
    for skill_index in range(0, Data.skill_trees[tree_name].skills.size()):
      var field_name = "skill_%d%d_xp" % [tree_index, skill_index]
      mastery += get_skill_level(GlobalStore.hero[field_name])
  return mastery
  
func get_unlocked_skills() -> Array:
  var tresholds = [0, 4, 12, 28, 52, 90, 145]
  var unlocked_skills = []
  var trees_lvl = [0, 0, 0]
  var skill_trees = Data.jobs[GlobalStore.hero["job"]].skill_trees
  # add aggregate all xp of skills each skilltree
  for i in range(0, skill_trees.size()):
    trees_lvl[i] += get_mastery(skill_trees[i])
  # add skill_ids if tr
  for skill_index in range (0, tresholds.size()):
    for tree_index in range (0, skill_trees.size()):
      var tree_name = skill_trees[tree_index]
      if trees_lvl[tree_index] >= tresholds[skill_index]:
        if Data.skill_trees[tree_name].skills.size() > skill_index:
          unlocked_skills.push_back(Data.skill_trees[tree_name].skills[skill_index])    
  return unlocked_skills
  
func get_player_level(total_xp):
  var lvl = 0
  var aggregated_xp = 0
  while aggregated_xp <= total_xp:
    # add to_next_lvl_xp, increments lvl
    aggregated_xp += get_player_xp_to_next(lvl)
    lvl += 1
  return lvl
  
    
func get_skill_level(total_xp):
  var lvl = 1
  var aggregated = 24
  while total_xp > aggregated:
    lvl += 1
    aggregated += get_skill_xp_to_next(lvl)
  return lvl
  
func get_player_xp_to_next(level: int):
  return 100*pow(1.5, level) + 75*level

func get_skill_xp_to_next(lvl: int) -> int:
  if lvl == 0:
    return 0
  return int(25 * pow(1.45, lvl-1) + 20*(lvl -1))

