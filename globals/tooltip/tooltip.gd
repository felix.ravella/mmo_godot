extends CanvasLayer

const status_line_scene: PackedScene = preload("res://globals/tooltip/status_line.tscn")

onready var tooltip_container = $PanelContainer
onready var title_label = $PanelContainer/MarginContainer/VBoxContainer/TitleLabel
onready var content_label: RichTextLabel = $PanelContainer/MarginContainer/VBoxContainer/ContentRichText
onready var status_lines_container: VBoxContainer = $PanelContainer/MarginContainer/VBoxContainer/StatusLinesContainer

func show_skill(skill_id: String, hovered_node_position: Vector2, offset: Vector2 = Vector2(90,40)):
  title_label.show()
  content_label.show()
  var skill: SkillData = Data.skills[skill_id]
  title_label.text = skill.label
  # var content_string = "%d AP" % skill.ap_cost
  # TODO display icons for range and target details
  var content_string: String = SkillsHolder.get(skill_id).get_description()
  content_label.text = content_string
  tooltip_container.rect_global_position = hovered_node_position + offset
  show()

func show_raw_content(text: String, hovered_node_position: Vector2, offset: Vector2 = Vector2(-20,110)):
  title_label.hide()
  content_label.show()
  content_label.text = text
  tooltip_container.rect_global_position = hovered_node_position + offset
  show()
  
func show_battler(battler: Battler, offset: Vector2 = Vector2(-20,30)) -> void:
  title_label.hide()
  content_label.show()
  # buld info text
  var info_text = battler.data.name
  info_text += "\nHP: %d / %d" % [battler.hp, battler.data.max_hp]
  info_text += " ARM: %d" % battler.armor if battler.armor > 0 else ""
  content_label.text = info_text
  # display all statuses
  for child in status_lines_container.get_children():
    child.queue_free()
  title_label.hide()
  for status in battler.statuses:
    var lines = status.get_lines()
    for line_description in lines:
      var new_line = status_line_scene.instance()
      status_lines_container.add_child(new_line)
      new_line.initialize(status.duration, line_description)
  tooltip_container.rect_global_position = battler.global_position + offset
  show()

func _ready():
  hide()


func _on_tooltip_panel_mouse_exited():
  hide()
