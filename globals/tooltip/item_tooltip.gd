extends CanvasLayer

const OFFSET = Vector2(40, 100)


onready var tooltip_panel = $Panel
onready var title_label: Label = $Panel/MarginContainer/VContainer/Header/TitleLabel
onready var secondary_label: Label = $Panel/MarginContainer/VContainer/Header/SecondaryLabel
onready var damage_icon = $Panel/MarginContainer/VContainer/Header/DamageIcon
onready var armor_icon = $Panel/MarginContainer/VContainer/Header/ArmorIcon
onready var stats_holder = $Panel/MarginContainer/VContainer/StatsHolder
onready var description: RichTextLabel = $Panel/MarginContainer/VContainer/Description

var text_font_normal = preload("res://tres/fonts/alice_18.tres")

func show_resource_tooltip(item_instance, hovered_node_position):
  var item_data = DataItems.resources[item_instance.blueprint]
  title_label.text = item_data.label
  secondary_label.text = Enums.ResourceCategory.keys()[item_data.category]
  for property in item_data.properties.keys():
    var property_value = item_data.properties[property]
    var new_text = "%s: %d" % [property, property_value]
    add_stat_label(new_text)
  tooltip_panel.rect_global_position = hovered_node_position + OFFSET
  show()
  
func show_equipment_tooltip(item_instance, hovered_node_position):
  var item_data = DataItems.equipables[item_instance.blueprint]
  if item_data.slot == Enums.Slot.WEAPON:
    damage_icon.show()
  else:
    armor_icon.show()
    secondary_label.text = str(item_data.armor)
  for i in range(4):
    if item_data["stat_%d" % i] != null:
      var stat_label = item_data["stat_%d" % i]
      var stat_value = item_instance["stat_%d_value" % i]
      var sign_label = "+" if stat_value >= 0 else ""
      var new_text = "%s: %s%d" % [stat_label, sign_label, stat_value]
      add_stat_label(new_text)
  title_label.text = item_data.label
  description.text = item_data.description
  tooltip_panel.rect_global_position = hovered_node_position + OFFSET
  show()
  
func add_stat_label(new_text: String):
  var new_label: Label = Label.new()
  new_label.text = new_text
  new_label.add_color_override("font_color", Color(0,0,0,1))
  new_label.add_font_override("font", text_font_normal)
  stats_holder.add_child(new_label)
  
func hide():
  title_label.text = ""
  secondary_label.text = ""
  damage_icon.hide()
  armor_icon.hide()
  description.text = ""
  for child in stats_holder.get_children():
    child.queue_free()
  .hide()

func _ready():
  hide()
