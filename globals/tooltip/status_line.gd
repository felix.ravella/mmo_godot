extends HBoxContainer


func initialize(_duration, _description):
  $HBoxContainer/DurationLabel.text = "%s" % _duration
  $DescriptionRichText.text = _description
