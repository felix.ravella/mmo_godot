extends Node2D

onready var skills = $SkillEffectsHolder

var statuses = []


func _ready():
  $Timer.start(2.0)
  yield($Timer, "timeout")
  print("after yield")
  $Timer.start(2.0)
  yield($Timer, "timeout")
  print("after yield2")
  $Timer.start(2.0)
  yield($Timer, "timeout")
  print("after yield3")
#  var a = ["", "", "", ""]
#  for i in range(a.size() -1, -1, -1):
#    print(i)

  
#  skills.push_front(Default.new())
#  skills.push_front(Default.new())
  
  
func _on_skill_effect_finished():
  print("emit received")

func _on_skill_button_pressed():
  var skill_name = "default"
  var skill = skills.get(skill_name)
  skill.connect("skill_effect_finished", self, "_on_skill_effect_finished")
  skill.launch(null, [])
  print("yield")
  yield(skill, "skill_effect_finished")
  print("yield passed")
  
func _on_status_button_pressed():
  var type = "blood_rage"
  var status_class = load("res://classes/status/%s_status.gd" % type)
  var new_status = status_class.new(null, null, 100, 4)
  print(new_status.duration)
#  var status = BloodRageStatus.new(null, null, 100, 4)
#  statuses.push_front(status)
  
func _on_reload_scene_pressed():
  for i in range(statuses.size()-1, -1, -1):
    if not is_instance_valid(statuses[i]):
      statuses.remove(i)
  for item in statuses:
    item.queue_free()
    item = null
  
  # get_tree().change_scene("res://scenes/playground/playground.tscn")

func get_skill_xp_to_next(lvl: int) -> int:
  if lvl == 0:
    return 0
  return int(25 * pow(1.45, lvl-1) + 20*(lvl -1))

func get_total_skill_xp_to_next(lvl: int) -> int:
  var total = 0
  for i in range (1, lvl+1):
    total += get_skill_xp_to_next(i)
  return total
    
func get_skill_lvl(total_xp: int) -> int:
  var lvl = 1
  var aggregated = 24
  while total_xp > aggregated:
    lvl += 1
    aggregated += get_skill_xp_to_next(lvl)
  return lvl
  
func get_hero_xp_to_next(lvl: int) -> int:
  if lvl == 0:
    return 0
  return int(100 * pow(1.5, lvl-1) + 75*(lvl -1))

func get_total_hero_xp_to_next(lvl: int) -> int:
  var total = 0
  for i in range (1, lvl+1):
    total += get_hero_xp_to_next(i)
  return total
    
func get_hero_lvl(total_xp: int) -> int:
  var lvl = 1
  var aggregated = 99
  while total_xp > aggregated:
    lvl += 1
    aggregated += get_hero_xp_to_next(lvl)
  return lvl
  
func test_xp_calculations():
  print("lvl %d: need %d xp" % [0, get_skill_xp_to_next(0)])
  print("lvl %d: need %d xp" % [1, get_skill_xp_to_next(1)])
  print("lvl %d: need %d xp" % [2, get_skill_xp_to_next(2)])
  print("lvl %d: need %d xp" % [3, get_skill_xp_to_next(3)])
  print("lvl %d: need %d xp" % [4, get_skill_xp_to_next(4)])
  print("lvl %d: need %d xp" % [5, get_skill_xp_to_next(5)])

  print("lvl %d. total to lvl up: %d" % [0, get_total_skill_xp_to_next(0)])
  print("lvl %d. total to lvl up: %d" % [1, get_total_skill_xp_to_next(1)])
  print("lvl %d. total to lvl up: %d" % [2, get_total_skill_xp_to_next(2)])
  print("lvl %d. total to lvl up: %d" % [3, get_total_skill_xp_to_next(3)])
  print("lvl %d. total to lvl up: %d" % [4, get_total_skill_xp_to_next(4)])
  print("lvl %d. total to lvl up: %d" % [5, get_total_skill_xp_to_next(5)])

  print("has %d xp. is lvl %d. should be 1" % [0, get_skill_lvl(0)])
  print("has %d xp. is lvl %d. should be 1" % [14, get_skill_lvl(14)])
  print("has %d xp. is lvl %d. should be 2" % [25, get_skill_lvl(25)])
  print("has %d xp. is lvl %d. should be 4" % [184, get_skill_lvl(184)])
  print("has %d xp. is lvl %d. should be 5" % [498, get_skill_lvl(498)])
  print("has %d xp. is lvl %d. should be 6" % [499, get_skill_lvl(499)])

  print("lvl %d: need %d xp" % [0, get_hero_xp_to_next(0)])
  print("lvl %d: need %d xp" % [1, get_hero_xp_to_next(1)])
  print("lvl %d: need %d xp" % [2, get_hero_xp_to_next(2)])
  print("lvl %d: need %d xp" % [3, get_hero_xp_to_next(3)])
  print("lvl %d: need %d xp" % [4, get_hero_xp_to_next(4)])
  print("lvl %d: need %d xp" % [5, get_hero_xp_to_next(5)])

  print("lvl %d. total to lvl up: %d" % [0, get_total_hero_xp_to_next(0)])
  print("lvl %d. total to lvl up: %d" % [1, get_total_hero_xp_to_next(1)])
  print("lvl %d. total to lvl up: %d" % [2, get_total_hero_xp_to_next(2)])
  print("lvl %d. total to lvl up: %d" % [3, get_total_hero_xp_to_next(3)])
  print("lvl %d. total to lvl up: %d" % [4, get_total_hero_xp_to_next(4)])
  print("lvl %d. total to lvl up: %d" % [5, get_total_hero_xp_to_next(5)])

  print("has %d xp. is lvl %d. should be 1" % [0, get_hero_lvl(0)])
  print("has %d xp. is lvl %d. should be 1" % [75, get_hero_lvl(75)])
  print("has %d xp. is lvl %d. should be 2" % [324, get_hero_lvl(324)])
  print("has %d xp. is lvl %d. should be 4" % [1025, get_hero_lvl(1025)])
  print("has %d xp. is lvl %d. should be 5" % [1852, get_hero_lvl(1852)])
  print("has %d xp. is lvl %d. should be 6" % [2068, get_hero_lvl(2068)])
