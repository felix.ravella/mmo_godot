extends TextureRect

var dropped_on_target: bool = false

func get_drag_data(position):
  print("get_drag_data_triggered")
  if not dropped_on_target:
    set_drag_preview(_get_preview_control())
  return self


func _get_preview_control() -> Control:
  """
    Must not be in the scene tree. Must not be freed, it will be automatically after drag.
    Do not keep a reference beyond the drag.
  """
  var preview = TextureRect.new()
  preview.texture = load("res://icon.png")
  preview.modulate.a = 0.5
  return preview


func _on_draggable_dropped_on_target(node):
  queue_free()
