extends Panel

signal draggable_dropped(draggable)

func can_drop_data(position, data):
  print("can_drop_data triggered!")
  return true
  
func drop_data(position, data):
  print("drop_data triggered")
  var copy = TextureRect.new()
  copy.texture = load("res://icon.png")
  add_child(copy)
  emit_signal('draggable_dropped', data)
