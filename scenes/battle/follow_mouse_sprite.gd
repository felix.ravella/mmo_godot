extends Sprite

const OFFSET = Vector2(25,10)

var skill_id = null setget _set_skill_id


func _set_skill_id(value) -> void:
  skill_id = value
  if !skill_id:
    texture = null
  else:
    texture = load("res://assets/icons/skills/%s.png" % skill_id)
    global_position = get_global_mouse_position() + OFFSET

func _process(_delta):
  if skill_id != null:
    var mouse_pos = get_global_mouse_position()
    global_position = mouse_pos + OFFSET
