extends Control

const skill_button_scene: PackedScene = preload("res://entities/skill_button/skill_button.tscn")

onready var hp_progress = $Panel/HPDisplay/HPProgressBar
onready var hp_progress_label = $Panel/HPDisplay/HPProgressBar/Label
onready var armor_progress = $Panel/HPDisplay/ArmorProgressBar
onready var armor_progress_label = $Panel/HPDisplay/ArmorProgressBar/Label

onready var ap_label = $Panel/APLabel
onready var mp_label = $Panel/MPLabel
onready var ready_button = $Panel/ReadyButton
onready var lock_button = $Panel/LockButton
onready var end_turn_button = $Panel/EndTurnButton
onready var your_turn_label = $Panel/EndTurnButton/YourTurnLabel
onready var skill_buttons_holder = $Panel/SkillButtonsHolder

var ready_status: bool = false setget _set_ready_status
var skills_buttons: Dictionary = {}
var is_self_turn: bool = false

signal toggle_ready
signal toggle_lock
signal end_turn_button_clicked
signal skill_button_pressed(skill_id)


func _set_ready_status(value):
  ready_button.text = "Ready" if value == false else "Unready"
  
func set_lock_status(value):
  ready_button.text = "Lock" if value == false else "Unlock"
  
func start_battle(self_battler: Battler) -> void:
  ready_button.hide()
  lock_button.hide()
  display_values(self_battler)
  
func start_turn(battler: Battler):
  your_turn_label.show()
  end_turn_button.show()
  initialize_skill_buttons(battler)
  is_self_turn = true
  
func end_turn():
  your_turn_label.hide()
  end_turn_button.hide()
  is_self_turn = false
  
func display_values(battler: Battler):
  hp_progress.max_value = battler.data.max_hp
  hp_progress.value = battler.hp
  hp_progress_label.text = "%d / %d" % [battler.hp, battler.data.max_hp]
  ap_label.text = "AP: %d / %d" % [battler.ap, battler.data.max_ap]
  mp_label.text = "MP: %d / %d" % [battler.mp, battler.data.max_mp]
  armor_progress.max_value = battler.armor_cap
  armor_progress.value = battler.armor
  armor_progress_label.text = "%d / %d" % [battler.armor, battler.armor_cap]
  armor_progress.visible = battler.armor > 0
  
# user specific battler in case its someday possible to control other battler than self player
func initialize_skill_buttons(battler: Battler):
  for child in skill_buttons_holder.get_children():
    child.queue_free()
  for skill_id in battler.data.skills:
    var new_button: SkillButton = skill_button_scene.instance()
    skill_buttons_holder.add_child(new_button)
    new_button.initialize(skill_id, battler)
    new_button.connect("skill_button_clicked", self, "_on_skill_button_pressed")
    skills_buttons[skill_id] = new_button
  refresh_buttons_display()
  
  
func refresh_buttons_display():
  for button in skill_buttons_holder.get_children():
    button.refresh_display()

func _on_ready_button_pressed():
  emit_signal("toggle_ready")
  
func _on_lock_button_pressed():
  emit_signal("toggle_lock")

func _on_end_turn_button_pressed():
  emit_signal("end_turn_button_clicked")

func _on_battler_values_changed(battler: Battler):
  display_values(battler)
  refresh_buttons_display()

func _on_battler_hovered(battler: Battler):
  display_values(battler)
  
func _on_skill_button_pressed(skill_id: String):
  if is_self_turn:
    emit_signal("skill_button_pressed", skill_id)
