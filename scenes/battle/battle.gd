extends CanvasLayer
class_name Battle

const battler_scene = preload('res://entities/battler/battler.tscn')
const plant_battler_scene = preload('res://entities/battler/plant_battler.tscn')
const battler_vignettte_scene = preload('res://entities/battler_vignette/battler_vignette.tscn')
const ground_effect_scene = preload('res://entities/ground_effect/ground_effect.tscn')

onready var grid: Node2D = $Grid
onready var ground_effects_holder: Node2D = $Grid/GroundEffectsHolder
onready var battlers_holder: Node2D = $Grid/BattlersHolder
onready var battle_HUD = $BattleHUD
onready var battle_log_panel = $BattleHUD/Panel/BattleLogPanel
onready var state_label: Label = $StateLabel
onready var initiative_holder = $InitiativeHolder
onready var follow_mouse_sprite = $FollowMouseSprite
onready var wait_timer = $WaitTimer

var death_queue: Array = []
var battle_data = []
var battlers: Dictionary = {}
var initiative_list = [] #is a list of battlers uuid, sorted by initiative
var current_battler_index = -1
var current_battler_uuid: String setget _set_current_battler_uuid
var summon_count: int = 0
var ground_effects: Dictionary = {}
var ground_effect_count: int = 0
#var current_battler = null setget _set_current_battler
var state: String = 'placing' setget _set_state # 'placing', 'pathing', 'targeting', 'processing', 'locked'
var armed_skill_id = null setget _set_armed_skill_id
var dead_monsters = []

var r_seed = 0

var skill_usage: Dictionary = {} # record of usage of skills by self instance

var monsters_cells = []
var players_cells = []



signal current_battler_changed(new_battler)
signal state_changed(new_state)
signal seed_recorded(value)


func _set_current_battler_uuid(value) -> void:
  if !value:
    current_battler_uuid = ""
    return
  if current_battler_uuid:
    var old_battler: Battler = battlers[current_battler_uuid]
    old_battler.is_current_battler = false
  current_battler_uuid = value
  battlers[current_battler_uuid].is_current_battler = true

func _set_state(value: String) -> void:
  state = value
  state_label.text = "state: %s" % state
  emit_signal('state_changed', state)
  
func _set_armed_skill_id(value):
  armed_skill_id = value
  follow_mouse_sprite.skill_id = value
  grid.reset_targeting_values()
  if armed_skill_id != null:
    var skill_data: SkillData = Data.skills[armed_skill_id]
    var origin: Vector2 = battlers[current_battler_uuid].current_cell.coords
    # compute actual range
    var target_range = skill_data.target_range
    
    if skill_data.modifiable_range == true:
      if battlers[current_battler_uuid].has_status("blinded"):
        target_range -=3
      target_range = clamp(target_range, 0, 99)
    
    var valid_cells = grid.get_area(origin, skill_data.target_shape, target_range)
    for cell in valid_cells:
      if skill_data.target_free_cell == true && cell.battler != null:
        continue
      cell.is_target = true

func add_ground_effect(center_cell, caster, type, radius, duration, power):
  print("adding effect")
  var new_effect: GroundEffect = ground_effect_scene.instance()
  ground_effects_holder.add_child(new_effect)
  var new_uuid = "ground_effect_%d" % ground_effect_count
  var affected_cells = grid.get_area(center_cell.coords, "circle", radius)
  
  new_effect._initialize(new_uuid, caster, type, affected_cells, duration, power)
  ground_effects[new_uuid] = new_effect
  ground_effect_count += 1
  for cell in affected_cells:
    cell.ground_effects[new_uuid] = new_effect
    if cell.battler != null and GroundEffectsHolder.get(type).has_method("_on_battler_entered"):
      GroundEffectsHolder.get(type)._on_battler_entered(cell.battler, new_effect)
      if cell.battler != null:
        cell.battler.entered_ground_effects_this_turn.append(new_uuid)
  return new_effect
  
func remove_ground_effect(uuid: String) -> void:
  # trigger on remove on all affected battler (if needed)
  var ground_effect = ground_effects[uuid]
  for cell in ground_effect.affected_cells:
    if cell.battler != null:
      if GroundEffectsHolder.get(ground_effect.type).has_method('_on_removed'):
        GroundEffectsHolder.get(ground_effect.type)._on_removed(cell.battler, ground_effect)
    cell.ground_effects.erase(uuid)
  # effectively remove effect instance and reference
  ground_effects.erase(uuid)
  ground_effect.queue_free()
  

# facing: "down_left
func add_battler(battler_data: Dictionary, hero_data: Dictionary, uuid: String, coords: Vector2, team: int, facing: String, is_plant = false) -> Battler:  
  var new_battler: Battler = battler_scene.instance()
  battlers_holder.add_child(new_battler)
  new_battler.initialize(battler_data, hero_data, uuid, grid.cells[coords.x][coords.y], team, facing)
  new_battler.connect('battler_clicked', self, '_on_battler_clicked')
  new_battler.connect("battler_damaged", self, "_on_battler_damaged")
  new_battler.connect("battler_healed", self, "_on_battler_healed")
  new_battler.connect("battler_died", self, "_on_battler_died")
  battlers[uuid] = new_battler
  return new_battler

func add_plant_battler(plant_type: String, summoner: Battler, spawn_coords: Vector2, power: int):
  var new_battler: PlantBattler = plant_battler_scene.instance()
  battlers_holder.add_child(new_battler)
  var new_uuid = "summon_%d" % summon_count
  summon_count += 1
  var spawn_cell = grid.cells[spawn_coords.x][spawn_coords.y]
  new_battler.create(plant_type, summoner, power, new_uuid, spawn_cell)
  new_battler.connect('battler_clicked', self, '_on_battler_clicked')
  new_battler.connect("battler_died", self, "_on_battler_died")  
  battlers[new_uuid] = new_battler
  var index_of_summoner: int = initiative_list.find(summoner.uuid)
  initiative_list.insert(index_of_summoner + 1, new_uuid)
  # add vignette 
  var new_vignette = battler_vignettte_scene.instance()
  initiative_holder.add_child(new_vignette)
  new_vignette.initialize(new_uuid, battlers[new_uuid])

func summon(battler_data: Dictionary, spawn_coords: Vector2, summoner: Battler, is_plant = false):
  var new_uuid = "summon_%d" % summon_count
  var _new_battler: Battler = add_battler(battler_data, {}, new_uuid, spawn_coords, summoner.team, "down_left")
  summon_count += 1
  var index_of_summoner: int = initiative_list.find(summoner.uuid)
  initiative_list.insert(index_of_summoner + 1, new_uuid)
  # add vignette 
  var new_vignette = battler_vignettte_scene.instance()
  initiative_holder.add_child(new_vignette)
  new_vignette.initialize(new_uuid, battlers[new_uuid])

func compare_by_initiative(uuid_a: String, uuid_b: String) -> bool:
  return battlers[uuid_a].get_total_stat("initiative") > battlers[uuid_b].get_total_stat("initiative")

func start_battle() -> void:
  # refresh displays: is_ready icon, HUD...
  for key in battlers:
    battlers[key].is_ready = false
  var self_battler: Battler = battlers[GlobalStore.user.player_uuid]
  battle_HUD.start_battle(self_battler)
  battle_HUD.connect("skill_button_pressed", self, "_on_skill_button_clicked")
  self_battler.connect("values_changed", battle_HUD, "_on_battler_values_changed")
  for row in grid.cells:
    for cell in row:
      cell.team_placement = null
  # sort battler uuid by battler initative
  initiative_list = battlers.keys().duplicate()
  initiative_list.sort_custom(self, "compare_by_initiative")
  # TODO display battlers on HUD by init
  for uuid in initiative_list:
    var new_vignette = battler_vignettte_scene.instance()
    initiative_holder.add_child(new_vignette)
    new_vignette.initialize(uuid, battlers[uuid])
  start_turn()

func start_turn():
  # set next battler
  current_battler_index = 0 if (current_battler_index + 1 == battlers.size()) else current_battler_index + 1
  var battler = battlers[initiative_list[current_battler_index]]
  # resolve battler's ground effects
  for uuid in ground_effects:
    var ground_effect = ground_effects[uuid]
    if ground_effect.caster == battler:
      ground_effect.duration -= 1
      if ground_effect.duration == 0:
        remove_ground_effect(uuid)
  while battlers[initiative_list[current_battler_index]].hp <= 0:
    current_battler_index = 0 if (current_battler_index + 1 == battlers.size()) else current_battler_index + 1
    battler = battlers[initiative_list[current_battler_index]]
    for uuid in ground_effects:
      var ground_effect = ground_effects[uuid]
      if ground_effect.caster == battler:
        ground_effect.duration -= 1
        if ground_effect.duration == 0:
          remove_ground_effect(uuid)
  self.current_battler_uuid = initiative_list[current_battler_index]
#  var battler = battlers[initiative_list[current_battler_index]]
  print("current battler is %s" % battler.data.name)
  if (battler.uuid.length() <= 4 || battler.uuid.begins_with('summon')) && GlobalStore.is_battle_owner:
    battler.connect("monster_decide_move", self, "_on_monster_decide_move")
    battler.connect("monster_decide_action", self, "_on_monster_decide_action")
    battler.connect("monster_decide_pass", self, "_on_monster_decide_pass")
  elif battler.uuid == GlobalStore.user.player_uuid:
    battle_HUD.start_turn(battler)
  battler.start_turn()
  resolve_state()


func end_turn():
  self.armed_skill_id = null
  var current_battler = battlers[current_battler_uuid]
  if current_battler.uuid.length() <= 4 || current_battler.uuid.begins_with('summon'):
    # should be connected to 3 signals, so only need to check one
    if current_battler.is_connected("monster_decide_move", self, "_on_monster_decide_move"):
      current_battler.disconnect("monster_decide_move", self, "_on_monster_decide_move")
      current_battler.disconnect("monster_decide_action", self, "_on_monster_decide_action")
      current_battler.disconnect("monster_decide_pass", self, "_on_monster_decide_pass")
  grid.reset_pathfinding_values()
  battle_HUD.end_turn()
  current_battler.end_turn()
  start_turn()
  
func _on_battler_died(dead_battler: Battler):
  death_queue.append(dead_battler)
  
func resolve_death():
  var dead_battler = death_queue.pop_front()
  if dead_battler.uuid.length() <= 4:
    dead_monsters.push_back(DataMonsters.monsters[dead_battler.data.name]) 
  # return a resolution is already done
  self.state = "locked"
#  yield(dead_battler.animation_player, "animation_finished")
  var surviving_teams: Array = []
  for battler_uuid in battlers:
    var battler = battlers[battler_uuid]
    if battler.uuid.begins_with("summon"):
      continue
    if battler.hp <= 0:
      continue
    if not surviving_teams.has(battler.team):
      surviving_teams.push_back(battler.team)

  var result = ""    
  if not surviving_teams.has(0):
    result = "lose"
  elif not surviving_teams.has(1):
    result = "win"
  var player_hps = {}
  for uuid in battlers.keys():
    # check if uuid is not of a monster or a summon:
    if uuid.length() > 8 && not uuid.begins_with("summon"):
      player_hps[uuid] = battlers[uuid].hp
  if result != "" && GlobalStore.is_battle_owner:
    WsClient._send_packet("battle_ended", {
      "battle_uuid": GlobalStore.battle_uuid,
      "result": result,
      "dead_monsters": dead_monsters,
      "player_hps": player_hps
    });
    # save locally player hp
    var new_hp = battlers[GlobalStore.hero.uuid].hp
    if new_hp <= 0:
      new_hp = 1
    GlobalStore.hero.hp = new_hp
  else:
    pass
  resolve_state()
    
# called on turn start and after actions
# decide the state and who should take next action
func resolve_state():
  # disconnect wait timer, if needed
  if wait_timer.is_connected("timeout", self, "resolve_state"):
    wait_timer.disconnect("timeout", self, "resolve_state")
  if death_queue.size() > 0:
    resolve_death()
  else:
    var current_battler = battlers[current_battler_uuid]
    if current_battler.hp <= 0:
      start_turn()
      return
    if current_battler.uuid.length() <= 4 || current_battler.uuid.begins_with('summon'):
      self.state = "locked"
      current_battler.ia_decide()
    elif current_battler.uuid == GlobalStore.user.player_uuid:
      self.state = "pathing"
    else:
      self.state = "locked"

func _on_cell_clicked(cell):
  match state:
    'processing':
      return
    'placing':
      if cell.battler != null:
        print('occupied')
        return
      if cell.team_placement != GlobalStore.battle_team:
        print('no team')
        return
      if battlers[GlobalStore.user.player_uuid].is_ready:
        print('cannot move while ready')
        return
      WsClient._send_packet("change_start_position", {
        "battle_uuid": GlobalStore.battle_uuid,
        "target": {"x": cell.coords.x, "y": cell.coords.y},
      });
    'pathing':
      if not cell.is_pathing:
        return
      handle_move(cell)
    'targeting':
      self.state = "pathing"
      if not cell.is_target:
        return
      var current_battler: Battler = battlers[current_battler_uuid] 
      if not skill_usage.has(armed_skill_id):
        skill_usage[armed_skill_id] = 0
      skill_usage[armed_skill_id] += Data.skills[armed_skill_id].ap_cost
      HttpSeedRequest.connect("seed_received", self, "_on_seed_received")
      HttpSeedRequest.get_seed()
      yield(self, "seed_recorded")
      HttpSeedRequest.disconnect("seed_received", self, "_on_seed_received")
      launch_action(current_battler, armed_skill_id, cell, r_seed)
      WsClient._send_packet("launch_skill", {
        "battle_uuid": GlobalStore.battle_uuid,
        "battler_uuid": GlobalStore.user.player_uuid,
        "skill_id": armed_skill_id,
        "target": {"x": cell.coords.x, "y": cell.coords.y},
        "random_seed": r_seed 
      })
      self.armed_skill_id = null
    
func _unhandled_key_input(event: InputEventKey):
  # get real key depending on keyboard language
  var key = OS.get_scancode_string(event.scancode)
  match key:
    "Escape":
      if state == "targeting":
        self.armed_skill_id = null
        self.state = "pathing"
    
func handle_move(cell):
  # return if cell wasn't tagged as valid destination
  var current_battler: Battler = battlers[current_battler_uuid]
  if cell.is_free:
    var path = grid.find_path(current_battler, cell)
    # if path is 0-length: return
    if path == []:
      return
    self.state = "locked"
    current_battler.travel_to(path)
    # TODO emit ws
    WsClient._send_packet("move_battler", {
      "battle_uuid": GlobalStore.battle_uuid,
      "battler_uuid": GlobalStore.user.player_uuid, 
      "destination": {"x": cell.coords.x, "y": cell.coords.y}
    })
    yield(current_battler, "travel_finished")
    self.state = "pathing"  

func launch_action(caster: Battler, skill_id: String, target_cell: Cell, random_seed):
  var skill_data: SkillData = Data.skills[skill_id]
  var new_effect: SkillEffect = SkillsHolder.get(skill_id)
  var affected_cells = grid.get_area(target_cell.coords, skill_data.area_shape, skill_data.area_radius, caster)
  if skill_data.cooldown > 0:
    caster.cooldowns[skill_id] = skill_data.cooldown
  caster.cast(skill_id, target_cell)
  # caster.pay_skill_cost(skill_id)
  battle_log_panel.add_log("%s casts %s" % [caster.data.name, skill_data.label])
  new_effect.launch(caster, affected_cells, random_seed)
  wait_timer.connect("timeout", self, "resolve_state")
  wait_timer.start(0.8)

func _on_battler_damaged(battler, value):
  battle_log_panel.add_log("%s took %d damage" % [battler.data.name, value])
  
func _on_battler_healed(battler, value):
  battle_log_panel.add_log("%s recovered %d health" % [battler.data.name, value])

func _on_cell_hover(cell) -> void:
  if state == 'pathing' && GlobalStore.user.player_uuid == current_battler_uuid:
    var current_battler = battlers[current_battler_uuid]
    if current_battler.mp <= 0:
      return
    if cell.is_free:
      grid.reset_pathfinding_values()
      # slice for limiting to number of mp
      var path: Array = grid.find_path(current_battler, cell).slice(0, current_battler.mp -1) 
      # tag all cells in path as valid destination
      for cell in path:
        cell.is_pathing = true
  if state == 'targeting':
    for cell in get_tree().get_nodes_in_group("Cell"):
      cell.is_aoe = false
    if cell.is_target:
      var origin: Vector2 = cell.coords    
      var skill_data: SkillData = Data.skills[armed_skill_id]
      var affected_cells = grid.get_area(origin, skill_data.area_shape, skill_data.area_radius, battlers[current_battler_uuid])
      for cell in affected_cells:
        cell.is_aoe = true
    


func _on_skill_button_clicked(skill_id):
  if state == "locked" || state == "processing" || state == "placing":
    return
  if state == "targeting" && skill_id == armed_skill_id: 
    self.state = "pathing"
    self.armed_skill_id = null
    return
  else:
    self.state = "targeting"
    self.armed_skill_id = skill_id

func _on_end_turn_button_clicked() -> void:
  end_turn()
  # send to other players
  WsClient._send_packet("end_turn", {
    "battle_uuid": GlobalStore.battle_uuid,
    "battler_uuid": GlobalStore.user.player_uuid, 
  })

func _on_toggle_ready():
  WsClient._send_packet("set_ready_status", {
    "battle_uuid": GlobalStore.battle_uuid,
    "value": !battlers[GlobalStore.user.player_uuid].is_ready,
  });
  
func _on_seed_received(value):
  r_seed = int(value)
  emit_signal("seed_recorded")
  
func _ready():  
  grid.initialize(Vector2(12, 12), self)
  HUD.hide()
  WsClient.connect("player_joined_lobby", self, "_on_player_joined")
  WsClient.connect("player_changed_start_position", self, "_on_player_changed_start_position")
  WsClient.connect("player_changed_ready_status", self, "_on_player_changed_ready_status")
  WsClient.connect("battle_started", self, "_on_battle_started")
  WsClient.connect("battler_moved", self, "_on_battler_moved")
  WsClient.connect("turn_ended", self, "_on_turn_ended")
  WsClient.connect("battler_launched_skill", self, "_on_battler_launched_skill")
  WsClient.connect("battle_ended", self, "_on_battle_ended")
  battle_data = GlobalStore.battle_data
#  connect("current_battler_changed", $HUD, "_on_current_battler_changed") 

  # instanciate all monsters
  for monster in battle_data.horde:
    var new_battler_uuid = str(battlers.size())
    var monster_data = DataMonsters.monsters[monster.id]
    var monster_position = Vector2(monster.position.x, monster.position.y )
    var new_battler = add_battler(monster_data.battler_data, {}, new_battler_uuid, monster_position, 1, "down_left")
    battlers[new_battler_uuid] = new_battler
  # instanciate all players
  for uuid in battle_data.players:
    var player = battle_data.players[uuid]
    var player_position = Vector2(player.position.x, player.position.y)
    var new_battler = add_battler(player.battler_data, player.hero_data, uuid, player_position, 0, "up_right")
    battlers[uuid] = new_battler
    if uuid == GlobalStore.hero.uuid:
      battle_HUD.display_values(new_battler)
  
  for coord in battle_data.player_cells:
    var cell = grid.cells[coord.x][coord.y]
    players_cells.append(cell)
    cell.team_placement = 0
  for coord in battle_data.monster_cells:
    var cell = grid.cells[coord.x][coord.y]
    monsters_cells.append(cell)
    cell.team_placement = 1
  # get all unlocked skills of self battler
  battlers[GlobalStore.user.player_uuid].data.skills = GlobalStore.hero.get_unlocked_skills()
  battle_HUD.initialize_skill_buttons(battlers[GlobalStore.user.player_uuid])
  
# -> incoming message when another player has joined lobby
func _on_player_joined(uuid, battler_data, hero_data, position, team):
  add_battler(battler_data, hero_data, uuid, Vector2(position.x, position.y), team, "up_right")
  battle_log_panel.add_log("%s joined the battle!" % battler_data.name)

# -> incoming message when another player has slected a different starting cell
func _on_player_changed_start_position(uuid, target):
  battlers[uuid].teleport_to(grid.cells[target.x][target.y])

# -> incoming message when another player or monster (if self is not battler owner) moves
func _on_battler_moved(battler_uuid: String, destination: Dictionary) -> void:
  var battler: Battler = battlers[battler_uuid]
  var target_cell = grid.cells[destination.x][destination.y]
  var path: Array = grid.find_path(battler, target_cell)
  battler.travel_to(path)
  
# -> incoming message when another player or monster (if self is not battler owner) passes or runs out of time
func _on_turn_ended(battler_uuid: String) -> void:
  if battler_uuid != current_battler_uuid:
    print('ERROR! not owner instance received end turn from different current battler!')
    return
  end_turn()
  
# -> incoming message when another player or monster (if self is not battler owner) launches skill on target
func _on_battler_launched_skill(_battler_uuid: String, skill_id: String, target: Dictionary, random_seed) -> void:
  var cell = grid.cells[target.x][target.y]
  var current_battler: Battler = battlers[current_battler_uuid]
  launch_action(current_battler, skill_id, cell, random_seed)
  
func _on_player_changed_ready_status(uuid: String, value: bool):
  battlers[uuid].is_ready = value
  if uuid == GlobalStore.user.player_uuid:
    battle_HUD.ready_status = value
  
func _on_battle_started():
  start_battle()
  
# all instances receive this signal after battle_owner has resolved that battle is finished
func _on_battle_ended(payload):
  # compute and skill_gain
  var xp_gain = payload.xp
  var total_weight: float = 0.0
  for key in skill_usage:
    total_weight += skill_usage[key]
  var skill_xp = {}
  for key in skill_usage:
    skill_xp[key] = int(skill_usage[key] / total_weight * xp_gain)
  # compute loot
  # add to global_store for hanling on map change
  GlobalStore.battle_gains["result"] = payload.result
  GlobalStore.battle_gains["xp_gain"] = xp_gain
  GlobalStore.battle_gains["skill_xp"] = skill_xp
  GlobalStore.battle_gains["loot"] = payload.loot
  # request change map (prvious if win, default if lose)
  if payload.result == "win":
    WsClient._send_packet("change_map", {
      "destination_map" : GlobalStore.current_map,
      "destination_position": { "x": GlobalStore.current_position.x, "y": GlobalStore.current_position.y }
    })
  else:
    WsClient._send_packet("change_map", {
      "destination_map" : "10,13",
      "destination_position": { "x": 300, "y": 300 }
    })

# only if self is battler_owner. handles monster move, emits, then re-triggers ia decision
func _on_monster_decide_move(battler: Battler, destination: Vector2):
  var target_cell = grid.cells[destination.x][destination.y]
  var path: Array = grid.find_path(battler, target_cell)
  # send to other players
  WsClient._send_packet("move_battler", {
    "battle_uuid": GlobalStore.battle_uuid,
    "battler_uuid": battler.uuid, 
    "destination": {"x": destination.x, "y": destination.y}
  })
  battler.travel_to(path)
  yield(battler, "travel_finished")
  battler.ia_decide()

# only if self is battler_owner. handles monster move, emits, then re-triggers ia decision
func _on_monster_decide_action(battler: Battler, skill_id: String, target_cell):
  HttpSeedRequest.connect("seed_received", self, "_on_seed_received")
  HttpSeedRequest.get_seed()
  yield(self, "seed_recorded")
  HttpSeedRequest.disconnect("seed_received", self, "_on_seed_received")
  launch_action(battler, skill_id, target_cell, r_seed)
  WsClient._send_packet("launch_skill", {
    "battle_uuid": GlobalStore.battle_uuid,
    "battler_uuid": battler.uuid,
    "skill_id": skill_id,
    "target": {"x": target_cell.coords.x, "y": target_cell.coords.y}, 
    "random_seed": r_seed  
  })
#  yield(get_tree().create_timer(0.5), "timeout")

# only if self is battler_owner. handles monster passing turn and emits
func _on_monster_decide_pass(battler: Battler):
  end_turn()
  # send to other players
  WsClient._send_packet("end_turn", {
    "battle_uuid": GlobalStore.battle_uuid,
    "battler_uuid": battler.uuid, 
  })

