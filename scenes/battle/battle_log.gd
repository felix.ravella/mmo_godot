extends Panel

const battle_log_scene: PackedScene = preload("res://entities/battle_log/battle_log.tscn")

onready var scroll_container = $ScrollContainer
onready var scrollbar = scroll_container.get_v_scrollbar()
onready var logs_holder = $ScrollContainer/LogsHolder

func add_log(body: String) -> void:
  var new_log: Label = battle_log_scene.instance()
  logs_holder.add_child(new_log)
  new_log.text = body
  yield(get_tree(), "idle_frame")
  scroll_container.scroll_vertical = scrollbar.max_value
