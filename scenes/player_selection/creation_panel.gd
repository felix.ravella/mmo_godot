extends Panel

const job_selection_scene: PackedScene = preload("res://entities/job_selection_button/job_selection_button.tscn")

onready var jobs_list: HBoxContainer = $JobsList
onready var confirm_button = $ConfirmButton
onready var selected_job_label = $SelectedJob

var character_name = ''
var selected_job: String = '' setget _set_selected_job

signal new_character_confirmed(name, job)


func _set_selected_job(value: String):
  selected_job = value
  selected_job_label.text = "Selected: %s" % ('None' if value == '' else Data.jobs[value].label)
  confirm_button.disabled = (character_name == '' || value == '')

func close():
  self.selected_job = ''
  self.character_name = ''
  $CharacterNameEdit.text = ''
  hide()  

func _on_name_edit_changed(new_text):
  character_name = new_text
  confirm_button.disabled = (character_name == '' || selected_job == '')  

func _on_job_button_pressed(job_name):
  if selected_job == job_name:
    self.selected_job = ''
  else:
    self.selected_job = job_name
    
func _on_close_button_pressed():
  close()


func _on_confirm_button_pressed():
  emit_signal("new_character_confirmed", character_name, selected_job)  

func _ready():
  for key in Data.jobs:
    var new_button = job_selection_scene.instance()
    jobs_list.add_child(new_button)
    new_button.initialize(Data.jobs[key])
    new_button.connect('job_button_pressed', self, "_on_job_button_pressed")
    
