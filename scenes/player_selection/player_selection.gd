extends Control

const character_button_scene = preload("res://entities/character_selection_button/character_selection_button.tscn")

onready var http_client = $HTTPRequest
onready var character_list = $HBoxContainer/CharacterList
onready var creation_panel = $CreationPanel

var selected_hero_data = null

func _ready():
  creation_panel.connect("new_character_confirmed", self, "_on_new_character_confirmed")
  var headers = ["Content-Type: application/json"]
  var url = "%s/users/%s/players" % [Data.server_url, GlobalStore.user.uuid]
  http_client.connect("request_completed", self, "_on_fetch_heroes_request_completed")
  var _resp = $HTTPRequest.request(url, headers, false, HTTPClient.METHOD_GET)


func _on_fetch_heroes_request_completed(_result, response_code, _headers, body):
  http_client.disconnect("request_completed", self, "_on_fetch_heroes_request_completed")  
  var response = parse_json(body.get_string_from_utf8())
  for item in response:
    var new_button = character_button_scene.instance()
    character_list.add_child(new_button)
    new_button.initialize(item)
    new_button.connect("character_button_pressed", self, "_on_character_button_pressed")
    
func _on_fetch_hero_request_completed(_result, response_code, _headers, body):
  http_client.disconnect("request_completed", self, "_on_fetch_hero_request_completed")  
  var response = parse_json(body.get_string_from_utf8())
  GlobalStore.load_hero(response)
  WsClient.connect("ws_connection_completed", self, "_on_ws_connection_completed")
  WsClient.connect_ws()
    
func _on_character_button_pressed(data):
  http_client.connect("request_completed", self, "_on_fetch_hero_request_completed")  
  var headers = ["Content-Type: application/json"]  
  var url = "%s/players/%s" % [Data.server_url, data.uuid]
  var _resp = $HTTPRequest.request(url, headers, false, HTTPClient.METHOD_GET)  
  

func _on_new_character_button_pressed():
  creation_panel.show()  
  
func _on_new_character_confirmed(character_name: String, job: String):
  var headers = ["Content-Type: application/json"]  
  var url = "%s/players" % [Data.server_url] 
  var payload = JSON.print({
    "name": character_name,
    "job": job,
    "user_uuid": GlobalStore.user.uuid
  })
  http_client.connect("request_completed", self, "_on_create_request_completed")  
  var _resp = $HTTPRequest.request(url, headers, false, HTTPClient.METHOD_POST, payload)
  
func _on_create_request_completed(_result, response_code, _headers, body):
  http_client.disconnect("request_completed", self, "_on_create_request_completed")  
  var response = parse_json(body.get_string_from_utf8())
  var new_button = character_button_scene.instance()
  character_list.add_child(new_button)
  new_button.initialize(response)
  new_button.connect("character_button_pressed", self, "_on_character_button_pressed")
  creation_panel.close()


func _on_ws_connection_completed():
  var map_id = GlobalStore.hero.current_map
  GlobalStore.current_map = map_id
  get_tree().change_scene("res://scenes/maps/%s/%s.tscn" %  [map_id, map_id])


