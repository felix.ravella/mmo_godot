extends ColorRect

const cart_item_scene: PackedScene = preload("res://scenes/merchant_window/cart_item.tscn")

onready var action_toggle: TextureButton = $ActionToggle
onready var grid_container: GridContainer = $CartScrollContainer/GridContainer
onready var total_price_label: Label = $TotalPriceLabel

onready var player_inventory_panel = get_node("../PlayerInventory")

var is_selling = false setget _set_is_selling
var total_price = 0

var buying_list: Array = []
var selling_list: Array = []


signal draggable_dropped
signal selling_list_changed() 

func _set_is_selling(value: bool) -> void:
  is_selling = true
  refresh_display()  
  $ActionButton.text = "Sell" if is_selling else "Buy"

func refresh_display():
  for child in grid_container.get_children():
    child.queue_free()
  var list_to_display = selling_list if is_selling else buying_list
  total_price = 0
  for item in list_to_display:
    var new_cart_item = cart_item_scene.instance()
    grid_container.add_child(new_cart_item)
    new_cart_item.initialize(item, is_selling)
    var price = 0
    if item.item_type == "equipable":
      price = DataItems.equipables[item.blueprint].price
    else:
      price = DataItems.resources[item.blueprint].price * item.quantity
    total_price += int(price / 4.0) if is_selling else price
  total_price_label.text = str(total_price)

func can_drop_data(position, node):
  if node is SellableItem:
    return true
  return false
  
func drop_data(position, node):
  # TODO add option to add specific quantity
  if is_selling:
    if node.item_type == "resource":
      add_sellable(node, node.instance_data.quantity)
    if node.item_type == "equipable":
      add_sellable(node, 1)
  # TODO else: add_buyable
  
func add_sellable(node, added_quantity):
  if node.item_type == "equipable":
    var new_data = node.instance_data
    new_data.item_type = "equipable"
    selling_list.append(node.instance_data)
  else:
    var index_at = null
    for i in range(selling_list.size()):
      if selling_list[i].blueprint == node.instance_data.blueprint && selling_list[i].quality == node.instance_data.quality:
        index_at = i
        break
    if index_at != null:
      selling_list[index_at].instance_data.quantity += added_quantity  
    else:
      var new_data = node.instance_data
      new_data.quantity = added_quantity
      new_data.item_type = "resource"      
      selling_list.append(new_data)
  emit_signal("selling_list_changed")
  refresh_display()
    
# receives selling item data, compare with hero inventory
func compute_remaining_quantity(sold_instance_data) -> int:
  for inventory_item in GlobalStore.hero.resources:
    if inventory_item.uuid == sold_instance_data.uuid:
      return inventory_item.quantity - sold_instance_data.quantity   
  print("WARNING! Couldn't find item in compute remaining quantity (%s, %s)" % [sold_instance_data.blueprint, sold_instance_data.blueprint])
  return 1

func _on_action_type_toggled(button_pressed: bool):
  self.is_selling = button_pressed


func _on_action_button_pressed():
  if is_selling:
    var sell_payload = {
      "hero_uuid": GlobalStore.hero.uuid,
      "sold_items": {
        "resources": [],
        "deleted_resource_uuids": [],
        "deleted_equipable_uuids": [],
       },
      "new_money": GlobalStore.hero.money + total_price
    }
    for sold_item in selling_list:
      if sold_item.item_type == "equipable":
        sell_payload["sold_items"]["deleted_equipable_uuids"].push_back(sold_item.uuid)
      if sold_item.item_type == "resource":
        var new_quantity = compute_remaining_quantity(sold_item)
        if new_quantity == 0:
          sell_payload["sold_items"]["deleted_resource_uuids"].push_back(sold_item.uuid)
        else:
          sell_payload["sold_items"]["resources"].push_back({
            "uuid": sold_item.resource_instance.uuid,
            "new_quantity": new_quantity
           })
    # build sell request
    var headers = ["Content-Type: application/json"]
    var url = "%s/sell" % Data.server_url
    $HTTPRequest.connect("request_completed", self, "_on_sell_request_completed")
    var payload = JSON.print(sell_payload)
    var _resp = $HTTPRequest.request(url, headers, false, HTTPClient.METHOD_POST, payload)
  else:
    print("TODO BUY")
    

func _on_sell_request_completed(_result, response_code, _headers, body):
  $HTTPRequest.disconnect("request_completed", self, "_on_sell_request_completed")
  match response_code: 
    200:
      selling_list = []
      var response = parse_json(body.get_string_from_utf8())
      GlobalStore.load_hero(response)
      refresh_display()
      player_inventory_panel.refresh_display()
    _:
      self.error_message = "Server unreachable, error %s " % response_code
