extends ColorRect

const sellable_item_scene: PackedScene = preload("res://scenes/merchant_window/sellable_item.tscn")

onready var items_grid = $Panel/ItemsGridContainer
onready var cart_panel = get_node("../CartPanel")

var selected_item_type: String = "resources" setget _set_selected_item_type
var armed_item_index = null setget _set_armed_item_index

func _set_selected_item_type(value: String) -> void:
  var old_value = selected_item_type
  if old_value != value:
    selected_item_type = value
    refresh_display()
  
func _set_armed_item_index(value) -> void:
  armed_item_index = value

func refresh_display():
  for child in items_grid.get_children():
    child.queue_free()
  match selected_item_type:
    "equipables":
      for index in range(GlobalStore.hero.equipables.size()):
        var equipable = GlobalStore.hero.equipables[index]
        if is_selling_stack(equipable, "equipable"):
          continue
        var location = Enums.Slot.keys()[DataItems.equipables[equipable.blueprint].slot]
        # check if item is currently equiped. if yes, don't display it
        if !!GlobalStore.hero.equiped[location]:
          if GlobalStore.hero.equiped[location].uuid == equipable.uuid:
            continue
        var new_sellable_item = sellable_item_scene.instance()
        items_grid.add_child(new_sellable_item)
        new_sellable_item._initialize(index, "equipable", equipable)
        new_sellable_item.connect("clicked", self, "_on_bag_item_clicked")
    "resources":
      for index in range(GlobalStore.hero.resources.size()):
        var resource = GlobalStore.hero.resources[index]
        if is_selling_stack(resource, "resource"):
          continue
        var new_sellable_item = sellable_item_scene.instance()
        items_grid.add_child(new_sellable_item)        
        new_sellable_item._initialize(index, "resource", resource)
        new_sellable_item.connect("clicked", self, "_on_bag_item_clicked")      

# receives item data from inveotry, compare with selling list
func is_selling_stack(instance_data, item_type) -> bool:
  for sold_item in cart_panel.selling_list:
    if sold_item.uuid == instance_data.uuid:
      if item_type == "equipable":
        return true
      if item_type == "resource":
        if sold_item.quantity == instance_data.quantity:
          return true
  return false  
  

func _on_item_type_button_pressed(item_type: String):
  self.selected_item_type = item_type
  self.armed_item_index = null

func _on_bag_item_clicked(index):
  if armed_item_index == index:
    print("TODO handle double click")
  else:
    self.armed_item_index = index
    print("TODO set timer for double click")


func _draw():
  refresh_display()


func _on_cart_panel_selling_list_changed():
  refresh_display()
