extends VBoxContainer

const quality_star_scene: PackedScene = preload("res://entities/inventory_item/quality_star.tscn")

onready var icon_texture = $IconTexture
onready var price_label = $HBoxContainer/PriceLabel
onready var quantity_panel: Panel = $IconTexture/QuantityPanel
onready var quantity_label: Label = $IconTexture/QuantityPanel/QuantityLabel
onready var quality_stars_container = $IconTexture/QualityStarsContainer

var instance_data

func initialize(_instance_data, is_selling = false):
  instance_data = _instance_data
  var data_source = DataItems.equipables if instance_data.item_type == "equipable" else DataItems.resources
  var item_data = data_source[instance_data.blueprint]
  var texture_path
  var price = 0
  match _instance_data.item_type:
    "equipable":
      texture_path = "res://assets/icons/equipables/%s.png" % instance_data.blueprint
      price = item_data.price
    "resource":
      quantity_panel.show()
      quantity_label.text = "%d" % instance_data.quantity
      for i in range(instance_data.quality):
        var new_star = quality_star_scene.instance()
        quality_stars_container.add_child(new_star)
      texture_path = "res://assets/icons/resources/%s.png" % instance_data.blueprint
      price = item_data.price * instance_data.quantity
  if ResourceLoader.exists(texture_path):
    icon_texture.texture = load(texture_path)
  
  if is_selling:
    price = int(price / 4.0)
  price_label.text = str(price)
