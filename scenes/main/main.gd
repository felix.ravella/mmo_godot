extends CanvasLayer

onready var login_edit = $Panel/LoginEdit
onready var password_edit = $Panel/PasswordEdit
onready var error_label = $Panel/ErrorLabel
onready var action_label = $Panel/ActionLabel
onready var confirm_button = $Panel/ConfirmButton
onready var switch_action_button = $Panel/SwitchActionButton

var action = "connect" setget _set_action
var login = "test"
var password = "test"
var error_message = "" setget _set_error_message

  
func _set_action(value: String):
  action = value
  action_label.text = "CONNEXION" if (action == "connect") else "CREATE ACCOUNT"
  confirm_button.text = "Connect" if (action == "connect") else "Create"
  switch_action_button.text = "Switch to account creation" if (action == "connect") else "Switch to connect"
  
func _set_error_message(value: String):
  error_message = value
  error_label.text = value
  
  
func _on_login_text_changed(new_text: String):
  login = new_text

func _on_password_text_changed(new_text: String):
  var hidden = ""

  password = hidden

func _on_switch_action_button_pressed():
  if action == "connect":
    self.action = "signup"
  else:
    self.action = "connect"

func _on_test_button_pressed(text: String):
  login = text
  password = text
  _connect()
  
func _on_confirm_button_pressed():
  if action == "connect":
    _connect()
  if action == "signup":
    signup()
  
  
func _connect():
  self.error_message = ""
  var headers = ["Content-Type: application/json"]
  var payload = JSON.print({"login": login, "password": password})
  var url = "%s/connect" % Data.server_url
  $HTTPRequest.connect("request_completed", self, "_on_login_request_completed")
  var _resp = $HTTPRequest.request(url, headers, false, HTTPClient.METHOD_POST, payload)

func signup():
  self.error_message = ""  
  var headers = ["Content-Type: application/json"]
  var payload = JSON.print({"login": login, "password": password})
  var url = "%s/users" % Data.server_url
  $HTTPRequest.connect("request_completed", self, "_on_signup_request_completed")
  var _resp = $HTTPRequest.request(url, headers, false, HTTPClient.METHOD_POST, payload)

func _on_login_request_completed(_result, response_code, _headers, body):
  $HTTPRequest.disconnect("request_completed", self, "_on_login_request_completed")
  match response_code: 
    404:
      self.error_message = "Incorrect login or password" 
    200: 
      var response = parse_json(body.get_string_from_utf8())
      GlobalStore.user.uuid = "%s" % response.uuid
      # TODO load thoses data on selecting player
  #      GlobalStore.user.data = null
  #      GlobalStore.current_map = "playground"
  #      WsClient.connect("ws_connection_completed", self, "_on_ws_connection_completed")
  #      WsClient.connect_ws()
      get_tree().change_scene("res://scenes/player_selection/player_selection.tscn")
    _:
      self.error_message = "Server unreachable, error %s " % response_code
    
  
func _on_signup_request_completed(_result, response_code, _headers, body):
  $HTTPRequest.disconnect("request_completed", self, "_on_signup_request_completed")
  match response_code: 
    409:
      self.error_message = "login is already taken" 
    200:  
      var response = parse_json(body.get_string_from_utf8())
      GlobalStore.user.uuid = "%s" % response.uuid
      # TODO load thoses data on selecting player
      GlobalStore.user.data = null
      GlobalStore.current_map = "10,13"
      WsClient.connect("ws_connection_completed", self, "_on_ws_connection_completed")
      get_tree().change_scene("res://scenes/player_selection/player_selection.tscn")
    _:
      self.error_message = "Server unreachable, error %s " % response_code

#func _on_ws_connection_completed():
#  get_tree().change_scene("res://scenes/maps/playground/playground.tscn")
  
  #WsClient._send_packet("change_map", "start")
#  print(response)
#  print(response.id)
  
func _ready():
  pass


