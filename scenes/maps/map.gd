extends Node2D

const player_scene: PackedScene = preload("res://entities/player/player.tscn")
const horde_scene: PackedScene = preload("res://entities/horde/horde.tscn")
const lobby_scene: PackedScene = preload("res://entities/battle_lobby/battle_lobby.tscn")

onready var http_request: HTTPRequest = $HTTPRequest
onready var entities_holder: YSort = $EntitiesHolder

# key uuid => value entity
var players = {}
# key uuid => value entity
var hordes = {}
# key uuid => value entity
var lobbies = {}

# set when clickd on monster.reset when move
# used to check if should trigger / join lobby on contact
var selected_fight = null

var self_player: Player = null # handle to this client player

func add_player(uuid, body):
  var new_player = player_scene.instance()
  entities_holder.add_child(new_player)
  new_player.initialize(uuid, body)    
  players[uuid] = new_player
  # stores self_player if correct id
  if uuid == GlobalStore.user.player_uuid:
    self_player = new_player
    store_position()
    new_player.connect("change_map_area_touched", self, "_on_player_touched_change_map")
    new_player.connect("horde_touched", self, "_on_player_touched_horde")
    new_player.connect("lobby_touched", self, "_on_player_touched_lobby")

func remove_player(uuid):
  entities_holder.remove_child(players[uuid])
  players.erase(uuid)
  
func add_horde(uuid, data, _position):
  var new_horde = horde_scene.instance()
  entities_holder.add_child(new_horde)
  # temporary
  _position = Vector2(150 + (75 * hordes.size()), 150 + (75 * hordes.size()))
  new_horde.initialize(uuid, data, _position)
  hordes[uuid] = new_horde
  new_horde.connect("horde_clicked", self, "_on_horde_clicked")
  
func remove_horde(uuid):
  entities_holder.remove_child(hordes[uuid])
  hordes.erase(uuid)
  
func add_battle_lobby(uuid, data, _position):
  var new_lobby = lobby_scene.instance()
  entities_holder.add_child(new_lobby)
  # temporary
  _position = Vector2(150 + (75 * lobbies.size()), 250 + (75 * lobbies.size()))
  new_lobby.initialize(uuid, data, _position)
  lobbies[uuid] = new_lobby
  new_lobby.connect("lobby_clicked", self, "_on_lobby_clicked")
  

func remove_battle_lobby(uuid):
  entities_holder.remove_child(lobbies[uuid])
  lobbies.erase(uuid)

# store self_player position on map for re-use after battle, if necessary
func store_position():
  var pos = Vector2(int(self_player.position.x), int(self_player.position.y))
  GlobalStore.current_position = pos

func _ready():
  Tooltip.hide()
  WsClient.connect("player_joined", self, "_on_player_joined")
  WsClient.connect("player_left", self, "_on_player_left")
  WsClient.connect("player_moved", self, "_on_player_moved")
  WsClient.connect("horde_added", self, "_on_horde_added")
  WsClient.connect("horde_removed", self, "_on_horde_removed")
  WsClient.connect("battle_added", self, "_on_battle_added")
  WsClient.connect("battle_removed", self, "_on_battle_removed")
  
  var headers = ["Content-Type: application/json"]
  #var url = "http://localhost:3000/maps/%s" % GlobalStore.current_map
  #var url = "https://mmo-b75a3ec16f3f.herokuapp.com/maps/%s" % GlobalStore.current_map
  var url = "%s/maps/%s" % [Data.server_url, GlobalStore.current_map]
  
  http_request.connect("request_completed", self, "_on_map_data_received")
  http_request.request(url, headers, false, HTTPClient.METHOD_GET)
  
  
func _on_map_data_received(_result, _response_code, _headers, body):
  var response = parse_json(body.get_string_from_utf8())
  if _response_code != 200:
    return
  for uuid in response.players:
    add_player(uuid, response.players[uuid])
  for uuid in response.hordes:
    add_horde(uuid, response.hordes[uuid], Vector2(0,0))
  for uuid in response.battles:
    # display lobbies only  for pending battles, not battles already playing
#    print(response.battles[uuid])
    if response.battles[uuid].status != "playing":
      add_battle_lobby(uuid, response.battles[uuid], Vector2(0,0))
  HUD.show()
  HUD.initialize()
  if not GlobalStore.battle_gains.empty():
    var battle_gains_screen = load("res://entities/battle_gains_screen/battle_gains_screen.tscn").instance()
    add_child(battle_gains_screen)
    # display gains in battle_gains component
    battle_gains_screen.initialize()
    GlobalStore.battle_gains = {}
    # re initialize

func _on_player_joined(uuid, body):
  add_player(uuid, body)

func _on_player_left(uuid):
  remove_player(uuid)

func _on_horde_added(uuid, data, _position):
  #_position = Vector2(150 + (100 * hordes.size()), 150 + (75 * hordes.size()))
  add_horde(uuid, data, _position)
  
func _on_horde_removed(uuid):
  remove_horde(uuid)
  
func _on_battle_added(uuid, data, _position):
  #_position = Vector2(150 + (100 * lobbies.size()), 150 + (75 * lobbies.size()))
  add_battle_lobby(uuid, data, _position)
  
func _on_battle_removed(uuid):
  remove_battle_lobby(uuid)
  

# handle move via click
func _unhandled_input(event):
  if event is InputEventMouseButton:
    if event.button_index == BUTTON_LEFT and event.pressed:
      if !!self_player:
        selected_fight = null
        var pos: Vector2 = get_global_mouse_position()
        move_self_player_to(pos)

func _on_horde_clicked(horde):
  selected_fight = horde.uuid
  move_self_player_to(horde.position)
  
func _on_lobby_clicked(lobby):
  selected_fight = lobby.uuid
  move_self_player_to(lobby.position)

func move_self_player_to(pos: Vector2):
  self_player.set_target_location(pos)
  var content = {"x": pos.x, "y": pos.y}
  WsClient._send_packet("move_player", content)
  store_position()

# handle other player move
func _on_player_moved(uuid, coords):
  # ignore if self_id received from server (already move on unhandled input)
  if uuid == GlobalStore.user.player_uuid:
    return
  var player: Player = players[uuid]
  var destination = Vector2(coords.x, coords.y)
  player.set_target_location(destination)

func _on_player_touched_change_map(destination_map: String, destination_position: Vector2):
  var payload = {
    "destination_map": destination_map,
    "destination_position": {"x": destination_position.x, "y": destination_position.y}
  }
  WsClient._send_packet("change_map", payload)

func _on_player_touched_horde(horde_uuid: String):
  store_position()
  if selected_fight == horde_uuid:
    WsClient._send_packet("create_lobby", horde_uuid)
  
func _on_player_touched_lobby(lobby_uuid: String):
  store_position()
  if selected_fight == lobby_uuid:
    WsClient._send_packet("join_lobby", lobby_uuid)

  
# TODO
func _on_lobby_status_changed(uuid, value):
  lobbies[uuid].status = value
