extends Node
class_name SkillData

var id: String
var label: String
var description: String
var ap_cost: int
var cooldown: int
var xp_ratio: float
var area_shape: String
var area_radius: int
var target_range: int
var modifiable_range: bool
var straight: bool
var requires_sight: bool
var cast_animation: String
var target_animation: String


func _init(data: Dictionary = {}):
  id = data.id
  label = data.label if data.has("label") else "MissingLabel"
  label = data.description if data.has("description") else "MissingDescription"
  ap_cost = data.ap_cost if data.has("ap_cost") else 0
  cooldown = data.cooldown if data.has("cooldown") else 0
  area_shape = data.ap_cost if data.has("area_shape") else "circle"
  area_radius = data.area_radius if data.has("area_radius") else 0
  target_range = data.target_range if data.has("target_range") else 1
  modifiable_range = data.modifiable_range if data.has("modifiable_range") else false
  straight = data.straight if data.has("straight") else false
  requires_sight = data.requires_sight if data.has("requires_sight") else false
  cast_animation = data.cast_animation if data.has("cast_animation") else "strike"
  target_animation = data.target_animation if data.has("target_animation") else "hurt"
