extends Node
class_name HeroData

const SKILL_THRESHOLDS = [0, 3, 3, 12, 25, 25, 46]

var uuid: String
# var name: String
var job: String
var xp: int
var money: int
var hp: int
var max_hp: int
var current_map: String
var position_x: int
var position_y: int

# tree 0
var skill_00_xp: int
var skill_01_xp: int
var skill_02_xp: int
var skill_03_xp: int

# tree 1
var skill_10_xp: int
var skill_11_xp: int
var skill_12_xp: int
var skill_13_xp: int

# tree 1
var skill_20_xp: int
var skill_21_xp: int
var skill_22_xp: int
var skill_23_xp: int

var equipables = []
var resources = []

var user_uuid: String

var equiped = {
  "BODY": null,
  "FOOT": null,
  "HEAD": null,
  "FINGER": null,
  "WEAPON": null,
 }

signal inventory_changed()

func initialize(data: Dictionary):
  uuid = data.uuid
  name = data.name
  job = data.job
  xp = data.xp
  money = data.money
  hp = data.hp
  max_hp = data.max_hp
  current_map = data.current_map
  position_x = data.position_x
  position_y = data.position_y
  skill_00_xp = data.skill_00_xp
  skill_01_xp = data.skill_01_xp
  skill_02_xp = data.skill_02_xp
  skill_03_xp = data.skill_03_xp
  skill_10_xp = data.skill_10_xp
  skill_11_xp = data.skill_11_xp
  skill_12_xp = data.skill_12_xp
  skill_13_xp = data.skill_13_xp
  skill_20_xp = data.skill_20_xp
  skill_21_xp = data.skill_21_xp
  skill_22_xp = data.skill_22_xp
  skill_23_xp = data.skill_23_xp
  equipables = data.equipables
  resources = data.resources
  user_uuid = data.user_uuid
  # set recorded equipables
  for equipable in equipables:
    var blueprint = DataItems.equipables[equipable.blueprint]
    var location = Enums.Slot.keys()[blueprint.slot]
    if data["equiped_%s_uuid" % location] == equipable.uuid:
      equiped[location] = equipable
  connect("inventory_changed", HUD.bag_panel, "refresh_display")
 
func add_equipable(equipment_instance):
  equipables.push_back(equipment_instance)
  emit_signal("inventory_changed")
  
func get_total_stat(stat_name: String) -> int:
  # TODO 
  # var base_value = get(stat_name)
  var base_value = 0
  if stat_name == "max_hp":
    base_value += max_hp
  var equiped_value = 0
  for equiped_item in equiped.values():
    if !equiped_item:
      continue
    var blueprint = DataItems.equipables[equiped_item.blueprint]
    # range(4) because for now, equipable can have 4 affixes at max
    for i in range(4):
      if (blueprint["stat_%d" % i] == stat_name):
        equiped_value += equiped_item["stat_%d_value" % i]
  
  return base_value + equiped_value

func get_starting_armor() -> int:
  var total_armor = 0
  for equiped_item in equiped.values():
    if !equiped_item:
      continue
    total_armor += DataItems.equipables[equiped_item.blueprint].armor
  return total_armor


## ---- Utility functions ---- ## 
func get_skill_xp_to_next(lvl: int) -> int:
  if lvl == 0:
    return 0
  return int(25 * pow(1.45, lvl-1) + 20*(lvl -1))

func get_total_skill_xp_to_next(lvl: int) -> int:
  var total = 0
  for i in range (1, lvl+1):
    total += get_skill_xp_to_next(i)
  return total
    
func get_skill_lvl(total_xp: int) -> int:
  var lvl = 1
  var aggregated = 24
  while total_xp > aggregated:
    lvl += 1
    aggregated += get_skill_xp_to_next(lvl)
  return lvl
  
func get_hero_xp_to_next(lvl: int) -> int:
  if lvl == 0:
    return 0
  return int(100 * pow(1.5, lvl-1) + 75*(lvl -1))

func get_total_hero_xp_to_next(lvl: int) -> int:
  var total = 0
  for i in range (1, lvl+1):
    total += get_hero_xp_to_next(i)
  return total
    
func get_hero_lvl(total_xp: int) -> int:
  var lvl = 1
  var aggregated = 99
  while total_xp > aggregated:
    lvl += 1
    aggregated += get_hero_xp_to_next(lvl)
  return lvl
  
func get_mastery(tree_name: String):
  var mastery = 0
  for skill_id in Data.skill_trees[tree_name].skills:
    var field_name = "skill_%s_xp" % Data.skills[skill_id].skill_code
#    if get(field_name) == 0:
#      mastery += 0
#    else:
    mastery += get_skill_lvl(get(field_name)) -1
  return mastery

func get_unlocked_skills() -> Array:
  #var tresholds = [0, 4, 12, 28, 52, 90, 145]
  # new version
  var unlocked_skills = []
  var trees_lvl = [0, 0, 0]
  var skill_trees: Array = Data.jobs[job].skill_trees
  # add aggregate all xp of skills each skilltree
  for i in range(0, skill_trees.size()):
    trees_lvl[i] += get_mastery(skill_trees[i])
  # add skill_ids tree mastery is higher than threshold
  for skill_index in range (0, SKILL_THRESHOLDS.size()):
    for tree_index in range (0, skill_trees.size()):
      var tree_name = skill_trees[tree_index]
      if trees_lvl[tree_index] >= SKILL_THRESHOLDS[skill_index]:
        if Data.skill_trees[tree_name].skills.size() > skill_index:
          unlocked_skills.push_back(Data.skill_trees[tree_name].skills[skill_index])    
  return unlocked_skills
