extends Node
class_name SkillTree

var id: String
var label: String
var skills: Array


func _init(data):
  id = data.id
  label = data.label if data.has("label") else "TreeLabel"
  skills = data.skills if data.has("skills") else ["default"]
