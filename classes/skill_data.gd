extends Node
class_name SkillData

var id: String
var label: String
var description: String
var ap_cost: int
var cooldown: int
var xp_ratio: float
var area_shape: String
var area_radius: int
var target_range: int
var target_free_cell: bool
var target_shape: String
var modifiable_range: bool
var straight: bool
var need_sight: bool
var cast_animation: String
var target_animation: String
var skill_tree_id: String
var skill_code: String

func _init(data: Dictionary = {}):
  id = data.id
  label = data.label if data.has("label") else "SkillLabel"
  description = data.description if data.has("description") else "SkillDesc"
  ap_cost = data.ap_cost if data.has("ap_cost") else 0
  cooldown = data.cooldown if data.has("cooldown") else 0
  area_shape = data.area_shape if data.has("area_shape") else "circle"
  area_radius = data.area_radius if data.has("area_radius") else 0
  target_range = data.target_range if data.has("target_range") else 1
  target_free_cell = data.target_free_cell if data.has("target_free_cell") else false
  target_shape = data.target_shape if data.has("target_shape") else "circle"
  modifiable_range = data.modifiable_range if data.has("modifiable_range") else false
  straight = data.straight if data.has("straight") else false
  need_sight = data.need_sight if data.has("need_sight") else false
  cast_animation = data.cast_animation if data.has("cast_animation") else "strike"
  target_animation = data.target_animation if data.has("target_animation") else "hurt"
  skill_tree_id = data.skill_tree_id if data.has("skill_tree_id") else "swords"
  skill_code = data.skill_code if data.has("skill_code") else "00"

func get_description_header() -> String:
  var text = "%d PA" % ap_cost
  text += ", Range %d" % target_range if target_range > 0 else ", Range: self" 
  text += ", CD %d" % cooldown if cooldown > 0 else ""
  return text
