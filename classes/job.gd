extends Node
class_name Job

var id: String
var label: String
var skill_trees: Array

func _init(data: Dictionary):
  id = data.id
  label = data.label if data.has("label") else "ClassLabel"
  skill_trees = data.skill_trees if data.has("skill_trees") else ["swords", "armors", "berserk"]
  
