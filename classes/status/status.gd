extends Node
class_name Status

var type: String # declared in child classes init()
var label: String # specified in child class init()
var is_harmful: bool # specified in child class init()
var is_stackable: bool # specified in child class init()
var author: Battler
var target: Battler
var power: int
var duration: int
var affixes: Array

### exemple affixe ###
#affixes = [
#  {"attribute": "strength", "value": -4, "origin": "Slow poison (misterRogue)",
#]
###

func _init(_author: Battler, _target: Battler, _power: int, _duration: int, _affixes: Array):
  author = _author
  target = _target
  power = _power
  duration = _duration
  affixes = _affixes
  
func get_lines():
  var lines = []
  for affix in affixes:
    var value = "+%d" % affix.value if affix.value >= 0 else "-%d" % affix.value
    var attribute = affix.attribute.capitalize()
    lines.append("%s %s (%s)" % [value, attribute, label])
  return lines

func _on_added():
  pass

func _on_attack_taken(_caster, _damages):
  pass

func _on_turn_started():
  pass
  
func _on_turn_ended():
  pass
  
func _on_removed():
  pass
