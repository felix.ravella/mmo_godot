extends Status
class_name HolyCandleStatus

# var is_harmful: bool = false # declared in parent class
# var author: Battler # declared in parent class
# var target: Battler # declared in parent class
# var power: int # declared in parent class
# var duration: int # declared in parent class
# var affixes: Array # declared in parent class
# var is_stackable: bool # declared in parent class

func _init(_author, _target, _power, _duration, _affixes).(_author, _target, _power, _duration, _affixes):
  type = "holy_candle"
  label = "Holy Candle"
  is_harmful = false
  is_stackable = true

func _on_turn_started():
  ._on_turn_started()
  target.armor += int(power * 2 / 100.0 )
  
func get_lines():
  var lines = []
  var armor_gain = int(power * 2 / 100.0 )
  lines.append("Gain %d armor on turn start" % armor_gain)
  var affixes = .get_lines()
  lines.append_array(affixes)
  return lines
