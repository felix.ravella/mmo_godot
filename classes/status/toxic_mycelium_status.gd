extends Status
class_name ToxicMyceliumStatus

# var is_harmful: bool = false # declared in parent class
# var author: Battler # declared in parent class
# var target: Battler # declared in parent class
# var power: int # declared in parent class
# var duration: int # declared in parent class
# var affixes: Array # declared in parent class
# var is_stackable: bool # declared in parent class

func _init(_author, _target, _power, _duration, _affixes).(_author, _target, _power, _duration, _affixes):
  type = "toxic_mycelium"
  label = "Toxic Mycelium"
  is_harmful = true
  is_stackable = true

func _on_turn_started():
  ._on_turn_started()
  var damages = int(2 * power/100.0)
  target.take_damage(damages)

func get_lines():
  var lines = []
  var damages = int(2 * power/100.0)
  lines.append("Take %d damages on turn start" % damages)
  var affixes = .get_lines()
  lines.append_array(affixes)
  return lines
