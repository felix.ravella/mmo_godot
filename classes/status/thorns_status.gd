extends Status
class_name ThornsStatus

# var is_harmful: bool = false # declared in parent class
# var author: Battler # declared in parent class
# var target: Battler # declared in parent class
# var power: int # declared in parent class
# var duration: int # declared in parent class
# var affixes: Array # declared in parent class
# var is_stackable: bool # declared in parent class

func _init(_author, _target, _power, _duration, _affixes).(_author, _target, _power, _duration, _affixes):
  type = "thorns"
  label = "Thorns"
  is_harmful = false
  is_stackable = true

func _on_attack_taken(caster, damages):
  ._on_attack_taken(caster, damages)
  var thorn_damages = int(1 * power / 100.0)
  caster.take_damage(thorn_damages)

func get_lines():
  var lines = []
  var damages = int(1 * power / 100.0)
  lines.append("Deal %d damages to attacker when hit" % damages)
  var affixes = .get_lines()
  lines.append_array(affixes)
  return lines

