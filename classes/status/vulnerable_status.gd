extends Status
class_name VulnerableStatus

# var is_harmful: bool = false # declared in parent class
# var author: Battler # declared in parent class
# var target: Battler # declared in parent class
# var power: int # declared in parent class
# var duration: int # declared in parent class
# var affixes: Array # declared in parent class
# var is_stackable: bool # declared in parent class

func _init(_author, _target, _power, _duration, _affixes).(_author, _target, _power, _duration, _affixes):
  type = "vulnerable"
  label = "Vulnerable"
  is_harmful = true
  is_stackable = false

func get_lines():
  var lines = []
  var vulnerable_power = int(power / 10.0) 
  lines.append("Take %d%% increased damages " % vulnerable_power)
  var affixes = .get_lines()
  lines.append_array(affixes)
  return lines
