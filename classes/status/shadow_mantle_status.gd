extends Status
class_name ShadowMantleStatus

# var is_harmful: bool = false # declared in parent class
# var author: Battler # declared in parent class
# var target: Battler # declared in parent class
# var power: int # declared in parent class
# var duration: int # declared in parent class
# var affixes: Array # declared in parent class
# var is_stackable: bool # declared in parent class

func _init(_author, _target, _power, _duration, _affixes).(_author, _target, _power, _duration, _affixes):
  type = "shadow_mantle"
  label = "Shadow Mantle"
  is_harmful = false
  is_stackable = false
