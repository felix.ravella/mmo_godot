extends Status
class_name CharmedStatus

# var is_harmful: bool = false # declared in parent class
# var author: Battler # declared in parent class
# var target: Battler # declared in parent class
# var power: int # declared in parent class
# var duration: int # declared in parent class
# var affixes: Array # declared in parent class
# var is_stackable: bool # declared in parent class

func _init(_author, _target, _power, _duration, _affixes).(_author, _target, _power, _duration, _affixes):
  type = "charmed"
  label = "Charmed"
  is_harmful = true
  is_stackable = false

func get_lines():
  var lines = []
  var charm_power = int(power / 10.0) 
  lines.append("Deal -%d%% damages to %s" % [charm_power, author.data.name])
  var affixes = .get_lines()
  lines.append_array(affixes)
  return lines
