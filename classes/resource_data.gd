extends Node
class_name ResourceData

var id: String
var label: String
var category
var properties: Dictionary
var price: int

func _init(data: Dictionary):
  id = data.id
  label = data.label if data.has("label") else "DefaultResourceLabel"
  category = data.category if data.has("category") else Enums.ResourceCategory.MISCELLANEOUS
  properties = data.properties if data.has("properties") else {}
  price = data.price if data.has("price") else 1
