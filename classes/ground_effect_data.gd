extends Node
class_name GroundEffectData

var id: String
#var name: String
var label: String
var color: Color
var multitrigger: bool



func _init(data: Dictionary):
  id = data.id
  label = data.label
  multitrigger = data.multitrigger
  color = Color("c46ca7eb")
  
