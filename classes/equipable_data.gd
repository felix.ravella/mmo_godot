extends Node
class_name EquipableData

var id: String
var slot
var label: String
var description: String
var price: int
var armor: int
var max_durability: int
var stat_0          # ex
var stat_0_formula
var stat_1
var stat_1_formula
var stat_2
var stat_2_formula
var stat_3
var stat_3_formula

func _init(data: Dictionary):
  id = data.id
  slot = data.slot
  label = data.label if data.has("label") else "DefaultEquippableLabel"
  description = data.description if data.has("description") else ""
  price = data.price if data.has("price") else 1
  armor = data.armor if data.has("armor") else 0
  max_durability = data.max_durability if data.has("max_durability") else 10
  stat_0 = data.stat_0 if data.has("stat_0") else null
  stat_0_formula = data.stat_0_formula if data.has("stat_0_formula") else null
  stat_1 = data.stat_1 if data.has("stat_1") else null
  stat_1_formula = data.stat_1_formula if data.has("stat_1_formula") else null
  stat_2 = data.stat_2 if data.has("stat_2") else null
  stat_2_formula = data.stat_2_formula if data.has("stat_2_formula") else null
  stat_3 = data.stat_3 if data.has("stat_3") else null
  stat_3_formula = data.stat_3_formula if data.has("stat_3_formula") else null
